macro(botofu_link_standard_filesystem CMAKE_TARGET)
    if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS "9.0")
            # See https://github.com/CLIUtils/CLI11#install
            target_link_libraries(${CMAKE_TARGET} PRIVATE stdc++fs)
            target_compile_definitions(${CMAKE_TARGET} PRIVATE CLI11_HAS_FILESYSTEM=1)
        endif()
    endif()
endmacro()
