# Compiler warning flags used
set(CLANG_WARNING_FLAGS
        -Wall -Wextra -Wpedantic
        -Wconversion -Wsign-conversion
        -Werror -pedantic-errors
        )
set(GCC_WARNING_FLAGS
        -Wall -Wextra -Wpedantic
        -Wconversion -Wsign-conversion
        -Werror -pedantic-errors
        )
set(MSVC_WARNING_FLAGS /W3)

set(BOTOFU_WARNING_FLAGS
        # Warnings for clang
        $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>>:${CLANG_WARNING_FLAGS}>
        # Warnings for GCC
        $<$<CXX_COMPILER_ID:GNU>:${GCC_WARNING_FLAGS}>
        # Warnings for MSVC
        $<$<CXX_COMPILER_ID:MSVC>:${MSVC_WARNING_FLAGS}>
        )

#Extra compilation flags
set(CLANG_COMPILATION_FLAGS)
set(GCC_COMPILATION_FLAGS)
set(MSVC_COMPILATION_FLAGS)

set(BOTOFU_COMPILATION_FLAGS
        $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>>:${CLANG_COMPILATION_FLAGS}>
        $<$<CXX_COMPILER_ID:GNU>:${GCC_COMPILATION_FLAGS}>
        $<$<CXX_COMPILER_ID:MSVC>:${MSVC_COMPILATION_FLAGS}>
        )


# We want static linking for some executables
set(CLANG_STATIC_LINKING_FLAGS)
set(GCC_STATIC_LINKING_FLAGS)
set(MSVC_STATIC_LINKING_FLAGS)

set(BOTOFU_STATIC_LINKING_FLAGS
        $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>>:${CLANG_STATIC_LINKING_FLAGS}>
        $<$<CXX_COMPILER_ID:GNU>:${GCC_STATIC_LINKING_FLAGS}>
        $<$<CXX_COMPILER_ID:MSVC>:${MSVC_STATIC_LINKING_FLAGS}>
        )
