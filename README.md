# Botofu

The bot and tooling platform for Dofus 2.0.

## Static libraries 

### [`ios/core`](src/botofu/ios/core/README.md)

Provides low-level I/Os facilities with the `BinaryReader` and `BinaryWriter` classes.

### [`swf/parser`](src/botofu/swf/parser/README.md)

Allow parsing arbitrary SWF files. Has been tested with several DofusInvoker.swf without any major issue.

## Shared libraries

### [`network/mitm/read_only`](src/botofu/network/mitm/read_only/README.md)

A read-only hook to intercept the network packets received and sent by the client
on Linux.

## Executables

### [`protocol/parser`](src/botofu/protocol/parser/README.md)

Use `ios` and `swf/parser` libraries to extract the protocol informations. Information is mainly extracted from the DofusInvoker.swf file, but other files in the Dofus path may be used to enrich the data available to the user.
