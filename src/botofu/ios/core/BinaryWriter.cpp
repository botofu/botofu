#include "BinaryWriter.h"

#include <cstring>

#include <boost/endian/conversion.hpp>

template <boost::endian::order endianness>
char *BinaryWriter<endianness>::data() const {
    return m_data;
}

template <boost::endian::order endianness>
BinaryWriter<endianness>::BinaryWriter(char *data, std::size_t size)
      : m_data{data}, m_size{size}, m_pos{0} {
}

template <boost::endian::order endianness>
char *BinaryWriter<endianness>::get_current_position() const noexcept {
    return m_data + m_pos;
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::advance(std::size_t bytes_wrote) {
    m_pos += bytes_wrote;
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_int8(int8 value) {
    this->write<int8>(value);
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_uint8(uint8 value) {
    this->write<uint8>(value);
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_int16(int16 value) {
    this->write_and_endian_convert<int16>(value);
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_uint16(uint16 value) {
    this->write_and_endian_convert<uint16>(value);
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_int32(int32 value) {
    this->write_and_endian_convert<int32>(value);
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_uint32(uint32 value) {
    this->write_and_endian_convert<uint32>(value);
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_int64(int64 value) {
    this->write_and_endian_convert<int64>(value);
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_uint64(uint64 value) {
    this->write_and_endian_convert<uint64>(value);
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_bool(bool value) {
    this->write<int8>(value);
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_bytes(char const *bytes,
                                           std::size_t size) {
    std::memcpy(this->get_current_position(), bytes, size * sizeof(char));
    this->advance(size * sizeof(char));
}

template <boost::endian::order endianness>
void BinaryWriter<endianness>::write_UTF(char const *bytes, std::size_t size) {
    this->write_uint16(static_cast<uint16>(size));
    this->write_bytes(bytes, size);
}

// Explicit instantiation definition
template struct BinaryWriter<boost::endian::order::big>;
template struct BinaryWriter<boost::endian::order::little>;
