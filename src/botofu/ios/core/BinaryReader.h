#ifndef BOTOFU_SRC_BOTOFU_IOS_CORE_BINARYREADER_H
#define BOTOFU_SRC_BOTOFU_IOS_CORE_BINARYREADER_H

#include <cstring>
#include <string>

#include <boost/endian/conversion.hpp>

#include "constants.h"
#include "types.h"

/**
 * @brief Simple binary reader that reads from a pre-allocated buffer.
 * @tparam endianness order in which we read the bytes inside a multi-byte read.
 * This does not change the indices of the bytes read by one write, but it may
 * change the order of the bytes in one read. In other words, a read_uint32
 * followed by a read_uint16 will always result in the following read memory
 * layout: | 0   8   16  24  32 | | 40  48 | but the ordering of the bytes
 * inside the uint32 and uint16 might change according to endianness.
 */
template <boost::endian::order endianness>
struct BinaryReader {
public:
    /**
     * @brief Default construct a reader without any buffer.
     *
     * This constructor can be used with the set_buffer method to construct a
     * BinaryReader.
     */
    BinaryReader();

    /**
     * @brief Constructs a reader with a pre-allocated buffer of @p size bytes
     *  starting at the address @p data.
     * @param data start of the pre-allocated buffer used to read.
     * @param size size of the pre-allocated buffer used to read.
     */
    BinaryReader(char const *buffer, std::size_t size) noexcept;

    /**
     * @brief Reset the reader and set the given buffer as the new read buffer.
     * @param buffer start of the pre-allocated buffer used to read.
     * @param size size of the pre-allocated buffer used to read.
     */
    void set_buffer(char const *buffer, std::size_t size) noexcept;

    /** Reads a boolean value. */
    bool read_bool();

    /** Reads a 1-byte signed value. */
    int8 read_int8();

    /** Reads a 1-byte unsigned value. */
    uint8 read_uint8();

    /** Reads a 2-byte signed value. */
    int16 read_int16();

    /** Reads a 2-byte unsigned value. */
    uint16 read_uint16();

    /** Reads a 4-byte signed value. */
    int32 read_int32();

    /** Reads a 4-byte unsigned value. */
    uint32 read_uint32();

    /** Reads a 8-byte signed value. */
    int64 read_int64();

    /** Reads a 8-byte unsigned value. */
    uint64 read_uint64();

    /** Reads a 2-byte signed compressed value. */
    int16 read_var_int16();

    /** Reads a 2-byte unsigned compressed value. */
    uint16 read_var_uint16();

    /** Reads a 4-byte signed compressed value. */
    int32 read_var_int32();

    /** Reads a 4-byte unsigned compressed value. */
    uint32 read_var_uint32();

    /** Reads a 8-byte signed compressed value. */
    int64 read_var_int64();

    /** Reads a 8-byte unsigned compressed value. */
    uint64 read_var_uint64();

    /** Reads a 4-byte float value. */
    float read_float();

    /** Reads a 8-byte double value. */
    double read_double();

    /** Reads @p size bytes. */
    ByteArray read_bytes(uint32 size);

    /** Reads the length as a 2-byte unsigned and then read length bytes. */
    ByteArray read_bytes();

    /** Reads @p size bytes. */
    std::string read_utf(uint32 size);

    /** Reads the length as a 2-byte unsigned and then read length bytes. */
    std::string read_utf();

    /** Change the current position of the reader to @p pos. */
    void seekg(std::size_t pos);

    /** Return the current position of the reader. */
    [[nodiscard]] std::size_t tellg() const;

    /** Ignore @p size bytes from the input buffer. */
    void ignore(std::size_t size);

protected:
    /**
     * @brief Get the number of bytes read by the reader.
     * @return the number of bytes read by the reader.
     */
    [[nodiscard]] char const *get_current_position() const noexcept;

    /**
     * @brief increase manually the number of bytes read by the reader.
     * @param bytes_read number of bytes that have been read but not accounted
     * for by the reader yet.
     */
    void advance(std::size_t bytes_read);

    /**
     * @brief Read the given @p data using the compression method from Adobe SWF
     *  format.
     *
     * The compression method is the following: numbers are encoded as a stream
     * of bytes where, for each byte in the stream:
     *  1. The first bit (left-most) is set to 1 if the next byte is also part
     * of the number, 0 if we read all the bytes of the number.
     *  2. The 7 right-most bits are the actual data.
     *
     * @tparam T type of @p data.
     * @param data data to read from the buffer.
     */
    template <typename T>
    void read_var(T &data);

    /**
     * @brief Read the given @p data using the compression method from Adobe SWF
     * format and \ convert endianness if needed.
     * @tparam T type of @p data.
     * @param data data to read from the buffer.
     */
    template <typename T>
    void read_var_and_endian_convert(T &data);

    /**
     * @brief Reads the given @p data from the underlying buffer.
     * @tparam T type of @p data.
     * @param data data to reader from the buffer.
     */
    template <typename T>
    void read(T &data);

    /**
     * @brief Reads the given @p data from the underlying buffer before a
     *  potential endianness swap.
     * @tparam T type of @p data.
     * @param data data to read from the buffer.
     */
    template <typename T>
    void read_and_endian_convert(T &data);

    /** Returning version of read_var(T& data). */
    template <typename T>
    T read_var();

    /** Returning version of read_var_and_endian_convert(T& data). */
    template <typename T>
    T read_var_and_endian_convert();

    /** Returning version of read(T& data). */
    template <typename T>
    T read();

    /** Returning version of read_and_endian_convert(T& data). */
    template <typename T>
    T read_and_endian_convert();

    /**
     * @brief Reads @p size bytes from the underlying buffer into @p data.
     * @param data a buffer of at least @p size bytes that will be set to the
     *  read data.
     * @param size number of bytes to read from the underlying buffer.
     */
    void read(char *data, std::size_t size);

    /**
     * @brief Convert @p data in place to match the Reader endianness.
     * @tparam T type of @p data.
     * @param data data that will be endian converted.
     */
    template <typename T>
    void endian_convert(T &data);

private:
    char const *m_buffer;
    std::size_t m_size;
    std::size_t m_pos;
};

// Explicit instantiation declaration.
extern template struct BinaryReader<boost::endian::order::big>;
extern template struct BinaryReader<boost::endian::order::little>;
// Aliases for endianness.
using BigEndianReader    = BinaryReader<boost::endian::order::big>;
using LittleEndianReader = BinaryReader<boost::endian::order::little>;

template <boost::endian::order endianness>
template <typename T>
void BinaryReader<endianness>::read_var(T &data) {
    uint8 byte;
    uint8 offset{0};
    bool  has_next;

    // First ensure that data is 0, then write over it with
    // logical OR.
    data = 0;

    do {
        byte     = this->read_uint8();
        has_next = (byte & MASK_10000000);

        data |= static_cast<T>(static_cast<T>(byte & MASK_01111111)
                               << offset);

        offset = static_cast<uint8>(offset + CHUNK_BIT_SIZE);
    } while (has_next);
}

template <boost::endian::order endianness>
template <typename T>
void BinaryReader<endianness>::read(T &data) {
    std::memcpy(&data, this->get_current_position(), sizeof(T));
    this->advance(sizeof(T));
}

template <boost::endian::order endianness>
template <typename T>
void BinaryReader<endianness>::endian_convert(T &data) {
    boost::endian::conditional_reverse_inplace<endianness,
                                               boost::endian::order::native>(
          data);
}

template <boost::endian::order endianness>
template <typename T>
void BinaryReader<endianness>::read_and_endian_convert(T &data) {
    this->read(data);
    this->endian_convert(data);
}

template <boost::endian::order endianness>
template <typename T>
T BinaryReader<endianness>::read() {
    T data{};
    this->read(data);
    return data;
}

template <boost::endian::order endianness>
template <typename T>
T BinaryReader<endianness>::read_and_endian_convert() {
    T data{};
    this->read_and_endian_convert(data);
    return data;
}

template <boost::endian::order endianness>
template <typename T>
void BinaryReader<endianness>::read_var_and_endian_convert(T &data) {
    this->read_var(data);
    this->endian_convert(data);
}

template <boost::endian::order endianness>
template <typename T>
T BinaryReader<endianness>::read_var() {
    T data{};
    this->read_var(data);
    return data;
}

template <boost::endian::order endianness>
template <typename T>
T BinaryReader<endianness>::read_var_and_endian_convert() {
    T data{};
    this->read_var_and_endian_convert(data);
    return data;
}

#endif   // BOTOFU_SRC_BOTOFU_IOS_CORE_BINARYREADER_H
