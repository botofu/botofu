#ifndef BOTOFU_SRC_BOTOFU_IOS_TYPES_H
#define BOTOFU_SRC_BOTOFU_IOS_TYPES_H

#include <array>
#include <cstdint>
#include <vector>

#include <boost/dynamic_bitset.hpp>

/**************************************************
 *                 INTEGER TYPES                  *
 **************************************************/
using int8   = std::int8_t;
using uint8  = std::uint8_t;
using int16  = std::int16_t;
using uint16 = std::uint16_t;
using int32  = std::int32_t;
using uint32 = std::uint32_t;
using int64  = std::int64_t;
using uint64 = std::uint64_t;

/**************************************************
 *           FLOATING POINT EQUIVALENTS           *
 **************************************************/
// First assert that float is 32 bits and double is 64 bits.
static_assert(sizeof(float) == sizeof(std::uint32_t),
              "Float are expected to be 32 bits.");
static_assert(sizeof(double) == sizeof(std::uint64_t),
              "Doubles are expected to be 64 bits.");
// Then define the types.
/** Integer type having the same binary size as a float. */
using IntSameSizeAsFloat = std::uint32_t;
/** Integer type having the same binary size as a double. */
using IntSameSizeAsDouble = std::uint64_t;

/**************************************************
 *                 BYTEARRAY TYPE                 *
 **************************************************/
/** A compile-time length ByteArray. */
template <std::size_t S>
using StaticByteArray = std::array<char, S>;
/** A runtime length ByteArray. */
using ByteArray = std::vector<char>;
/** A runtime length bit-array. */
using BitArray = boost::dynamic_bitset<uint8>;

#endif   // BOTOFU_SRC_BOTOFU_IOS_TYPES_H
