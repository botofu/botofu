#ifndef BOTOFU_SRC_BOTOFU_IOS_CORE_BINARYWRITER_H
#define BOTOFU_SRC_BOTOFU_IOS_CORE_BINARYWRITER_H

#include <cstring>
#include <string>

#include <boost/endian/conversion.hpp>

#include "constants.h"
#include "types.h"

/**
 * @brief Simple binary writer that writes into a pre-allocated buffer.
 *
 * @tparam endianness order in which we write the bytes inside a multi-byte
 * write. This does not change the indices of the bytes written by one write,
 * but it may change the order of the bytes in one write. In other words, a
 * write_uint32 followed by a write_uint16 will always result in the following
 * memory layout:  | 0   8   16  24  32 | | 40  48 | but the ordering of the
 * bytes inside the uint32 and uint16 might change according to endianness.
 */
template <boost::endian::order endianness>
struct BinaryWriter {
    BinaryWriter() = delete;

    /**
     * @brief Constructs a writer with a pre-allocated buffer of @p size bytes
     * starting at the address @p data.
     *
     * @param data start of the pre-allocated buffer used to write.
     * @param size size of the pre-allocated buffer used to write.
     */
    BinaryWriter(char *data, std::size_t size);

    /**
     * @brief Access the raw buffer used to write data.
     *
     * @return a non-const pointer to access the data. Modification of the
     * buffer is possible, but discouraged as it might corrupt the written data.
     */
    [[nodiscard]] char *data() const;

    /** Write the boolean @p value to the underlying buffer. */
    void write_bool(bool value);

    /** Write a 1-byte signed @p value to the underlying buffer. */
    void write_int8(int8 value);

    /** Write a 1-byte unsigned @p value to the underlying buffer. */
    void write_uint8(uint8 value);

    /** Write a 2-byte signed @p value to the underlying buffer. */
    void write_int16(int16 value);

    /** Write a 2-byte unsigned @p value to the underlying buffer. */
    void write_uint16(uint16 value);

    /** Write a 4-byte signed @p value to the underlying buffer. */
    void write_int32(int32 value);

    /** Write a 4-byte unsigned @p value to the underlying buffer. */
    void write_uint32(uint32 value);

    /** Write a 8-byte signed @p value to the underlying buffer. */
    void write_int64(int64 value);

    /** Write a 8-byte unsigned @p value to the underlying buffer. */
    void write_uint64(uint64 value);

    /** Write @p size bytes from @p bytes to the underlying buffer. */
    void write_bytes(char const *bytes, std::size_t size);

    /** Write @p size as a 2-byte unsigned value, followed by @p size bytes \
     * from @p bytes to the underlying buffer. */
    void write_UTF(char const *bytes, std::size_t size);

protected:
    /**
     * @brief Writes the given @p data into the underlying buffer.
     * @tparam T type of @p data.
     * @param data data to write into the buffer.
     */
    template <typename T>
    void write(T const &data);

    /**
     * @brief Convert @p data in place to match the Writer endianness.
     * @tparam T type of @p data.
     * @param data data that will be endian converted.
     */
    template <typename T>
    void endian_convert(T &data);

    /**
     * @brief Writes the given @p data into the underlying buffer after a
     * potential endianness swap.
     * @tparam T type of @p data.
     * @param data data to write into the buffer.
     */
    template <typename T>
    void write_and_endian_convert(T const &data);

    /**
     * @brief Write the given @p data using the compression method from Adobe
     * SWF format.
     *
     * The compression method is the following: numbers are encoded as a stream
     * of bytes where, for each byte in the stream:
     *  1. The first bit (left-most) is set to 1 if the next byte is also part
     * of the number, 0 if we read all the bytes of the number.
     *  2. The 7 right-most bits are the actual data.
     *
     * @tparam T type of @p data.
     * @param data data to write into the buffer.
     */
    template <typename T>
    void write_var(T data);

    /**
     * @brief Write the given @p data using the compression method from Adobe
     * SWF format and \ convert endianness if needed.
     * @tparam T type of @p data.
     * @param data data to write into the buffer.
     */
    template <typename T>
    void write_var_and_endian_convert(T data);

    /**
     * @brief Get the number of bytes written by the writer.
     * @return the number of bytes written by the writer.
     */
    [[nodiscard]] char *get_current_position() const noexcept;

    /**
     * @brief increase manually the number of bytes written by the writer.
     * @param bytes_wrote number of bytes that have been written but not
     * accounted for by the writer yet.
     */
    void advance(std::size_t bytes_wrote);

private:
    char *      m_data;
    std::size_t m_size;
    std::size_t m_pos;
};

// Explicit instantiation declaration.
extern template struct BinaryWriter<boost::endian::order::big>;
extern template struct BinaryWriter<boost::endian::order::little>;
// Aliases for endianness.
using BigEndianWriter    = BinaryWriter<boost::endian::order::big>;
using LittleEndianWriter = BinaryWriter<boost::endian::order::little>;

template <boost::endian::order endianness>
template <typename T>
void BinaryWriter<endianness>::endian_convert(T &data) {
    boost::endian::conditional_reverse_inplace<boost::endian::order::native,
                                               endianness>(data);
}

template <boost::endian::order endianness>
template <typename T>
void BinaryWriter<endianness>::write(T const &data) {
    std::memcpy(this->get_current_position(), &data, sizeof(T));
    this->advance(sizeof(T));
}

template <boost::endian::order endianness>
template <typename T>
void BinaryWriter<endianness>::write_and_endian_convert(const T &data) {
    this->write(boost::endian::conditional_reverse<boost::endian::order::native,
                                                   endianness>(data));
}

template <boost::endian::order endianness>
template <typename T>
void BinaryWriter<endianness>::write_var(T data) {
    uint8 byte;
    bool  has_next;

    do {
        byte = static_cast<uint8>(data & MASK_01111111);
        data >>= CHUNK_BIT_SIZE;
        has_next = static_cast<bool>(data);

        if (has_next) {
            byte |= MASK_10000000;
        }
        this->write_uint8(byte);
    } while (has_next);
}

template <boost::endian::order endianness>
template <typename T>
void BinaryWriter<endianness>::write_var_and_endian_convert(T data) {
    this->write_var<T>(
          boost::endian::conditional_reverse<boost::endian::order::native,
                                             endianness>(data));
}

#endif   // BOTOFU_SRC_BOTOFU_IOS_CORE_BINARYWRITER_H
