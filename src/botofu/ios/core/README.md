# `ios/core`

Implementation of simple I/O helpers for the botofu project.

## Content of the library

The library provides aliases for fixed-size integer types in `types.h` as
well as implementations of the `BinaryReader` and `BinaryWriter` classes.

The `BinaryReader` & `BinaryWriter` classes require a template parameter 
for the endianness desired. Both big and little endian versions of the 
`BinaryReader` and `BinaryWriter` classes are explicitly instantiated in  
`BinaryReader.cpp` and `BinaryWriter.cpp`, which means that your compiler
will compile `BinaryReader` and `BinaryWriter` only twice each (one time
for each endianness). This also means that you should link the static 
library generated.

Aliases for endian-specific readers and writers are given:

- `BigEndianWriter`
- `LittleEndianWriter`
- `BigEndianReader`
- `LittleEndianReader`

