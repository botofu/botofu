#include "BinaryReader.h"

template <boost::endian::order endianness>
BinaryReader<endianness>::BinaryReader(char const *buffer,
                                       std::size_t size) noexcept
      : m_buffer{buffer}, m_size{size}, m_pos{0} {
}

template <boost::endian::order endianness>
void BinaryReader<endianness>::set_buffer(char const *buffer,
                                          std::size_t size) noexcept {
    m_buffer = buffer;
    m_size   = size;
    m_pos    = 0;
}

template <boost::endian::order endianness>
char const *BinaryReader<endianness>::get_current_position() const noexcept {
    return m_buffer + m_pos;
}

template <boost::endian::order endianness>
void BinaryReader<endianness>::advance(std::size_t bytes_read) {
    m_pos += bytes_read;
}

template <boost::endian::order endianness>
bool BinaryReader<endianness>::read_bool() {
    return static_cast<bool>(read<uint8>());
}

template <boost::endian::order endianness>
int8 BinaryReader<endianness>::read_int8() {
    return read<int8>();
}

template <boost::endian::order endianness>
uint8 BinaryReader<endianness>::read_uint8() {
    return read<uint8>();
}

template <boost::endian::order endianness>
int16 BinaryReader<endianness>::read_int16() {
    return read<int16>();
}

template <boost::endian::order endianness>
uint16 BinaryReader<endianness>::read_uint16() {
    return read<uint16>();
}

template <boost::endian::order endianness>
int32 BinaryReader<endianness>::read_int32() {
    return read<int32>();
}

template <boost::endian::order endianness>
uint32 BinaryReader<endianness>::read_uint32() {
    return read<uint32>();
}

template <boost::endian::order endianness>
int64 BinaryReader<endianness>::read_int64() {
    return read<int64>();
}

template <boost::endian::order endianness>
uint64 BinaryReader<endianness>::read_uint64() {
    return read<uint64>();
}

template <boost::endian::order endianness>
void BinaryReader<endianness>::read(char *data, std::size_t size) {
    std::memcpy(data, this->get_current_position(), size * sizeof(char));
    this->advance(size * sizeof(char));
}

template <boost::endian::order endianness>
ByteArray BinaryReader<endianness>::read_bytes(uint32 const size) {
    ByteArray ret(size);
    this->read(ret.data(), size);
    return ret;
}

template <boost::endian::order endianness>
ByteArray BinaryReader<endianness>::read_bytes() {
    return this->read_bytes(this->read_uint16());
}

template <boost::endian::order endianness>
std::string BinaryReader<endianness>::read_utf(uint32 size) {
    std::string ret(size, 0);
    this->read(&(*ret.begin()), size);
    return ret;
}

template <boost::endian::order endianness>
std::string BinaryReader<endianness>::read_utf() {
    return this->read_utf(this->read_uint16());
}

template <boost::endian::order endianness>
void BinaryReader<endianness>::seekg(std::size_t pos) {
    m_pos = pos;
}

template <boost::endian::order endianness>
std::size_t BinaryReader<endianness>::tellg() const {
    return m_pos;
}

template <boost::endian::order endianness>
void BinaryReader<endianness>::ignore(std::size_t size) {
    this->advance(size);
}

template <boost::endian::order endianness>
BinaryReader<endianness>::BinaryReader()
      : m_buffer{nullptr}, m_size{0}, m_pos{0} {
}

template <boost::endian::order endianness>
int16 BinaryReader<endianness>::read_var_int16() {
    return this->read_var_and_endian_convert<int16>();
}

template <boost::endian::order endianness>
uint16 BinaryReader<endianness>::read_var_uint16() {
    return this->read_var_and_endian_convert<uint16>();
}

template <boost::endian::order endianness>
int32 BinaryReader<endianness>::read_var_int32() {
    return this->read_var_and_endian_convert<int32>();
}

template <boost::endian::order endianness>
uint32 BinaryReader<endianness>::read_var_uint32() {
    return this->read_var_and_endian_convert<uint32>();
}

template <boost::endian::order endianness>
int64 BinaryReader<endianness>::read_var_int64() {
    return this->read_var_and_endian_convert<int64>();
}

template <boost::endian::order endianness>
uint64 BinaryReader<endianness>::read_var_uint64() {
    return this->read_var_and_endian_convert<uint64>();
}

template <boost::endian::order endianness>
double BinaryReader<endianness>::read_double() {
    double              tmp;
    IntSameSizeAsDouble tmp_int;
    this->read_and_endian_convert(tmp_int);
    std::memcpy(&tmp, &tmp_int, sizeof(double));
    return tmp;
}

template <boost::endian::order endianness>
float BinaryReader<endianness>::read_float() {
    float              tmp;
    IntSameSizeAsFloat tmp_int;
    this->read_and_endian_convert(tmp_int);
    std::memcpy(&tmp, &tmp_int, sizeof(float));
    return tmp;
}

// Explicit instantiation definition
template struct BinaryReader<boost::endian::order::big>;
template struct BinaryReader<boost::endian::order::little>;
