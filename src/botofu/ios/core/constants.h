#ifndef BOTOFU_SRC_BOTOFU_IOS_CORE_CONSTANTS_H
#define BOTOFU_SRC_BOTOFU_IOS_CORE_CONSTANTS_H

#include <boost/endian/conversion.hpp>

#include "types.h"

constexpr const uint8 CHUNK_BIT_SIZE{static_cast<uint8>(sizeof(int8) * 8 - 1)};
constexpr const uint8 MASK_00000000{static_cast<uint8>(0)};
constexpr const uint8 MASK_10000000{static_cast<uint8>(128)};
constexpr const uint8 MASK_01000000{static_cast<uint8>(64)};
constexpr const uint8 MASK_00100000{static_cast<uint8>(32)};
constexpr const uint8 MASK_00010000{static_cast<uint8>(16)};
constexpr const uint8 MASK_00001000{static_cast<uint8>(8)};
constexpr const uint8 MASK_00000100{static_cast<uint8>(4)};
constexpr const uint8 MASK_00000010{static_cast<uint8>(2)};
constexpr const uint8 MASK_00000001{static_cast<uint8>(1)};
constexpr const uint8 MASK_01111111{static_cast<uint8>(127)};
constexpr const uint8 MASK_11111111{static_cast<uint8>(0xFF)};
constexpr const uint8 MASK_00001111{static_cast<uint8>(0x0F)};
constexpr const uint8 MASK_11110000{static_cast<uint8>(0xF0)};

// Declaration of endian values
constexpr const boost::endian::order BIGENDIAN = boost::endian::order::big;
constexpr const boost::endian::order LITTLEENDIAN =
      boost::endian::order::little;

#endif   // BOTOFU_SRC_BOTOFU_IOS_CORE_CONSTANTS_H
