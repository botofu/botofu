#include "CLIOptions.h"

CLIOptions::CLIOptions(int argc, char *argv[]) : argc{argc}, argv{argv} {
}

json CLIOptions::to_json() const {
    return std::vector<std::string>(this->argv + 1, this->argv + this->argc);
}
