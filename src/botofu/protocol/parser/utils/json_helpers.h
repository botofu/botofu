#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_UTILS_JSON_HELPERS_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_UTILS_JSON_HELPERS_H

#include <optional>

#include <nlohmann/json.hpp>

#include "botofu/protocol/parser/data_structures/ClassField.h"

using json = nlohmann::json;

/**
 * Insert a value into a JSON if the value is not already in the default values.
 *
 * If @p default_values is not provided (i.e. std::nullopt), insert @p value at
 * the key @p key into @p j.
 */
template <typename T>
void insert_value(std::optional<json> const &default_values,
                  json                      &j,
                  std::string const         &key,
                  T const                   &value) {
    if (   // the default values are not provided or
          !default_values ||
          // the key is not in the default values or
          (default_values->find(key) == default_values->end()) ||
          // the value provided and the default one are different
          (value != (*default_values)[key])) {
        // Then insert
        j[key] = value;
    }
}

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_UTILS_JSON_HELPERS_H
