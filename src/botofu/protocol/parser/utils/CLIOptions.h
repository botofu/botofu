#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_UTILS_CLIOPTIONS_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_UTILS_CLIOPTIONS_H

#include <filesystem>
#include <string>
#include <vector>

#include <nlohmann/json.hpp>

namespace fs = std::filesystem;
using json   = nlohmann::json;

struct CLIOptions {
    CLIOptions(int argc, char *argv[]);

    int const                argc;
    char const *const *const argv;
    fs::path                 dofus_invoker_path;
    fs::path                 target_json_path;
    int                      indent{0};
    bool                     no_default_optimisation{false};
    std::string              parsing_name;
    std::vector<std::string> tags;

    [[nodiscard]] json to_json() const;
};

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_UTILS_CLIOPTIONS_H
