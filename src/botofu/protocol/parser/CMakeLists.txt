add_executable(botofu_protocol_parser main.cpp)

# Get external dependencies
hunter_add_package(Boost COMPONENTS)
find_package(Boost CONFIG REQUIRED)
hunter_add_package(spdlog)
find_package(spdlog REQUIRED)
hunter_add_package(nlohmann_json)
find_package(nlohmann_json REQUIRED)
hunter_add_package(CLI11)
find_package(CLI11 REQUIRED)
hunter_add_package(date)
find_package(date CONFIG REQUIRED)

target_sources(botofu_protocol_parser INTERFACE version.h)
add_subdirectory(data_structures)
add_subdirectory(utils)

target_link_libraries(botofu_protocol_parser
        PUBLIC
        Boost::boost
        botofu_swf_parser
        botofu_common_utils
        nlohmann_json::nlohmann_json
        PRIVATE
        spdlog::spdlog
        CLI11::CLI11
        )

# Fix for old versions of GCC
botofu_link_standard_filesystem(botofu_swf_parser)

target_compile_options(botofu_protocol_parser PRIVATE ${BOTOFU_WARNING_FLAGS} ${BOTOFU_COMPILATION_FLAGS})
target_link_options(botofu_protocol_parser PRIVATE ${BOTOFU_STATIC_LINKING_FLAGS})