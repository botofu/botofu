#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_ENUMINFORMATION_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_ENUMINFORMATION_H

#include <string>
#include <vector>

#include <nlohmann/json.hpp>

#include "botofu/ios/core/types.h"
#include "botofu/common/dofus_interface/DofusType.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

using json = nlohmann::json;

class EnumInformation {
public:
    EnumInformation(TagDoAbc const &tag, uint32 index);

    [[nodiscard]] json to_json() const;

    [[nodiscard]] std::string const & get_name() const;

private:
    DofusType m_enum_type;
    DofusType m_member_type;
    std::vector<std::pair<std::string /*name*/, std::string /*value*/>>
          m_members;
};

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_ENUMINFORMATION_H
