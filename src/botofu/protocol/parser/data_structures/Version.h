#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_VERSION_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_VERSION_H

#include <cstddef>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

struct Version {
    std::size_t major;
    std::size_t minor;
    std::size_t patch;
    std::size_t build;

    [[nodiscard]] json to_json() const;
};

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_VERSION_H
