#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_LIMITS_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_LIMITS_H

#include <optional>
#include <string>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

struct Limits {
    explicit operator bool() const;

    std::optional<std::string> lower;
    std::optional<std::string> upper;
};

void to_json(json &j, const Limits &limits);

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_LIMITS_H
