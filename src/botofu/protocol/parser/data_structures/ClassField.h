#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSFIELD_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSFIELD_H

#include <optional>
#include <string>

#include <nlohmann/json.hpp>

#include "botofu/protocol/parser/data_structures/Limits.h"
#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/dofus_interface/DofusType.h"

using json = nlohmann::json;

/** Store all the informations on a field.
 *
 * A field is an attribute of the network classes in the DofusInvoker.swf.
 */
struct ClassField {
    ClassField();

    DofusAttribute name;
    DofusType      type;
    std::string    default_value;
    std::size_t    position;

    DofusMethod self_serialize_method;
    DofusMethod write_type_id_method;
    DofusMethod write_method;
    DofusMethod write_length_method;
    DofusMethod write_false_if_null_method;
    Limits      bounds;
    int         boolean_byte_wrapper_position;
    std::size_t constant_length;
    bool        is_vector;

    /**
     * @brief Export the ClassField instance to JSON.
     *
     * If @p default_values is provided (i.e. not std::nullopt), only include in
     * the resulting JSON the fields that are different than the default value.
     */
    [[nodiscard]] json
    to_json(std::optional<json> const &default_values = std::nullopt) const;
};

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSFIELD_H
