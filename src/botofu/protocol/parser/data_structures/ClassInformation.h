#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSINFORMATION_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSINFORMATION_H

#include <optional>
#include <string>
#include <unordered_map>

#include <nlohmann/json.hpp>

#include "botofu/protocol/parser/data_structures/ClassField.h"
#include "botofu/common/dofus_interface/DofusType.h"
#include "botofu/swf/parser/abc/ClassInfo.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/InstanceInfo.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

using json = nlohmann::json;

/**
 * Extracts and store all the data about one class from the network namespace.
 */
class ClassInformation {
public:
    ClassInformation(TagDoAbc const &tag, std::size_t index);

    /**
     * @brief Export the ClassInformation instance to JSON.
     * @param field_default_values optional default values for the fields
     * serialisation. If provided, the JSON serialisation of fields will only
     * contain the values that are different from the provided defaults. If not
     * provided (i.e. std::nullopt), the JSON serialisation of fields will
     * contain all the entries, even empty ones.
     * @return the JSON serialisation of the instance.
     */
    [[nodiscard]] json to_json(
          std::optional<json> const &field_default_values = std::nullopt) const;

    std::unordered_map<std::string /*field name*/, ClassField> const &
    get_fields() const;

    ClassField &get_field(std::string const &key);

    ClassField const &get_field(std::string const &key) const;

    std::string const &get_name() const;

    std::size_t get_write_position() const;
    void        increment_write_position();
    void        set_super_serialize();

private:
    /**
     * Extract the data with the given class by calling `extract_field_trait` on
     * all the traits associated with the given class.
     */
    void extract_fields(abc::ConstantPoolInfo const &constant_pool_info,
                        abc::ClassInfo const        &class_info,
                        abc::InstanceInfo const     &instance_info);

    /**
     * Create an entry in `m_fields` for the given field.
     * The `protocolId` field is a special case: the entry is not created in the
     * `m_fields` attribute but rather the value is directly loaded into the
     * `m_protocol_id` attribute.
     */
    void extract_field_trait(abc::TraitInfo const        &trait,
                             abc::ConstantPoolInfo const &constant_pool_info);

    /**
     * Extract the information from the getters / setters.
     * This is needed because some private attribute might be serialised (even
     * if it does not really make sense, do not trust Ankama on this...).
     */
    void
    extract_getters_setters(abc::ConstantPoolInfo const &constant_pool_info,
                            abc::InstanceInfo const     &instance_info);

    /**
     * Call the `extract_method` method on all the traits associated with the
     * given class.
     */
    void extract_methods(TagDoAbc const &tag, size_t index);

    /**
     * Extract the fields data from the code of the methods.
     *
     * The used methods are:
     * 1. `serializeAs*` for all the fields information
     * 2. `pack` to know if the class use the HASH_FUNCTION
     */
    void extract_method(TagDoAbc const &tag, abc::TraitInfo const &trait);

    std::unordered_map<std::string /*field name*/, ClassField> m_fields;
    DofusType                                                  m_class;
    DofusType                                                  m_super;
    unsigned long                                              m_protocol_id;
    bool        m_use_hash_function;
    bool        m_super_serialize;
    std::size_t m_current_write_position;
};

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSINFORMATION_H
