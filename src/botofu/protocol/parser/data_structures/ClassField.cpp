#include "ClassField.h"

#include "botofu/protocol/parser/utils/json_helpers.h"

ClassField::ClassField()
      : name(), type(), default_value(), position{static_cast<std::size_t>(-1)},
        self_serialize_method(), write_type_id_method(), write_method(),
        write_length_method(), write_false_if_null_method(), bounds(),
        boolean_byte_wrapper_position(-1),
        constant_length(0), is_vector{false} {
}

json ClassField::to_json(const std::optional<json> &default_values) const {
    json j;
    insert_value(default_values, j, "name", this->name.get_attribute_name());
    insert_value(default_values, j, "type", this->type.get_type());
    insert_value(default_values,
                 j,
                 "type_namespace",
                 this->type.get_namespace().to_string());
    insert_value(default_values, j, "default_value", this->default_value);
    insert_value(default_values, j, "position", this->position);
    insert_value(default_values,
                 j,
                 "self_serialize_method",
                 this->self_serialize_method.get_method_name());
    insert_value(default_values,
                 j,
                 "write_type_id_method",
                 this->write_type_id_method.get_method_name());
    insert_value(default_values,
                 j,
                 "write_method",
                 this->write_method.get_method_name());
    insert_value(default_values,
                 j,
                 "write_length_method",
                 this->write_length_method.get_method_name());
    insert_value(default_values,
                 j,
                 "write_false_if_null_method",
                 this->write_false_if_null_method.get_method_name());
    insert_value(default_values, j, "bounds", this->bounds);
    insert_value(default_values,
                 j,
                 "boolean_byte_wrapper_position",
                 this->boolean_byte_wrapper_position);
    insert_value(default_values, j, "constant_length", this->constant_length);
    insert_value(default_values, j, "is_vector", this->is_vector);
    return j;
}
