#include "Version.h"

json Version::to_json() const {
    return json::array({this->major, this->minor, this->patch, this->build});
}
