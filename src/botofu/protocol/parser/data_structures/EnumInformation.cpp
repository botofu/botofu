#include "EnumInformation.h"

#include "botofu/swf/parser/abc/ClassInfo.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/InstanceInfo.h"

EnumInformation::EnumInformation(TagDoAbc const &tag, uint32 index) {
    abc::ClassInfo const        &class_info{tag.m_classes[index]};
    abc::InstanceInfo const     &instance_info{tag.m_instances[index]};
    abc::ConstantPoolInfo const &constant_pool_info{tag.m_constant_pool_info};

    m_enum_type = DofusType(instance_info.get_name(constant_pool_info));
    m_member_type =
          DofusType(class_info.m_traits.front().get_type(constant_pool_info));
    // For an enumeration, the name of the enumerated values and their values
    // are encoded in the traits: the trait name is the name of the enumerated
    // value and the trait data is the value associated (seems to be everytime
    // an int but I keep it as a string for generality).
    for (auto const &trait : class_info.m_traits) {
        m_members.emplace_back(
              std::make_pair(trait.get_name(constant_pool_info),
                             trait.get_data(constant_pool_info).value()));
    }
}

json EnumInformation::to_json() const {
    json j;
    j["name"]         = m_enum_type.get_type();
    j["entries_type"] = m_member_type.get_type();
    for (auto const &member : m_members) {
        j["members"][member.first] = member.second;
    }
    return j;
}
std::string const &EnumInformation::get_name() const {
    return this->m_enum_type.get_type();
}
