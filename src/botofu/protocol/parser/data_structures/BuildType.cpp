#include "BuildType.h"

std::string build_type_to_string(BuildType const &build_type) {
    switch (build_type) {
    case BuildType::RELEASE:
        return "release";
    case BuildType::DEBUG:
        return "debug";
    case BuildType::UNKNOWN:
    default:
        return "unknown";
    }
}
