#include "Date.h"

#include <chrono>

#include <date/date.h>

std::size_t Date::timestamp() const {
    auto const day_date = date::year_month_day{date::year{this->year},
                                               date::month{this->month},
                                               date::day{this->day}};
    date::time_of_day<std::chrono::minutes> const day_time{
          std::chrono::minutes{this->hour * 60 + this->minute}};
    auto const total_time_minutes{date::local_days{day_date}
                                  + day_time.to_duration()};
    auto const total_time_seconds{
          std::chrono::duration_cast<std::chrono::seconds>(
                total_time_minutes.time_since_epoch())};
    std::size_t const timestamp{
          static_cast<std::size_t>(total_time_seconds.count())};

    return timestamp;
}
