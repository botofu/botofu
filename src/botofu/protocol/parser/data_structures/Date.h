#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DATE_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DATE_H

#include <cstddef>

struct Date {
    int year;
    unsigned int month;
    unsigned int day;
    unsigned int hour;
    unsigned int minute;

    [[nodiscard]] std::size_t timestamp() const;
};

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DATE_H
