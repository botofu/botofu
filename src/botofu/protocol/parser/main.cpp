#include <filesystem>
#include <memory>
#include <optional>

#include <CLI/CLI.hpp>
#include <spdlog/spdlog.h>

#include "botofu/protocol/parser/data_structures/ClassInformation.h"
#include "botofu/protocol/parser/data_structures/EnumInformation.h"
#include "botofu/common/matchers/MetadataFinder.h"
#include "botofu/protocol/parser/utils/CLIOptions.h"
#include "botofu/protocol/parser/version.h"
#include "botofu/swf/parser/SwfFile.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

namespace fs = std::filesystem;

static std::optional<std::shared_ptr<TagDoAbc>>
get_do_abc_tag(fs::path const &dofus_invoker_path) {
    SPDLOG_INFO("Parsing SWF file provided: '{}'...",
                dofus_invoker_path.string());
    SwfFile file(dofus_invoker_path);
    auto    do_abc_tag_optional =
          file.get_first_tag_by_type(swf_constants::TagType::DOABC);
    if (!do_abc_tag_optional) {
        SPDLOG_ERROR("Could not find a DoABC tag in the SWF file provided.");
        return std::nullopt;
    }
    return std::dynamic_pointer_cast<TagDoAbc>(*do_abc_tag_optional);
}

static void extract_network_namespace_data(json &          j,
                                           TagDoAbc const &tag_do_abc,
                                           bool            use_default) {
    ClassField          default_field;
    std::optional<json> default_field_json;
    if (use_default) {
        default_field_json    = default_field.to_json();
        j["default"]["field"] = *default_field_json;
    } else {
        default_field_json    = std::nullopt;
        j["default"]["field"] = {};
    }

    // 6. Extract the network data
    SPDLOG_INFO("Extracting network data from {} classes definitions...",
                tag_do_abc.m_instances.size());
    abc::ConstantPoolInfo const &constant_pool_info{
          tag_do_abc.m_constant_pool_info};
    for (uint32_t i{0}; i < tag_do_abc.m_instances.size(); ++i) {
        abc::InstanceInfo const &instance_info{tag_do_abc.m_instances[i]};
        // Do not consider the instances outside of the network namespace
        if (!instance_info.is_in_network_namespace(constant_pool_info)) {
            continue;
        }
        // Separate the enumerations & the messages/types
        if (instance_info.is_network_enumeration(constant_pool_info)) {
            EnumInformation enum_information(tag_do_abc, i);
            j["enumerations"][enum_information.get_name()] =
                  enum_information.to_json();
        } else if (instance_info.is_network_message(constant_pool_info)) {
            ClassInformation message_information(tag_do_abc, i);
            j["messages"][message_information.get_name()] =
                  message_information.to_json(default_field_json);
        } else if (instance_info.is_network_type(constant_pool_info)) {
            ClassInformation type_information(tag_do_abc, i);
            j["types"][type_information.get_name()] =
                  type_information.to_json(default_field_json);
        }
    }
}

static void export_to_stream(std::ostream &out, json const &j, int indent) {
    if (indent == 0) {
        out << j.dump();
    } else {
        out << j.dump(indent);
    }
}

static void
export_json(json const &j, int indent, fs::path const &export_path) {
    SPDLOG_INFO("Exporting {} enumerations, {} messages and {} types to '{}'.",
                j["enumerations"].size(),
                j["messages"].size(),
                j["types"].size(),
                export_path.empty() ? "stdout" : export_path.string());
    if (export_path.empty()) {
        export_to_stream(std::cout, j, indent);
    } else {
        std::ofstream out(export_path);
        export_to_stream(out, j, indent);
    }
}

int main(int argc, char *argv[]) {
    // 1. Command line interface.
    CLIOptions cli_options(argc, argv);
    CLI::App   app("Protocol parser for the Botofu project. Version "
                 + std::to_string(PROTOCOL_PARSER_MAJOR_VERSION) + "."
                 + std::to_string(PROTOCOL_PARSER_MINOR_VERSION) + ".");
    app.add_option("DofusInvokerPath",
                   cli_options.dofus_invoker_path,
                   "Path to the DofusInvoker.swf")
          ->required()
          ->check(CLI::ExistingFile);
    app.add_option("ExportPath,-o,--out",
                   cli_options.target_json_path,
                   "Path to the file that will contain the output of the "
                   "protocol parsing. "
                   "If not provided, dump to the standard output.");
    app.add_option("-i,--indent",
                   cli_options.indent,
                   "Number of spaces used to indent resulting JSON. Default to "
                   "minified (0).");
    app.add_option("--name",
                   cli_options.parsing_name,
                   "Custom name for the parsing that will be included in the "
                   "JSON metadata.");
    app.add_option(
          "-t,--tag",
          cli_options.tags,
          "A list of tags that will be included in the generated JSON.");
    app.add_flag("-a,--all,--no-default",
                 cli_options.no_default_optimisation,
                 "No \"default\" entry in the resulting JSON. All the "
                 "keys will be "
                 "explicitly written, with a default value for the "
                 "non-set keys.");

    CLI11_PARSE(app, argc, argv)

    // 2. Load the SWF file provided and recover the DoABC tag.
    auto optional_tag_do_abc = get_do_abc_tag(cli_options.dofus_invoker_path);
    if (!optional_tag_do_abc) {
        return 1;
    }
    std::shared_ptr<TagDoAbc> const tag_do_abc = *optional_tag_do_abc;

    // 3. Create the JSON that will gather the information
    json all_data;

    // 4. Extract all the versions available
    SPDLOG_INFO("Extracting metadata...");
    MetadataFinder metadata_finder(*tag_do_abc, cli_options);
    all_data["metadata"] = metadata_finder.to_json();

    // 5. Extract the network namespace data
    extract_network_namespace_data(
          all_data, *tag_do_abc, !cli_options.no_default_optimisation);

    // 6. Export the JSON to the given file.
    export_json(all_data, cli_options.indent, cli_options.target_json_path);
}
