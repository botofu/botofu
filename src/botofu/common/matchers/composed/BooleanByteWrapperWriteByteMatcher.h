#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERWRITEBYTEMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERWRITEBYTEMATCHER_H

#include <memory>

#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/BooleanByteWrapperWriteByteCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.writeByte([unknown_name]);
 *  ================================
 */
struct BooleanByteWrapperWriteByteMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    BooleanByteWrapperWriteByteMatcher(
          abc::ConstantPoolInfo const &constant_pool_info,
          DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<BooleanByteWrapperWriteByteCoreMatcher>
          m_boolean_byte_wrapper_write_byte_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERWRITEBYTEMATCHER_H
