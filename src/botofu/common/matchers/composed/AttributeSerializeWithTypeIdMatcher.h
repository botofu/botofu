#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTESERIALIZEWITHTYPEIDMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTESERIALIZEWITHTYPEIDMATCHER_H

#include <memory>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/AttributeSerializeCoreMatcher.h"
#include "botofu/common/matchers/core/DebugLineInstructionCoreMatcher.h"
#include "botofu/common/matchers/core/WriteAttributeTypeIdCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.[writemethod](this.[attribute].getTypeId());
 *  this.[attribute].[serialize/serializeAs_...](output);
 *  ================================
 */
struct AttributeSerializeWithTypeIdMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    AttributeSerializeWithTypeIdMatcher(
          abc::ConstantPoolInfo const &constant_pool_info,
          DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<WriteAttributeTypeIdCoreMatcher> m_write_type_id_matcher;
    std::shared_ptr<AttributeSerializeCoreMatcher>
          m_attribute_serialize_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTESERIALIZEWITHTYPEIDMATCHER_H
