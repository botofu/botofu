#include "RuntimeLenListSerializeAsMatcher.h"

#include <botofu/protocol/parser/data_structures/ClassInformation.h>
#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE)                         \
    MEMBER(std::make_shared<TYPE>(constant_pool_info))

RuntimeLenListSerializeAsMatcher::RuntimeLenListSerializeAsMatcher(
      abc::ConstantPoolInfo const &constant_pool_info,
      DofusMethod const           &method)
      : BaseComposedMatcher(constant_pool_info, method),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_debugline_matcher,
                                         DebugLineInstructionCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(
              m_write_attribute_attribute_method_matcher,
              WriteAttributeAttributeMethodCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_for_loop_start_matcher,
                                         ForLoopStartCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(
              m_serialize_call_with_type_manager_matcher,
              SerializeCallWithTypeManagerCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(
              m_for_loop_end_attribute_attribute_matcher,
              ForLoopEndAttributeAttributeCoreMatcher) {
    this->add_matcher(m_write_attribute_attribute_method_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_for_loop_start_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_serialize_call_with_type_manager_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_for_loop_end_attribute_attribute_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void RuntimeLenListSerializeAsMatcher::update_output(
      ClassInformation &current_class_information) const {
    SPDLOG_DEBUG("Matched TypeManager list serialisation starting at "
                 "instruction n°{} of method '{}'.",
                 m_write_attribute_attribute_method_matcher
                       ->get_match_instruction_index(),
                 p_method.to_string());
    DofusAttribute const &attribute =
          m_write_attribute_attribute_method_matcher->attribute;
    if (!this->check_field(current_class_information.get_fields(), attribute)) {
        return;
    }
    ClassField &field =
          current_class_information.get_field(attribute.get_attribute_name());
    field.self_serialize_method =
          m_serialize_call_with_type_manager_matcher->method;
    field.write_length_method =
          m_write_attribute_attribute_method_matcher->method;
    field.position = current_class_information.get_write_position();
    current_class_information.increment_write_position();
    this->update_vector_type_attribute(field);
}
