#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_RUNTIMELENLISTNUMERICTYPE_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_RUNTIMELENLISTNUMERICTYPE_H

#include <memory>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>

#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/ArrayAttributeIndexingWriteCoreMatcher.h"
#include "botofu/common/matchers/core/DebugLineInstructionCoreMatcher.h"
#include "botofu/common/matchers/core/ForLoopEndAttributeAttributeCoreMatcher.h"
#include "botofu/common/matchers/core/ForLoopStartCoreMatcher.h"
#include "botofu/common/matchers/core/WriteAttributeAttributeMethodCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.[writemethod1]](this.[attribute].length);
 *  for(var [i]:uint = 0; [i]] < this.[attribute].length; [i]++)
 *  {
 *      output.[writemethod2](this.[attribute][[i]]);
 *  }
 *  ================================
 */
struct RuntimeLenListNumericType final
      : public BaseComposedMatcher<ClassInformation> {
    RuntimeLenListNumericType(abc::ConstantPoolInfo const &constant_pool_info,
                              DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<WriteAttributeAttributeMethodCoreMatcher>
          m_write_attribute_attribute_matcher;
    std::shared_ptr<ForLoopStartCoreMatcher> m_for_loop_start_matcher;
    std::shared_ptr<ArrayAttributeIndexingWriteCoreMatcher>
          m_array_indexing_matcher;
    std::shared_ptr<ForLoopEndAttributeAttributeCoreMatcher>
          m_for_loop_end_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_RUNTIMELENLISTNUMERICTYPE_H
