#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_UNMATCHEDWRITEMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_UNMATCHEDWRITEMATCHER_H

#include <memory>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/SingleCallPropVoidCoreMatcher.h"

/**
 * Match the unmatched calls to
 *
 *  ================================
 *  output.[writemethod/serialize/serializeAs](this.[attribute])
 *  ================================
 */
struct UnmatchedWriteMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    UnmatchedWriteMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                          DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<SingleCallPropVoidCoreMatcher> m_prop_void_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_UNMATCHEDWRITEMATCHER_H
