#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_CHECKIFNULLSERIALIZEASMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_CHECKIFNULLSERIALIZEASMATCHER_H

#include <memory>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>

#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/CheckIfNullSerializeAsCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  if(this.[attribute] == null)
 *  {
 *      output.[writemethod1](0);
 *  }
 *  else
 *  {
 *      output.[writemethod1](1);
 *      this.[attribute].[serialize/serializeAs](output);
 *  }
 *  ================================
 */
struct CheckIfNullSerializeAsMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    CheckIfNullSerializeAsMatcher(
          abc::ConstantPoolInfo const &constant_pool_info,
          DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<CheckIfNullSerializeAsCoreMatcher>
          m_check_if_null_serialize_as_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_CHECKIFNULLSERIALIZEASMATCHER_H
