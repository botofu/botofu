#include "AttributeWriteBothBoundChecksMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE)                         \
    MEMBER(std::make_shared<TYPE>(constant_pool_info))

AttributeWriteBothBoundChecksMatcher::AttributeWriteBothBoundChecksMatcher(
      abc::ConstantPoolInfo const &constant_pool_info,
      DofusMethod const           &method)
      : BaseComposedMatcher(constant_pool_info, method),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_debugline_matcher,
                                         DebugLineInstructionCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_comparison_less_than_matcher,
                                         ComparisonLessThanCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_logical_or_macher,
                                         LogicalOrCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_comparison_greater_than_matcher,
                                         ComparisonGreaterThanCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_if_false_instruction_matcher,
                                         IfFalseInstructionCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_throw_matcher,
                                         ThrowExceptionCoreMatcher),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_write_attribute_matcher,
                                         WriteAttributeMethodCoreMatcher) {
    this->add_matcher(m_comparison_less_than_matcher);
    this->add_matcher(m_logical_or_macher);
    this->add_matcher(m_comparison_greater_than_matcher);
    this->add_matcher(m_if_false_instruction_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_throw_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_write_attribute_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void AttributeWriteBothBoundChecksMatcher::update_output(
      ClassInformation &current_class_information) const {
    SPDLOG_DEBUG("Found write of numeric attribute with both bounds checks at "
                 "instruction n°{} in '{}'.",
                 m_comparison_less_than_matcher->get_match_instruction_index(),
                 p_method.to_string());
    DofusAttribute const &attribute = m_write_attribute_matcher->attribute;
    if (!this->check_field(current_class_information.get_fields(), attribute)) {
        return;
    }
    ClassField &field =
          current_class_information.get_field(attribute.get_attribute_name());
    field.bounds.lower = m_comparison_less_than_matcher->limits.lower;
    field.bounds.upper = m_comparison_greater_than_matcher->limits.upper;
    field.write_method = m_write_attribute_matcher->method;
    field.position     = current_class_information.get_write_position();
    current_class_information.increment_write_position();
}
