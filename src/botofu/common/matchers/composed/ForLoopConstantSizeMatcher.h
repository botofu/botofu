#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_FORLOOPCONSTANTSIZEMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_FORLOOPCONSTANTSIZEMATCHER_H

#include <memory>

#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/ArrayAttributeIndexingWriteCoreMatcher.h"
#include "botofu/common/matchers/core/DebugLineInstructionCoreMatcher.h"
#include "botofu/common/matchers/core/ForLoopEndConstantSizeCoreMatcher.h"
#include "botofu/common/matchers/core/ForLoopStartCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  for(var [i]:uint = 0; [i]] < [constant]; [i]++)
 *  {
 *      output.[writemethod](this.[attribute][[i]]);
 *  }
 *  ================================
 */
struct ForLoopConstantSizeMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    ForLoopConstantSizeMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                               DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<ForLoopStartCoreMatcher> m_for_loop_start_matcher;
    std::shared_ptr<ArrayAttributeIndexingWriteCoreMatcher>
                                                       m_array_indexing_matcher;
    std::shared_ptr<ForLoopEndConstantSizeCoreMatcher> m_for_loop_end_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_FORLOOPCONSTANTSIZEMATCHER_H
