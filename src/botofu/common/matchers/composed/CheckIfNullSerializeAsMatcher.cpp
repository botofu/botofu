#include "CheckIfNullSerializeAsMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE)                         \
    MEMBER(std::make_shared<TYPE>(constant_pool_info))

CheckIfNullSerializeAsMatcher::CheckIfNullSerializeAsMatcher(
      abc::ConstantPoolInfo const &constant_pool_info,
      DofusMethod const           &method)
      : BaseComposedMatcher(constant_pool_info, method),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_check_if_null_serialize_as_matcher,
                                         CheckIfNullSerializeAsCoreMatcher) {
    this->add_matcher(m_check_if_null_serialize_as_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void CheckIfNullSerializeAsMatcher::update_output(
      ClassInformation &current_class_information) const {
    SPDLOG_DEBUG(
          "Matched null-checked write at instruction n°{} in function '{}'.",
          m_check_if_null_serialize_as_matcher->get_match_instruction_index(),
          p_method.to_string());
    DofusAttribute const &attribute =
          m_check_if_null_serialize_as_matcher->attribute;
    if (!this->check_field(current_class_information.get_fields(), attribute)) {
        return;
    }
    ClassField &field =
          current_class_information.get_field(attribute.get_attribute_name());
    field.self_serialize_method = m_check_if_null_serialize_as_matcher->method;
    field.write_false_if_null_method =
          m_check_if_null_serialize_as_matcher->is_null_write_method;
    field.position = current_class_information.get_write_position();
    current_class_information.increment_write_position();
}
