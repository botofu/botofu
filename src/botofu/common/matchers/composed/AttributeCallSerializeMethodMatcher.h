#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTECALLSERIALIZEMETHODMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTECALLSERIALIZEMETHODMATCHER_H

#include <memory>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/AttributeSerializeCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  this.[attribute].[serialize/serializeAs_...](output);
 *  ================================
 */
struct AttributeCallSerializeMethodMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    AttributeCallSerializeMethodMatcher(
          abc::ConstantPoolInfo const &constant_pool_info,
          DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<AttributeSerializeCoreMatcher>
          m_attribute_serialize_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTECALLSERIALIZEMETHODMATCHER_H
