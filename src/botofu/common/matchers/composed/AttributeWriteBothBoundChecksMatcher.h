#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTEWRITEBOTHBOUNDCHECKSMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTEWRITEBOTHBOUNDCHECKSMATCHER_H

#include <memory>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>

#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/ComparisonGreaterThanCoreMatcher.h"
#include "botofu/common/matchers/core/ComparisonLessThanCoreMatcher.h"
#include "botofu/common/matchers/core/DebugLineInstructionCoreMatcher.h"
#include "botofu/common/matchers/core/IfFalseInstructionCoreMatcher.h"
#include "botofu/common/matchers/core/LogicalOrCoreMatcher.h"
#include "botofu/common/matchers/core/ThrowExceptionCoreMatcher.h"
#include "botofu/common/matchers/core/WriteAttributeMethodCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  if([attribute] < [val1] || [attribute] > [val2]) {
 *      throw new Error("Forbidden value (" + this.[attribute] + ") on element
 * sourceId.");
 *  }
 *  output.[writemethod](this.[attribute]);
 *  ================================
 */
struct AttributeWriteBothBoundChecksMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    explicit AttributeWriteBothBoundChecksMatcher(
          abc::ConstantPoolInfo const &constant_pool_info,
          DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<ComparisonLessThanCoreMatcher>
                                          m_comparison_less_than_matcher;
    std::shared_ptr<LogicalOrCoreMatcher> m_logical_or_macher;
    std::shared_ptr<ComparisonGreaterThanCoreMatcher>
          m_comparison_greater_than_matcher;
    std::shared_ptr<IfFalseInstructionCoreMatcher>
                                               m_if_false_instruction_matcher;
    std::shared_ptr<ThrowExceptionCoreMatcher> m_throw_matcher;
    std::shared_ptr<WriteAttributeMethodCoreMatcher> m_write_attribute_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTEWRITEBOTHBOUNDCHECKSMATCHER_H
