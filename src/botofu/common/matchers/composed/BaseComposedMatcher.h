#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_BASECOMPOSEDMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_BASECOMPOSEDMATCHER_H

#include <memory>
#include <unordered_map>
#include <vector>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>
#include <spdlog/spdlog.h>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/data_structures/ClassField.h"

template <typename OutputType>
struct BaseComposedMatcher {
    using FieldsType =
          std::unordered_map<std::string /*field name*/, ClassField>;

    BaseComposedMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                        DofusMethod const           &method)
          : p_constant_pool_info(constant_pool_info), p_core_matchers(),
            p_method(method), m_pattern_length{0} {
        for (auto const &matcher : p_core_matchers) {
            this->m_pattern_length += matcher->size();
        }
    }

    /**
     * @brief Adds the given core matcher to the ComposedMatcher.
     * @param matcher the core matcher to add.
     */
    void add_matcher(const std::shared_ptr<BaseCoreMatcher> &matcher) {
        this->m_pattern_length += matcher->size();
        this->p_core_matchers.push_back(matcher);
    }

    /**
     * @brief Constructs an instance of the given @p MatcherType with the
     * provided
     * @p args and adds it to the ComposedMatcher instance.
     * @tparam MatcherType type of the matcher to construct and add
     * @tparam Args types of the arguments forwarded to the matcher constructor
     * @param args arguments forwarded to the matched constructor
     */
    template <typename MatcherType, typename... Args>
    void add_matcher(Args... args) {
        this->add_matcher(
              std::make_unique<MatcherType>(std::forward<Args>(args)...));
    }

    /**
     * @brief Find the pattern stored by the instance in the given sequence of
     * instructions.
     * @param instructions the list of instructions we want to search in.
     * @param start index of the first considered instruction in @p instruction.
     * @return the index of the first instruction matching the pattern or
     * instruction.size() if the pattern could not be found in the instruction
     * list.
     */
    [[nodiscard]] std::size_t
    find_pattern(std::vector<Instr> const &instructions,
                 std::size_t               start) const {
        // If there are not enough instructions to match, return a "no match".
        if (instructions.size() - start <= m_pattern_length) {
            return instructions.size();
        }

        std::size_t const max_start_search{instructions.size()
                                           - m_pattern_length + 1};
        for (std::size_t i{start}; i < max_start_search; ++i) {
            if (this->match_pattern(instructions, i)) {
                return i;
            }
        }
        return instructions.size();
    }

    /**
     * @brief Find the pattern stored by the instance in the given sequence of
     * instructions and update the corresponding matchers
     * @param instructions the list of instructions we want to search in.
     * @param start index of the first considered instruction in @p instruction.
     * @return the index of the first instruction matching the pattern or
     * instruction.size() if the pattern could not be found in the instruction
     * list.
     */
    [[nodiscard]] std::size_t
    find_pattern_and_update_matchers(std::vector<Instr> const &instructions,
                                     std::size_t               start) {
        std::size_t const pattern_found{
              this->find_pattern(instructions, start)};
        if (pattern_found < instructions.size()) {
            this->update_matchers(instructions, pattern_found);
        }
        return pattern_found;
    }

    /**
     * @brief Check if the instructions starting at @p start match the stored
     * pattern.
     * @param instructions the list of instructions we want to search in.
     * @param start index of the first considered instruction in @p instruction.
     * @return true if instructions[start] and the following instructions match
     * the provided pattern, else false.
     */
    [[nodiscard]] bool match_pattern(std::vector<Instr> const &instructions,
                                     std::size_t               start) const {
        for (auto const &matcher : p_core_matchers) {
            if (matcher->match_pattern(instructions, start)) {
                start += matcher->size();
            } else {   // No match
                return false;
            }
        }
        return true;
    }

    /**
     * @brief Update the @p output structure according to the matched pattern
     * @param output structure containing the information on the class we are
     * currently analysing.
     */
    virtual void update_output(OutputType &output) const = 0;

    /**
     * @brief Update the internal CoreMatchers
     * @param instructions instructions
     * @param match_position index of the first instruction of a match
     * @pre this->match_pattern(instructions, match_position) == true
     */
    virtual void update_matchers(std::vector<Instr> const &instructions,
                                 std::size_t               match_position) {
        for (auto &matcher : p_core_matchers) {
            matcher->update(instructions, match_position);
            match_position += matcher->size();
        }
    }

    /**
     * @brief Return the size of the pattern matched by the instance
     * @return the size of the pattern matched by the instance
     */
    [[nodiscard]] std::size_t size() const {
        return m_pattern_length;
    }

protected:
    [[nodiscard]] bool
    check_field(const BaseComposedMatcher::FieldsType &fields,
                DofusAttribute const                  &attribute) const {
        std::string const &field_name{attribute.get_attribute_name()};
        if (fields.find(field_name) == fields.end()) {
            SPDLOG_ERROR(
                  "Field '{}' not registered when calling update_fields.",
                  attribute.to_string());
            return false;
        }
        return true;
    }

    void update_vector_type_attribute(ClassField &field) const {
        if (!field.type.is_vector()) {
            // The ByteArray type is an exception. In the RawDataMessage it is
            // serialised with a for-loop, but is not a Vector type!
            // We only warn if the current type is not ByteArray either.
            if (!field.type.is_bytearray()) {
                SPDLOG_WARN("Found a non-vector type ('{}') in a for loop in "
                            "method '{}'.",
                            field.type.get_type(),
                            p_method.to_string());
            }
            return;
        }
        constexpr std::size_t vector_type_prefix_length{7};
        std::string const    &vector_type(field.type.get_type());
        DofusType const       in_vector_type(vector_type.substr(
              vector_type_prefix_length,
              vector_type.length() - (vector_type_prefix_length + 1)));
        field.type      = in_vector_type;
        field.is_vector = true;
    }

    abc::ConstantPoolInfo const                  &p_constant_pool_info;
    std::vector<std::shared_ptr<BaseCoreMatcher>> p_core_matchers;
    DofusMethod const                            &p_method;

private:
    std::size_t m_pattern_length;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_BASECOMPOSEDMATCHER_H
