#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_RUNTIMELENLISTTYPEMANAGERWRITEMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_RUNTIMELENLISTTYPEMANAGERWRITEMATCHER_H

#include <memory>

#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/DebugLineInstructionCoreMatcher.h"
#include "botofu/common/matchers/core/ForLoopEndAttributeAttributeCoreMatcher.h"
#include "botofu/common/matchers/core/ForLoopStartCoreMatcher.h"
#include "botofu/common/matchers/core/SerializeCallWithTypeManagerCoreMatcher.h"
#include "botofu/common/matchers/core/WriteAttributeAttributeMethodCoreMatcher.h"
#include "botofu/common/matchers/core/WriteTypeWithTypeManagerCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.[writemethod1]](this.[attribute].length);
 *  for(var [i]:uint = 0; [i]] < this.[attribute].length; [i]++)
 *  {
 *      output.[writemethod2]((this.[attribute][[i]] as [...]).getTypeId());
 *      (this.[attribute][[i] as [...]).serializeAs_[...](output);
 *  }
 *  ================================
 */
struct RuntimeLenListTypeManagerWriteMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    RuntimeLenListTypeManagerWriteMatcher(
          abc::ConstantPoolInfo const &constant_pool_info,
          DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<WriteAttributeAttributeMethodCoreMatcher>
          m_write_attribute_attribute_method_matcher;
    std::shared_ptr<ForLoopStartCoreMatcher> m_for_loop_start_matcher;
    std::shared_ptr<WriteTypeWithTypeManagerCoreMatcher>
          m_write_type_with_type_manager_matcher;
    std::shared_ptr<SerializeCallWithTypeManagerCoreMatcher>
          m_serialize_call_with_type_manager_matcher;
    std::shared_ptr<ForLoopEndAttributeAttributeCoreMatcher>
          m_for_loop_end_attribute_attribute_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_RUNTIMELENLISTTYPEMANAGERWRITEMATCHER_H
