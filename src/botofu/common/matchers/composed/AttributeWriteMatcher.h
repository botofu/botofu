#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTEWRITEMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTEWRITEMATCHER_H

#include <memory>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/WriteAttributeMethodCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.[writemethod](this.[attribute]);
 *  ================================
 */
struct AttributeWriteMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    AttributeWriteMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                          DofusMethod const           &method);

    void update_output(ClassInformation &output) const final;

private:
    std::shared_ptr<WriteAttributeMethodCoreMatcher>
          m_write_attribute_method_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_ATTRIBUTEWRITEMATCHER_H
