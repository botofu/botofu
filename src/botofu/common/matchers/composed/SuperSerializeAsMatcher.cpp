#include "SuperSerializeAsMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE)                         \
    MEMBER(std::make_shared<TYPE>(constant_pool_info))

SuperSerializeAsMatcher::SuperSerializeAsMatcher(
      abc::ConstantPoolInfo const &constant_pool_info,
      DofusMethod const           &method)
      : BaseComposedMatcher(constant_pool_info, method),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_serialize_as_attribute_matcher,
                                         SuperSerializeAsCoreMatcher) {
    this->add_matcher(m_serialize_as_attribute_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void SuperSerializeAsMatcher::update_output(
      ClassInformation &current_class_information) const {
    SPDLOG_DEBUG(
          "Matched '{}' at instruction n°{} of method '{}'.",
          m_serialize_as_attribute_matcher->method.to_string(),
          m_serialize_as_attribute_matcher->get_match_instruction_index(),
          p_method.to_string());
    // Not linked to an attribute. We do not increase the number of writes.
    current_class_information.set_super_serialize();
}
