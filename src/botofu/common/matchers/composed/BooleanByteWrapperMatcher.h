#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERMATCHER_H

#include <memory>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/BooleanByteWrapperSetFlagCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  [unknown_name] = BooleanByteWrapper.setFlag([unknown_name], [bit_index],
 * this.[attribute]);
 *  ================================
 */
struct BooleanByteWrapperMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    BooleanByteWrapperMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                              DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

    void update_matchers(std::vector<Instr> const &instructions,
                         std::size_t               match_position) final;

private:
    std::size_t m_number_of_bit_fields_found_yet;
    std::shared_ptr<BooleanByteWrapperSetFlagCoreMatcher>
          m_boolean_byte_wrapper_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERMATCHER_H
