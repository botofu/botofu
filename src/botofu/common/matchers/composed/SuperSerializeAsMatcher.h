#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_SUPERSERIALIZEASMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_SUPERSERIALIZEASMATCHER_H

#include <memory>

#include <botofu/protocol/parser/data_structures/ClassInformation.h>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/common/matchers/core/SuperSerializeAsCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  super.serializeAs_[...](output);
 *  ================================
 */
struct SuperSerializeAsMatcher final
      : public BaseComposedMatcher<ClassInformation> {
    SuperSerializeAsMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                            DofusMethod const           &method);

    void update_output(ClassInformation &current_class_information) const final;

private:
    std::shared_ptr<SuperSerializeAsCoreMatcher>
          m_serialize_as_attribute_matcher;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_COMPOSED_SUPERSERIALIZEASMATCHER_H
