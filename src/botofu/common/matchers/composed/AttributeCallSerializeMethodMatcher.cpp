#include "AttributeCallSerializeMethodMatcher.h"

#include <botofu/protocol/parser/data_structures/ClassInformation.h>
#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE)                         \
    MEMBER(std::make_shared<TYPE>(constant_pool_info))

AttributeCallSerializeMethodMatcher::AttributeCallSerializeMethodMatcher(
      abc::ConstantPoolInfo const &constant_pool_info,
      DofusMethod const           &method)
      : BaseComposedMatcher(constant_pool_info, method),
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_attribute_serialize_matcher,
                                         AttributeSerializeCoreMatcher) {
    this->add_matcher(m_attribute_serialize_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void AttributeCallSerializeMethodMatcher::update_output(
      ClassInformation &current_class_information) const {
    SPDLOG_DEBUG("Matched '{}' at instruction n°{} of method '{}'.",
                 m_attribute_serialize_matcher->method.to_string(),
                 m_attribute_serialize_matcher->get_match_instruction_index(),
                 p_method.to_string());
    DofusAttribute const &attribute = m_attribute_serialize_matcher->attribute;
    if (!this->check_field(current_class_information.get_fields(), attribute)) {
        return;
    }
    ClassField &field =
          current_class_information.get_field(attribute.get_attribute_name());
    field.self_serialize_method = m_attribute_serialize_matcher->method;
    field.position = current_class_information.get_write_position();
    current_class_information.increment_write_position();
}
