#include "BooleanByteWrapperMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE)                         \
    MEMBER(std::make_shared<TYPE>(constant_pool_info))

BooleanByteWrapperMatcher::BooleanByteWrapperMatcher(
      abc::ConstantPoolInfo const &constant_pool_info,
      DofusMethod const           &method)
      : BaseComposedMatcher(constant_pool_info, method),
        m_number_of_bit_fields_found_yet{0},
        BOTOFU_INITIALISE_MATCHER_MEMBER(m_boolean_byte_wrapper_matcher,
                                         BooleanByteWrapperSetFlagCoreMatcher) {
    this->add_matcher(m_boolean_byte_wrapper_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void BooleanByteWrapperMatcher::update_output(
      ClassInformation &current_class_information) const {
    SPDLOG_DEBUG(
          "Matched a BooleanByteWrapper.setFlag at instruction n°'{}' of "
          "method '{}'.",
          m_boolean_byte_wrapper_matcher->get_match_instruction_index(),
          p_method.to_string());
    DofusAttribute const &attribute = m_boolean_byte_wrapper_matcher->attribute;
    if (!this->check_field(current_class_information.get_fields(), attribute)) {
        return;
    }
    ClassField &field =
          current_class_information.get_field(attribute.get_attribute_name());
    // static_cast is safe here because we do not expect any method that uses
    // 2^31 flags with the BooleanByteWrapper
    field.boolean_byte_wrapper_position =
          static_cast<int>(this->m_boolean_byte_wrapper_matcher->bit_index);
    field.position = current_class_information.get_write_position();
    // Do not increase the write position here as we do not write anything.
}

void BooleanByteWrapperMatcher::update_matchers(
      std::vector<Instr> const &instructions, std::size_t match_position) {
    BaseComposedMatcher::update_matchers(instructions, match_position);
    if (this->m_number_of_bit_fields_found_yet % 8
        != m_boolean_byte_wrapper_matcher->bit_index) {
        SPDLOG_WARN(
              "The internal computation of the bit-index does not agree "
              "with the parsed bit index. I computed {}, the bytecode said {}.",
              this->m_number_of_bit_fields_found_yet % 8,
              this->m_boolean_byte_wrapper_matcher->bit_index);
    }
    ++this->m_number_of_bit_fields_found_yet;
}
