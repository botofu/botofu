#include "botofu/common/matchers/instructions/InstructionMatcher.h"

#include <cstddef>
#include <optional>
#include <vector>

InstructionMatcher::InstructionMatcher(
      std::string                              instruction_name_regex,
      std::regex_constants::syntax_option_type flag)
      : m_instruction_name_regex(instruction_name_regex, flag),
        m_operand_checks() {
}

InstructionMatcher::InstructionMatcher(
      std::string                              instruction_name_regex,
      std::vector<OperandCheckCallable>        operand_checks,
      std::regex_constants::syntax_option_type flag)
      : m_instruction_name_regex(instruction_name_regex, flag),
        m_operand_checks(operand_checks) {
}

bool InstructionMatcher::matches(
      Instr const                 &instruction,
      abc::ConstantPoolInfo const &constant_pool_info) const {
    if (!std::regex_match(instruction.model.name,
                          this->m_instruction_name_regex)) {
        return false;
    }
    if (!this->m_operand_checks.has_value()) {
        return true;
    }
    std::vector<OperandCheckCallable> const &operand_checks{
          this->m_operand_checks.value()};
    if (operand_checks.size() != instruction.operands.size()) {
        return false;
    }

    for (std::size_t i{0}; i < instruction.operands.size(); ++i) {
        if (!operand_checks[i](instruction.operands[i], constant_pool_info)) {
            return false;
        }
    }
    return true;
}
