#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_INSTRUCTIONS_PUSH_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_INSTRUCTIONS_PUSH_H

#include <string>

#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"

enum struct PushInstructionType { BYTE, DOUBLE, INT, UINT, SHORT, UNKNOWN };

PushInstructionType get_push_type_from_name(std::string const &name);

std::string get_push_representation_from_type(
      PushInstructionType          type,
      abc::ConstantPoolInfo const &constant_pool_info,
      Instr const                 &instruction);

std::string const PUSH_INSTRUCTION_REGEX{
      "push((byte)|(double)|(int)|(uint)|(short))"};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_INSTRUCTIONS_PUSH_H
