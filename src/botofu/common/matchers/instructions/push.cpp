#include "push.h"

PushInstructionType get_push_type_from_name(std::string const &name) {
    if (name == "pushdouble") {
        return PushInstructionType::DOUBLE;
    } else if (name == "pushint") {
        return PushInstructionType::INT;
    } else if (name == "pushuint") {
        return PushInstructionType::UINT;
    } else if (name == "pushbyte") {
        return PushInstructionType::BYTE;
    } else if (name == "pushshort") {
        return PushInstructionType::SHORT;
    }
    return PushInstructionType::UNKNOWN;
}

std::string get_push_representation_from_type(
      PushInstructionType          type,
      abc::ConstantPoolInfo const &constant_pool_info,
      Instr const &                instruction) {
    uint32 const val{instruction.operands[0]};
    switch (type) {
    case PushInstructionType::DOUBLE:
        return std::to_string(constant_pool_info.get_double(val));
    case PushInstructionType::INT:
        return std::to_string(constant_pool_info.get_integer(val));
    case PushInstructionType::UINT:
        return std::to_string(constant_pool_info.get_uinteger(val));
    case PushInstructionType::BYTE:
        return std::to_string(static_cast<int8>(val));
    case PushInstructionType::SHORT:
        return std::to_string(static_cast<int16>(val));
    case PushInstructionType::UNKNOWN:
    default:
        return std::string("UNKNOWN");
    }
}
