#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_INSTRUCTIONS_INSTRUCTIONMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_INSTRUCTIONS_INSTRUCTIONMATCHER_H

#include <functional>
#include <optional>
#include <regex>
#include <string>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"

class InstructionMatcher {
    using OperandCheckCallable =
          std::function<bool(uint32, abc::ConstantPoolInfo const &)>;

public:
    InstructionMatcher(std::string instruction_name_regex,
                       std::regex_constants::syntax_option_type flag =
                             std::regex_constants::optimize
                             | std::regex_constants::extended);

    InstructionMatcher(std::string                       instruction_name_regex,
                       std::vector<OperandCheckCallable> operand_checks,
                       std::regex_constants::syntax_option_type flag =
                             std::regex_constants::optimize
                             | std::regex_constants::extended);

    bool matches(Instr const                 &instruction,
                 abc::ConstantPoolInfo const &constant_pool_info) const;

private:
    std::regex const                                 m_instruction_name_regex;
    std::optional<std::vector<OperandCheckCallable>> m_operand_checks;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_INSTRUCTIONS_INSTRUCTIONMATCHER_H
