#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_INSTRUCTIONS_IO_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_INSTRUCTIONS_IO_H

#include <string>

enum struct IOMethodType {
    NOTSET,
    BOOL,
    INT8,
    UINT8,
    INT16,
    UINT16,
    INT32,
    UINT32,
    INT64,
    UINT64,
    FLOAT,
    DOUBLE,
    UTF,
    BYTES,
    UNKNOWN,
};

IOMethodType get_io_method_type_from_name(std::string const &name);

bool is_var_io_method_from_name(std::string const &name);

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_INSTRUCTIONS_IO_H
