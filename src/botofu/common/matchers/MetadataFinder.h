#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_METADATAFINDER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_METADATAFINDER_H

#include <filesystem>

#include <nlohmann/json.hpp>

#include "botofu/protocol/parser/data_structures/BuildType.h"
#include "botofu/protocol/parser/data_structures/Date.h"
#include "botofu/protocol/parser/data_structures/Version.h"
#include "botofu/protocol/parser/utils/CLIOptions.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

namespace fs = std::filesystem;

using json = nlohmann::json;

/**
 * Encapsulate the research of version numbers, build type and build dates into
 * a single class.
 *
 * When multiple sources are supposed to agree on the versions, the agreement
 * will be checked and a warning will be issued in case of disagreement.
 */
struct MetadataFinder {
    MetadataFinder(TagDoAbc const &tag_do_abc, CLIOptions const &cli_options);

    [[nodiscard]] json to_json() const;

    Version           build_version;
    BuildType         build_type;
    Date              build_date;
    std::size_t       protocol_version;
    std::size_t       protocol_minimum_version;
    Date              protocol_date;
    CLIOptions const &cli_options;

private:
    std::size_t extract_version(std::vector<Instr> const    &instructions,
                                abc::ConstantPoolInfo const &constant_pool_info,
                                fs::path const &dofus_invoker_file_path);

    std::size_t
    extract_build_date(std::vector<Instr> const    &instructions,
                       std::size_t                  start_idx,
                       abc::ConstantPoolInfo const &constant_pool_info);
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_METADATAFINDER_H
