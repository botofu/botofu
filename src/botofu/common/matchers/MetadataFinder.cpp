#include "MetadataFinder.h"

#include <charconv>
#include <filesystem>
#include <fstream>
#include <regex>

#include <boost/algorithm/hex.hpp>
#include <boost/uuid/detail/md5.hpp>
#include <spdlog/spdlog.h>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/dofus_interface/DofusType.h"
#include "botofu/common/matchers/core/BuildDateCoreMatcher.h"
#include "botofu/common/matchers/core/VersionCoreMatcher.h"
#include "botofu/protocol/parser/version.h"

namespace fs = std::filesystem;
using md5    = boost::uuids::detail::md5;

// From https://stackoverflow.com/a/55070762/4810787
static inline std::string get_md5_hex_string(fs::path const &filepath) {
    // Compute the hash
    constexpr unsigned CHUNK_SIZE{1u << 13u};
    std::string        s(CHUNK_SIZE, '\0');
    std::ifstream      file_stream(filepath, std::ios::binary);
    md5                hash;
    md5::digest_type   digest;
    while (file_stream.read(s.data(), CHUNK_SIZE)) {
        hash.process_bytes(s.data(), s.size());
    }
    hash.get_digest(digest);
    // To string
    const auto  charDigest = reinterpret_cast<const char *>(&digest);
    std::string result;
    boost::algorithm::hex(charDigest,
                          charDigest + sizeof(md5::digest_type),
                          std::back_inserter(result));
    return result;
}

static std::unordered_map<std::string, unsigned int> MONTHS_NUMBERS = {
      {"Jan", 1},
      {"Feb", 2},
      {"Mar", 3},
      {"Apr", 4},
      {"May", 5},
      {"Jun", 6},
      {"Jul", 7},
      {"Aug", 8},
      {"Sep", 9},
      {"Oct", 10},
      {"Nov", 11},
      {"Dec", 12},
};

/**
 * Extract the date from the protocol and return it as an instance of Date.
 */
static inline Date extract_protocol_date(std::string const &protocol_date) {
    std::regex protocol_date_regex(
          R"([[:alpha:]]{3}, ([[:digit:]]+) ([[:alpha:]]{3}) )"
          R"(([[:digit:]]+) ([[:digit:]]+):([[:digit:]]+):[[:digit:]]+)",
          std::regex_constants::extended);
    std::smatch protocol_date_match;
    Date        date{};
    if (!std::regex_search(
              protocol_date, protocol_date_match, protocol_date_regex)) {
        SPDLOG_WARN("Protocol build date could not be retrieved from the "
                    "string '{}'. "
                    "A date filled with zeros will be used.",
                    protocol_date);
        return date;
    }
    std::from_chars(protocol_date_match[3].first.base(),
                    protocol_date_match[3].second.base(),
                    date.year);
    date.month = MONTHS_NUMBERS[protocol_date_match[2]];
    std::from_chars(protocol_date_match[1].first.base(),
                    protocol_date_match[1].second.base(),
                    date.day);
    std::from_chars(protocol_date_match[4].first.base(),
                    protocol_date_match[4].second.base(),
                    date.hour);
    std::from_chars(protocol_date_match[5].first.base(),
                    protocol_date_match[5].second.base(),
                    date.minute);
    return date;
}

/**
 * Try to parse the VERSION file that should be nearby the DofusInvoker.swf to
 * extract the patch version number.
 */
static inline Version
find_build_version(fs::path const &dofus_invoker_file_path) {
    fs::path const version_file_path(dofus_invoker_file_path.parent_path()
                                     / "VERSION");

    if (!fs::is_regular_file(version_file_path)) {
        SPDLOG_WARN(
              "VERSION file not found in '{}'. Dofus client build version "
              "will be set to 0.",
              fs::absolute(version_file_path).string());
        return Version{0, 0, 0, 0};
    }
    std::ifstream input(version_file_path.string());
    std::string version_file_content(std::istreambuf_iterator<char>(input), {});
    std::regex  version_regex(
          R"(([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+))",
          std::regex_constants::extended);
    std::smatch version_match;
    if (!std::regex_search(
              version_file_content, version_match, version_regex)) {
        SPDLOG_WARN("Could not match content of VERSION file '{}'. "
                    "Dofus client build version will be set to 0.",
                    version_file_content);
        return Version{0, 0, 0, 0};
    }
    return Version{std::stoul(version_match[1]),
                   std::stoul(version_match[2]),
                   std::stoul(version_match[3]),
                   std::stoul(version_match[4])};
}

MetadataFinder::MetadataFinder(TagDoAbc const   &tag_do_abc,
                               CLIOptions const &cli_options)
      : build_version{}, build_type{BuildType::UNKNOWN}, build_date{},
        protocol_version{0}, protocol_minimum_version{0}, protocol_date{},
        cli_options{cli_options} {
    abc::ConstantPoolInfo const &constant_pool_info{
          tag_do_abc.m_constant_pool_info};
    // 1. Find the method_info of the DofusClientMain constructor
    std::size_t method_info_idx{0};
    for (; method_info_idx < tag_do_abc.m_method_count; ++method_info_idx) {
        DofusMethod const method_name(
              tag_do_abc.m_methods_info[method_info_idx].get_name(
                    constant_pool_info));
        if (method_name.get_class_name() == "DofusClientMain"
            && method_name.get_method_name() == "DofusClientMain") {
            break;
        }
    }

    // 2. Find the method body
    std::size_t method_body_idx{0};
    for (; method_body_idx < tag_do_abc.m_method_bodies.size();
         ++method_body_idx) {
        if (tag_do_abc.m_method_bodies[method_body_idx].m_method
            == method_info_idx) {
            break;
        }
    }

    // 3. Disassemble the method body
    abc::MethodBody const &method_body{
          tag_do_abc.m_method_bodies[method_body_idx]};
    std::vector<Instr> method_instructions{method_body.disassemble()};

    // 4. Iterate over the instructions and find the information we want
    // 4.1. Find the version
    std::size_t const past_version_idx{
          this->extract_version(method_instructions,
                                constant_pool_info,
                                cli_options.dofus_invoker_path)};
    // 4.2. Find the build date
    this->extract_build_date(
          method_instructions, past_version_idx, constant_pool_info);

    // 5. Recover the protocol version
    // 5.1. Find the com.ankamagames.dofus.network.Metadata instance_info
    std::size_t metadata_class_idx{0};
    for (; metadata_class_idx < tag_do_abc.m_instances.size();
         ++metadata_class_idx) {
        if (tag_do_abc.m_instances[metadata_class_idx].is_in_network_namespace(
                  constant_pool_info)) {
            DofusType instance_info(
                  tag_do_abc.m_instances[metadata_class_idx].get_name(
                        constant_pool_info));
            if (instance_info.get_type() == "Metadata") {
                break;
            }
        }
    }
    // 5.2. Iterate over the class traits and get the protocol version / date
    abc::ClassInfo const &metadata_class_info{
          tag_do_abc.m_classes[metadata_class_idx]};
    for (auto const &trait : metadata_class_info.m_traits) {
        std::string const trait_name{trait.get_name(constant_pool_info)};
        if (trait_name == "PROTOCOL_BUILD") {
            this->protocol_version =
                  std::stoul(trait.get_data(constant_pool_info).value());
        }
        if (trait_name == "PROTOCOL_DATE") {
            std::string const protocol_date_str(
                  trait.get_data(constant_pool_info).value());
            this->protocol_date = extract_protocol_date(protocol_date_str);
        }
        if (trait_name == "PROTOCOL_REQUIRED_BUILD") {
            this->protocol_minimum_version =
                  std::stoul(trait.get_data(constant_pool_info).value());
        }
    }
}

std::size_t
MetadataFinder::extract_version(std::vector<Instr> const    &instructions,
                                abc::ConstantPoolInfo const &constant_pool_info,
                                const fs::path &dofus_invoker_file_path) {
    // First, try to extract the version from the VERSION file
    this->build_version = find_build_version(dofus_invoker_file_path);

    // Then extract the version from the provided instructions
    VersionCoreMatcher version_matcher(constant_pool_info);
    std::size_t const  match_index{
          version_matcher.find_pattern_and_update(instructions)};

    // If the build version extracted from the VERSION file is 0.0.0.0 (VERSION
    // file not found), use the build version from the DofusInvoker.swf
    if (this->build_version.major == 0 && this->build_version.minor == 0
        && this->build_version.patch == 0 && this->build_version.build == 0) {
        SPDLOG_WARN("Using version '{}.{}.{}.0' from DofusInvoker.swf. Build "
                    "ID is set to 0.",
                    version_matcher.major,
                    version_matcher.minor,
                    version_matcher.patch);
        this->build_version = Version{version_matcher.major,
                                      version_matcher.minor,
                                      version_matcher.patch,
                                      0};
    } else {
        // Check that the versions agree.
#define BOTOFU_CHECK_VERSION_COMPOSANT(VERSION_COMPOSANT)                      \
    if (this->build_version.VERSION_COMPOSANT                                  \
        != version_matcher.VERSION_COMPOSANT) {                                \
        SPDLOG_WARN("Found major version '{}' in VERSION file and major "      \
                    "version '{}' in "                                         \
                    "DofusInvoker.swf. Preferring major version from VERSION " \
                    "file: '{}'.",                                             \
                    this->build_version.VERSION_COMPOSANT,                     \
                    version_matcher.VERSION_COMPOSANT,                         \
                    this->build_version.VERSION_COMPOSANT);                    \
    }
        BOTOFU_CHECK_VERSION_COMPOSANT(major)
        BOTOFU_CHECK_VERSION_COMPOSANT(minor)
        BOTOFU_CHECK_VERSION_COMPOSANT(patch)
#undef BOTOFU_CHECK_VERSION_COMPOSANT
    }
    this->build_type = version_matcher.build_type;
    return match_index;
}

std::size_t MetadataFinder::extract_build_date(
      std::vector<Instr> const    &instructions,
      std::size_t                  start_idx,
      abc::ConstantPoolInfo const &constant_pool_info) {
    BuildDateCoreMatcher build_date_matcher(constant_pool_info);
    std::size_t const    match_index{
          build_date_matcher.find_pattern_and_update(instructions, start_idx)};
    this->build_date = build_date_matcher.date;
    return match_index;
}

json MetadataFinder::to_json() const {
    json j;
    j["client"]["hash"] =
          get_md5_hex_string(this->cli_options.dofus_invoker_path);
    j["client"]["version"]       = this->build_version.to_json();
    j["client"]["build"]["time"] = this->build_date.timestamp();
    j["client"]["build"]["type"] = build_type_to_string(this->build_type);

    j["protocol"]["last_updated"]       = this->protocol_date.timestamp();
    j["protocol"]["version"]["current"] = this->protocol_version;
    j["protocol"]["version"]["minimum"] = this->protocol_minimum_version;

    j["parser"]["version"] = json::array(
          {PROTOCOL_PARSER_MAJOR_VERSION, PROTOCOL_PARSER_MINOR_VERSION});

    j["parsing"]["time"] =
          std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::system_clock::now().time_since_epoch())
                .count();
    j["parsing"]["name"]    = this->cli_options.parsing_name;
    j["parsing"]["tags"]    = this->cli_options.tags;
    j["parsing"]["options"] = this->cli_options.to_json();

    j["protocol"]["time"]    = this->protocol_date.timestamp();
    j["protocol"]["version"] = {{"current", this->protocol_version},
                                {"minimum", this->protocol_minimum_version}};
    return j;
}
