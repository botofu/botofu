#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_PATTERNMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_PATTERNMATCHER_H

#include <memory>
#include <queue>
#include <vector>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/composed/BaseComposedMatcher.h"
#include "botofu/swf/parser/abc/MethodBody.h"
#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"

namespace details {

template <typename OutputType>
using ComparedType =
      std::pair<std::size_t /*index of first match*/,
                std::shared_ptr<BaseComposedMatcher<OutputType>> /*matcher*/>;

template <typename OutputType>
struct BaseComposedMatcherComparator {
    /**
     * @brief Comparator used to sort in the priority queue.
     *
     * From https://en.cppreference.com/w/cpp/container/priority_queue :
     *
     * Note that the Compare parameter is defined such that it returns true if
     * its first argument comes before its second argument in a weak ordering.
     * But because the priority queue outputs largest elements first, the
     * elements that "come before" are actually output last. That is, the front
     * of the queue contains the "last" element according to the weak ordering
     * imposed by Compare.
     *
     * We want the earliest match (smallest index in .first) to be on top of the
     * queue, i.e. to come first. In case of equality, the longest matcher
     * should come first.
     *
     * @param lhs first element to compare
     * @param rhs second element to compare
     * @return true if lhs should come after rhs in pops, otherwise false.
     */
    bool operator()(ComparedType<OutputType> const &lhs,
                    ComparedType<OutputType> const &rhs) const {
        /*
         * We want the earliest match (smallest index in .first) to be on top of
         * the queue, i.e. to pop first. In case of equality, the longest
         * sequence matched should pop first.
         *
         * We return true if lhs should come after rhs in pops, otherwise false.
         */
        // First sort by match index
        if (lhs.first != rhs.first) {
            return lhs.first > rhs.first;
        }
        // If 2 matchers matched at the same position, the longest pops first.
        return lhs.second->size() < rhs.second->size();
    }
};
}   // namespace details

template <typename OutputType>
class PatternMatcher {
    using ComparedType = details::ComparedType<OutputType>;
    using Comparator   = details::BaseComposedMatcherComparator<OutputType>;

public:
    PatternMatcher(abc::MethodBody const &method_body)
          : m_code(method_body.disassemble()), m_matchers(), m_position{0} {
    }

    /**
     * @brief Add the given matcher to the list of matchers considered
     * @param matcher the matcher to add
     */
    void add_matcher(
          const std::shared_ptr<BaseComposedMatcher<OutputType>> &matcher) {
        std::size_t const first_occurence{
              matcher->find_pattern_and_update_matchers(m_code, m_position)};
        m_matchers.emplace(first_occurence, matcher);
    }

    /**
     * @brief Add an instance of @p MatcherType constructed with @p args
     * @tparam MatcherType type of the matcher to add
     * @tparam Args types of the arguments forwarded to the matcher constructor
     * @param args arguments forwarded to the matcher constructor
     */
    template <typename MatcherType, typename... Args>
    void add_matcher(Args... args) {
        this->add_matcher(std::make_shared<MatcherType>(std::forward(args)...));
    }

    /**
     * @brief Match all the patterns in the instructions given at instance
     *    construction and update @p fields accordingly
     * @param current_class_information structure containing the information on
     *    the class we are currently analysing.
     */
    void match_all(OutputType &output) {
        // Get the best match and its position
        auto best_match = m_matchers.top();
        m_position      = best_match.first;
        // While there is at least one positive match
        while (m_position < m_code.size()) {
            auto const &matcher = best_match.second;
            // Call the matcher's update_fields function.
            matcher->update_output(output);
            // Update the position to the end of the matched instruction
            // sequence.
            m_position += matcher->size();
            // Refresh the matchers that point to instructions we already
            // covered.
            this->refresh_matchers();
            // Refresh the best match.
            best_match = m_matchers.top();
            // Refresh the position.
            m_position = best_match.first;
        }
    }

private:
    void refresh_matchers() {
        auto best_match = m_matchers.top();
        while (best_match.first < m_position) {
            // Pop the invalid matcher.
            m_matchers.pop();
            // And push it again to update it.
            this->add_matcher(best_match.second);
            // And loop over best_match
            best_match = m_matchers.top();
        }
    }

    std::vector<Instr> const m_code;
    std::priority_queue<ComparedType, std::vector<ComparedType>, Comparator>
                      m_matchers;
    std::size_t       m_position;
    DofusMethod const m_method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_PATTERNMATCHER_H
