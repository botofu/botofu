#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_BUILDDATECOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_BUILDDATECOREMATCHER_H

#include "botofu/common/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/data_structures/Date.h"

struct BuildDateCoreMatcher final : public BaseCoreMatcher {
    explicit BuildDateCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    Date date;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_BUILDDATECOREMATCHER_H
