#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_SUPERSERIALIZEASCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_SUPERSERIALIZEASCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct SuperSerializeAsCoreMatcher final : public BaseCoreMatcher {
    explicit SuperSerializeAsCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusMethod method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_SUPERSERIALIZEASCOREMATCHER_H
