#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_ARRAYATTRIBUTEINDEXINGWRITECOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_ARRAYATTRIBUTEINDEXINGWRITECOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct ArrayAttributeIndexingWriteCoreMatcher final : public BaseCoreMatcher {
    explicit ArrayAttributeIndexingWriteCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusAttribute attribute;
    DofusMethod    write_method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_ARRAYATTRIBUTEINDEXINGWRITECOREMATCHER_H
