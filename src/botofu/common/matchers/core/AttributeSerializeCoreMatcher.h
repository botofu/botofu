#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_ATTRIBUTESERIALIZECOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_ATTRIBUTESERIALIZECOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct AttributeSerializeCoreMatcher final : public BaseCoreMatcher {
    explicit AttributeSerializeCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusAttribute attribute;
    DofusMethod    method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_ATTRIBUTESERIALIZECOREMATCHER_H
