#include "WriteAttributeMethodCoreMatcher.h"

#include "botofu/common/dofus_interface/DofusMethod.h"

WriteAttributeMethodCoreMatcher::WriteAttributeMethodCoreMatcher(
      abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher({"getlocal(_[[:digit:]])?",
                         "getlocal_0",
                         "getproperty",
                         "callpropvoid"},
                        constant_pool_info),
        attribute{}, method{} {
}

void WriteAttributeMethodCoreMatcher::update(
      std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    // First recover the name of the serialized attribute
    this->attribute =
          this->get_attribute_from_getproperty(instructions[match_start + 2]);

    // Then recover the method used to serialize
    Instr const &io_call_instruction{instructions[match_start + 3]};
    this->method = DofusMethod(
          p_constant_pool_info.get_multiname(io_call_instruction.operands[0])
                .to_string(p_constant_pool_info));
}
