#include "SerializeCallWithTypeManagerCoreMatcher.h"

SerializeCallWithTypeManagerCoreMatcher::
      SerializeCallWithTypeManagerCoreMatcher(
            abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher({"getlocal_0",
                         "getproperty",
                         "getlocal(_[[:digit:]])?",
                         "getproperty",
                         "getlex",
                         "astypelate",
                         "getlocal(_[[:digit:]])?",
                         "callpropvoid"},
                        constant_pool_info),
        list_attribute{}, type{} {
}

void SerializeCallWithTypeManagerCoreMatcher::update(
      std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    this->list_attribute =
          this->get_attribute_from_getproperty(instructions[match_start + 1]);
    Instr const &getlex_instruction{instructions[match_start + 4]};
    this->type = DofusType(
          p_constant_pool_info.get_multiname(getlex_instruction.operands[0])
                .to_string(p_constant_pool_info));
    this->method =
          this->get_method_from_callprop_void(instructions[match_start + 7]);
}
