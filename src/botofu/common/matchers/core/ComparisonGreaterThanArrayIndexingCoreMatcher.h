#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_COMPARISONGREATERTHANARRAYINDEXINGCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_COMPARISONGREATERTHANARRAYINDEXINGCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"
#include "botofu/common/matchers/instructions/push.h"
#include "botofu/protocol/parser/data_structures/Limits.h"

struct ComparisonGreaterThanArrayIndexingCoreMatcher final
      : public BaseCoreMatcher {
    explicit ComparisonGreaterThanArrayIndexingCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusAttribute      attribute;
    Limits              limits;
    PushInstructionType push_type;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_COMPARISONGREATERTHANARRAYINDEXINGCOREMATCHER_H
