#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_BOOLEANBYTEWRAPPERSETFLAGCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_BOOLEANBYTEWRAPPERSETFLAGCOREMATCHER_H

#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct BooleanByteWrapperSetFlagCoreMatcher final : public BaseCoreMatcher {
    explicit BooleanByteWrapperSetFlagCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    std::size_t    bit_index;
    DofusAttribute attribute;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_BOOLEANBYTEWRAPPERSETFLAGCOREMATCHER_H
