#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_SINGLECALLPROPVOIDCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_SINGLECALLPROPVOIDCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct SingleCallPropVoidCoreMatcher final : public BaseCoreMatcher {
    explicit SingleCallPropVoidCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    bool        is_valid;
    DofusMethod called_method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_SINGLECALLPROPVOIDCOREMATCHER_H
