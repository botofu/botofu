#include "DebugLineInstructionCoreMatcher.h"

DebugLineInstructionCoreMatcher::DebugLineInstructionCoreMatcher(
      abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher({"debugline"}, constant_pool_info) {
}

void DebugLineInstructionCoreMatcher::update(
      std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
}
