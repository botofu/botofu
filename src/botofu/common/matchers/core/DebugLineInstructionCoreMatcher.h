#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_DEBUGLINEINSTRUCTIONCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_DEBUGLINEINSTRUCTIONCOREMATCHER_H

#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct DebugLineInstructionCoreMatcher final : public BaseCoreMatcher {
    explicit DebugLineInstructionCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_DEBUGLINEINSTRUCTIONCOREMATCHER_H
