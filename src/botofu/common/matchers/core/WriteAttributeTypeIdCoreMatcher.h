#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_WRITEATTRIBUTETYPEIDCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_WRITEATTRIBUTETYPEIDCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct WriteAttributeTypeIdCoreMatcher final : public BaseCoreMatcher {
    explicit WriteAttributeTypeIdCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusAttribute attribute;
    DofusMethod    method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_WRITEATTRIBUTETYPEIDCOREMATCHER_H
