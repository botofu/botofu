#include "BooleanByteWrapperWriteByteCoreMatcher.h"

BooleanByteWrapperWriteByteCoreMatcher::BooleanByteWrapperWriteByteCoreMatcher(
      abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher(
              {"getlocal_1", "getlocal(_[[:digit:]])?", "callpropvoid"},
              constant_pool_info) {
}

void BooleanByteWrapperWriteByteCoreMatcher::update(
      std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    // Do nothing more as we already have all the information needed.
}
