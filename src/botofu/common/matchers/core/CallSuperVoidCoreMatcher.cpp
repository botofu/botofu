#include "CallSuperVoidCoreMatcher.h"

#include <boost/algorithm/string/predicate.hpp>

using boost::algorithm::starts_with;

CallSuperVoidCoreMatcher::CallSuperVoidCoreMatcher(
      abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher({"callsupervoid"}, constant_pool_info),
        called_method() {
}

void CallSuperVoidCoreMatcher::update(std::vector<Instr> const &instructions,
                                      std::size_t               match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    Instr const      &supervoid_instruction{instructions[match_start]};
    DofusMethod const supervoid_method(
          p_constant_pool_info.get_multiname(supervoid_instruction.operands[0])
                .to_string(p_constant_pool_info));
    std::string const propvoid_method_name(supervoid_method.get_method_name());
    if (starts_with(propvoid_method_name, "serializeAs_")) {
        this->called_method = supervoid_method;
    }
}
