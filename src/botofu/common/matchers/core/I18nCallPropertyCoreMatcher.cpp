#include "I18nCallPropertyCoreMatcher.h"

#include <string>

#include <boost/algorithm/string/predicate.hpp>

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/dofus_interface/DofusNamespace.h"
#include "botofu/common/matchers/instructions/InstructionMatcher.h"
#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/Multiname.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/Namespace.h"

namespace {
bool is_getlex_I18n_singleton(uint32                       operand,
                              abc::ConstantPoolInfo const &constant_pool_info) {
    return constant_pool_info.get_multiname(operand).get_name(
                 constant_pool_info)
           == "I18n";
}
}   // namespace

I18nCallPropertyCoreMatcher::I18nCallPropertyCoreMatcher(
      abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher(
              {InstructionMatcher("getlocal_0"),
               InstructionMatcher("getlex", {is_getlex_I18n_singleton}),
               InstructionMatcher("getlocal_0"),
               InstructionMatcher("getproperty"),
               InstructionMatcher("callproperty"),
               InstructionMatcher("initproperty")},
              constant_pool_info),
        serialisation_method(), set_attribute_name(), attribute_used_as_id() {
}

void I18nCallPropertyCoreMatcher::update(std::vector<Instr> const &instructions,
                                         std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    Instr const &i18n_instance_instruction{instructions[match_start + 1]};
    Instr const &getprop_instruction{instructions[match_start + 3]};
    Instr const &callprop_instruction{instructions[match_start + 4]};
    Instr const &initprop_instruction{instructions[match_start + 5]};

    auto const &i18n_instance_multiname{
          this->p_constant_pool_info.get_multiname(
                i18n_instance_instruction.operands[0])};
    DofusNamespace const i18n_class_namespace{
          i18n_instance_multiname.get_namespace(this->p_constant_pool_info)
                .value()
                .to_string(this->p_constant_pool_info)};
    std::string const i18n_class_name{
          i18n_instance_multiname.get_name(this->p_constant_pool_info).value()};
    std::string const i18n_method_name{
          this->p_constant_pool_info
                .get_multiname(callprop_instruction.operands[0])
                .get_name(this->p_constant_pool_info)
                .value()};
    this->serialisation_method =
          DofusMethod(i18n_method_name, i18n_class_name, i18n_class_namespace);
    this->set_attribute_name =
          DofusAttribute(this->p_constant_pool_info
                               .get_multiname(initprop_instruction.operands[0])
                               .to_string(this->p_constant_pool_info));
    this->attribute_used_as_id =
          DofusAttribute(this->p_constant_pool_info
                               .get_multiname(getprop_instruction.operands[0])
                               .to_string(this->p_constant_pool_info));
    // We know for SURE that both set_attribute_name and
    // set_attribute_name are attributes of the same class, which is "this"
    // (because this matcher only matches patterns with "getlocal_0").
    // Because set_attribute_name contains information on "this", we can use
    // that information and add it to attribute_used_as_id that is missing it.
    this->attribute_used_as_id.set_class_name(
          this->set_attribute_name.get_class_name());
    this->attribute_used_as_id.set_namespace(
          this->set_attribute_name.get_namespace());
}
