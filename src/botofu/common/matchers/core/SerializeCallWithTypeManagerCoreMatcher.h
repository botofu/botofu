#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_SERIALIZECALLWITHTYPEMANAGERCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_SERIALIZECALLWITHTYPEMANAGERCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/dofus_interface/DofusType.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct SerializeCallWithTypeManagerCoreMatcher final : public BaseCoreMatcher {
    explicit SerializeCallWithTypeManagerCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusAttribute list_attribute;
    DofusType      type;
    DofusMethod    method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_SERIALIZECALLWITHTYPEMANAGERCOREMATCHER_H
