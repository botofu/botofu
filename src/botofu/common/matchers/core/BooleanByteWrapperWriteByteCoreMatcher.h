#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_BOOLEANBYTEWRAPPERWRITEBYTECOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_BOOLEANBYTEWRAPPERWRITEBYTECOREMATCHER_H

#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct BooleanByteWrapperWriteByteCoreMatcher final : public BaseCoreMatcher {
    explicit BooleanByteWrapperWriteByteCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_BOOLEANBYTEWRAPPERWRITEBYTECOREMATCHER_H
