#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_WRITEATTRIBUTEMETHODCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_WRITEATTRIBUTEMETHODCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct WriteAttributeMethodCoreMatcher final : public BaseCoreMatcher {
    explicit WriteAttributeMethodCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusAttribute attribute;
    DofusMethod    method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_WRITEATTRIBUTEMETHODCOREMATCHER_H
