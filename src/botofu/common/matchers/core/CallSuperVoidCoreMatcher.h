#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_CALLSUPERVOIDCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_CALLSUPERVOIDCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct CallSuperVoidCoreMatcher final : public BaseCoreMatcher {
    explicit CallSuperVoidCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusMethod called_method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_CALLSUPERVOIDCOREMATCHER_H
