#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_WRITETYPEWITHTYPEMANAGERCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_WRITETYPEWITHTYPEMANAGERCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/dofus_interface/DofusType.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct WriteTypeWithTypeManagerCoreMatcher final : public BaseCoreMatcher {
    explicit WriteTypeWithTypeManagerCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusAttribute list_attribute;
    DofusType      type;
    DofusMethod    write_method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_WRITETYPEWITHTYPEMANAGERCOREMATCHER_H
