#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_FORLOOPSTARTCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_FORLOOPSTARTCOREMATCHER_H

#include "botofu/common/matchers/core/BaseCoreMatcher.h"
#include "botofu/common/matchers/instructions/push.h"
#include "botofu/protocol/parser/data_structures/Limits.h"

struct ForLoopStartCoreMatcher final : public BaseCoreMatcher {
    explicit ForLoopStartCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    PushInstructionType push_type;
    Limits              limits;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_FORLOOPSTARTCOREMATCHER_H
