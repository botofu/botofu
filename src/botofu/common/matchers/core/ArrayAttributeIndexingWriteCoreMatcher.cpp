#include "ArrayAttributeIndexingWriteCoreMatcher.h"

ArrayAttributeIndexingWriteCoreMatcher::ArrayAttributeIndexingWriteCoreMatcher(
      abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher({"getlocal_1",
                         "getlocal_0",
                         "getproperty",
                         "getlocal(_[[:digit:]])?",
                         "getproperty",
                         "callpropvoid"},
                        constant_pool_info),
        attribute{}, write_method{} {
}

void ArrayAttributeIndexingWriteCoreMatcher::update(
      std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    this->attribute =
          this->get_attribute_from_getproperty(instructions[match_start + 2]);
    this->write_method =
          this->get_method_from_callprop_void(instructions[match_start + 5]);
}
