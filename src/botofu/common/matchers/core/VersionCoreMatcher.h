#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_VERSIONCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_VERSIONCOREMATCHER_H

#include "botofu/common/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/data_structures/BuildType.h"

struct VersionCoreMatcher final : public BaseCoreMatcher {
    explicit VersionCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    std::size_t major;
    std::size_t minor;
    std::size_t patch;
    BuildType   build_type;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_VERSIONCOREMATCHER_H
