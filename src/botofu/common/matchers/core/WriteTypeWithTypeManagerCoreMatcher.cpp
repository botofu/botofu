#include "WriteTypeWithTypeManagerCoreMatcher.h"

WriteTypeWithTypeManagerCoreMatcher::WriteTypeWithTypeManagerCoreMatcher(
      abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher({"getlocal(_[[:digit:]])?",
                         "getlocal_0",
                         "getproperty",
                         "getlocal(_[[:digit:]])?",
                         "getproperty",
                         "getlex",
                         "astypelate",
                         "callproperty",
                         "callpropvoid"},
                        constant_pool_info),
        list_attribute{}, type{}, write_method{} {
}

void WriteTypeWithTypeManagerCoreMatcher::update(
      std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    this->list_attribute =
          this->get_attribute_from_getproperty(instructions[match_start + 2]);
    Instr const &getlex_instruction{instructions[match_start + 5]};
    this->type = DofusType(
          p_constant_pool_info.get_multiname(getlex_instruction.operands[0])
                .to_string(p_constant_pool_info));
    Instr const &callpropvoid_instruction{instructions[match_start + 8]};
    this->write_method =
          DofusMethod(p_constant_pool_info
                            .get_multiname(callpropvoid_instruction.operands[0])
                            .to_string(p_constant_pool_info));
}
