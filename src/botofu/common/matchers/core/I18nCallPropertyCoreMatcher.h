#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_I18NCALLPROPERTYCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_I18NCALLPROPERTYCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct I18nCallPropertyCoreMatcher final : public BaseCoreMatcher {
    explicit I18nCallPropertyCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusMethod    serialisation_method;
    DofusAttribute set_attribute_name;
    DofusAttribute attribute_used_as_id;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_I18NCALLPROPERTYCOREMATCHER_H
