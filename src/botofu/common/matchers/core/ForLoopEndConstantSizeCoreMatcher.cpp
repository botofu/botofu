#include "ForLoopEndConstantSizeCoreMatcher.h"

#include "botofu/common/matchers/instructions/push.h"

ForLoopEndConstantSizeCoreMatcher::ForLoopEndConstantSizeCoreMatcher(
      abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher({"getlocal(_[[:digit:]])?",
                         "increment",
                         "convert_u",
                         "setlocal(_[[:digit:]])?",
                         "getlocal(_[[:digit:]])?",
                         PUSH_INSTRUCTION_REGEX,
                         "iflt"},
                        constant_pool_info),
        loop_size{0} {
}

void ForLoopEndConstantSizeCoreMatcher::update(
      std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    Instr const              &push_instruction{instructions[match_start + 5]};
    PushInstructionType const pushtype{
          get_push_type_from_name(push_instruction.model.name)};
    this->loop_size = std::stoul(get_push_representation_from_type(
          pushtype, p_constant_pool_info, push_instruction));
}
