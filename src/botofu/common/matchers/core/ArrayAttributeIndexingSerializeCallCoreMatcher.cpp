#include "ArrayAttributeIndexingSerializeCallCoreMatcher.h"

ArrayAttributeIndexingSerializeCallCoreMatcher::
      ArrayAttributeIndexingSerializeCallCoreMatcher(
            abc::ConstantPoolInfo const &constant_pool_info)
      : BaseCoreMatcher({"getlocal_0",
                         "getproperty",
                         "getlocal(_[[:digit:]])?",
                         "getproperty",
                         "getlocal(_[[:digit:]])?",
                         "callpropvoid"},
                        constant_pool_info),
        attribute{}, method{} {
}

void ArrayAttributeIndexingSerializeCallCoreMatcher::update(
      std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    this->attribute =
          this->get_attribute_from_getproperty(instructions[match_start + 1]);
    this->method =
          this->get_method_from_callprop_void(instructions[match_start + 5]);
}
