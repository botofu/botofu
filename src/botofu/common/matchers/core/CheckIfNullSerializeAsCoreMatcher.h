#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_CHECKIFNULLSERIALIZEASCOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_CHECKIFNULLSERIALIZEASCOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct CheckIfNullSerializeAsCoreMatcher final : public BaseCoreMatcher {
    explicit CheckIfNullSerializeAsCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusAttribute attribute;
    DofusMethod    method;
    DofusMethod    is_null_write_method;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_CHECKIFNULLSERIALIZEASCOREMATCHER_H
