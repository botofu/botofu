#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_FORLOOPENDATTRIBUTEATTRIBUTECOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_FORLOOPENDATTRIBUTEATTRIBUTECOREMATCHER_H

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct ForLoopEndAttributeAttributeCoreMatcher final : public BaseCoreMatcher {
    explicit ForLoopEndAttributeAttributeCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    DofusAttribute attribute;
    DofusAttribute subattribute;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_FORLOOPENDATTRIBUTEATTRIBUTECOREMATCHER_H
