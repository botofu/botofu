#ifndef BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_FORLOOPENDCONSTANTSIZECOREMATCHER_H
#define BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_FORLOOPENDCONSTANTSIZECOREMATCHER_H

#include "botofu/common/matchers/core/BaseCoreMatcher.h"

struct ForLoopEndConstantSizeCoreMatcher final : public BaseCoreMatcher {
    explicit ForLoopEndConstantSizeCoreMatcher(
          abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions,
                std::size_t               match_start) final;

    std::size_t loop_size;
};

#endif   // BOTOFU_SRC_BOTOFU_COMMON_MATCHERS_CORE_FORLOOPENDCONSTANTSIZECOREMATCHER_H
