#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSATTRIBUTE_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSATTRIBUTE_H

#include <string>

#include <boost/functional/hash.hpp>
#include <nlohmann/json.hpp>

#include "DofusNamespace.h"

using json = nlohmann::json;

class DofusAttribute {
public:
    DofusAttribute() = default;

    explicit DofusAttribute(std::string const &name);

    [[nodiscard]] bool is_private() const;

    [[nodiscard]] DofusNamespace const &get_namespace() const;

    [[nodiscard]] std::string const &get_class_name() const;

    [[nodiscard]] std::string const &get_attribute_name() const;

    [[nodiscard]] std::string to_string() const;

    bool operator==(DofusAttribute const &other) const;

    void set_attribute_name(std::string const &name);
    void set_class_name(std::string const &name);
    void set_namespace(DofusNamespace const &name);

    [[nodiscard]] json to_json() const;

private:
    DofusNamespace m_namespace;
    std::string    m_class_name;
    std::string    m_attribute_name;
};

namespace std {

template <>
struct hash<DofusAttribute> {
    std::size_t operator()(const DofusAttribute &dofus_attribute) const;
};

}   // namespace std

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSATTRIBUTE_H
