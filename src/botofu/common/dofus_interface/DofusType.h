#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSTYPE_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSTYPE_H

#include <string>

#include <nlohmann/json.hpp>

#include "DofusNamespace.h"

using json = nlohmann::json;

class DofusType {
public:
    DofusType() = default;

    explicit DofusType(std::string const &type);

    [[nodiscard]] DofusNamespace const &get_namespace() const;

    [[nodiscard]] std::string const &get_type() const;

    [[nodiscard]] std::string to_string() const;

    [[nodiscard]] bool is_vector() const;

    [[nodiscard]] bool is_bytearray() const;

    [[nodiscard]] json to_json() const;

private:
    std::string    m_type;
    DofusNamespace m_namespace;
};

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSTYPE_H
