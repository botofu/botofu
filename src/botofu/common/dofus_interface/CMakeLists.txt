target_sources(botofu_common_utils PRIVATE
        DofusNamespace.cpp
        DofusType.cpp
        DofusAttribute.cpp
        DofusMethod.cpp
        )
target_sources(botofu_common_utils INTERFACE
        DofusNamespace.h
        DofusType.h
        DofusAttribute.h
        DofusMethod.h
        )
