#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSNAMESPACE_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSNAMESPACE_H

#include <string>
#include <vector>

#include <boost/functional/hash.hpp>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

class DofusNamespace {
public:
    DofusNamespace() = default;

    explicit DofusNamespace(std::string const &dofus_namespace);

    bool operator==(DofusNamespace const &other) const;

    [[nodiscard]] std::vector<std::string> const &get_namespace_parts() const;

    [[nodiscard]] std::string to_string() const;

    [[nodiscard]] json to_json() const;

private:
    std::vector<std::string> m_namespace_parts;
};

namespace std {

template <>
struct hash<DofusNamespace> {
    std::size_t operator()(const DofusNamespace &dofus_namespace) const;
};

}   // namespace std

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSNAMESPACE_H
