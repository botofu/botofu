#include "DofusMethod.h"

#include "botofu/swf/parser/abc/constants.h"

DofusMethod::DofusMethod(std::string const &method) {
    std::size_t namespace_delimiter{
          method.find_first_of(abc::NAMESPACE_CLASS_SEPARATOR)};
    if (namespace_delimiter == std::string::npos) {
        namespace_delimiter = 0;
    }
    std::size_t class_delimiter{
          method.find_last_of(abc::CLASS_METHOD_SEPARATOR)};
    if (class_delimiter == std::string::npos) {
        // Try with ":"
        class_delimiter = method.find_last_of(abc::NAMESPACE_CLASS_SEPARATOR);
        if (class_delimiter == std::string::npos) {
            class_delimiter = 0;
        }
    }
    this->m_namespace  = DofusNamespace(method.substr(0, namespace_delimiter));
    this->m_class_name = method.substr(
          namespace_delimiter + (namespace_delimiter != 0),
          class_delimiter - namespace_delimiter - (namespace_delimiter != 0));
    this->m_method_name =
          method.substr(class_delimiter + (class_delimiter != 0));
}

DofusMethod::DofusMethod(std::string    method_name,
                         std::string    class_name,
                         DofusNamespace namespace_)
      : m_method_name(std::move(method_name)),
        m_class_name(std::move(class_name)),
        m_namespace(std::move(namespace_)) {
}

std::string DofusMethod::to_string() const {
    std::string result(this->get_namespace().to_string());
    if (!result.empty()) {
        result += abc::NAMESPACE_SEPARATOR;
    }
    result += this->get_class_name();
    if (!result.empty()) {
        result += abc::CLASS_METHOD_SEPARATOR;
    }
    result += this->get_method_name();
    return result;
}

json DofusMethod::to_json() const {
    json j;
    j["method_name"] = this->m_method_name;
    j["class_name"]  = this->m_class_name;
    j["namespace"]   = this->m_namespace.to_json();
    return j;
}

DofusNamespace DofusMethod::get_namespace() const {
    return m_namespace;
}

std::string DofusMethod::get_method_name() const {
    return m_method_name;
}

std::string DofusMethod::get_class_name() const {
    return m_class_name;
}

IOMethodType DofusMethod::get_method_type() const {
    return get_io_method_type_from_name(m_method_name);
}

bool DofusMethod::is_var_method() const {
    return is_var_io_method_from_name(m_method_name);
}
