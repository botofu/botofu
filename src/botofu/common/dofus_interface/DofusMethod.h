#ifndef BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSMETHOD_H
#define BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSMETHOD_H

#include <string>

#include <nlohmann/json.hpp>

#include "DofusNamespace.h"
#include "botofu/common/matchers/instructions/io.h"

using json = nlohmann::json;

class DofusMethod {
public:
    DofusMethod() = default;

    explicit DofusMethod(std::string const &method);
    DofusMethod(std::string    method_name,
                std::string    class_name,
                DofusNamespace namespace_);

    [[nodiscard]] DofusNamespace get_namespace() const;

    [[nodiscard]] std::string get_method_name() const;

    [[nodiscard]] std::string get_class_name() const;

    [[nodiscard]] std::string to_string() const;

    [[nodiscard]] IOMethodType get_method_type() const;

    [[nodiscard]] bool is_var_method() const;

    [[nodiscard]] json to_json() const;

private:
    std::string    m_method_name;
    std::string    m_class_name;
    DofusNamespace m_namespace;
};

#endif   // BOTOFU_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSMETHOD_H
