#include "DofusType.h"

#include <boost/algorithm/string/predicate.hpp>

#include "botofu/swf/parser/abc/constants.h"

using boost::algorithm::ends_with;
using boost::algorithm::starts_with;

DofusType::DofusType(std::string const &type) {
    std::size_t const pos{type.find_first_of(abc::NAMESPACE_CLASS_SEPARATOR)};
    if (pos == std::string::npos) {
        m_namespace = DofusNamespace();
        m_type      = type;
    } else {
        m_namespace = DofusNamespace(type.substr(0, pos));
        m_type      = type.substr(pos + 1);
    }
}

DofusNamespace const &DofusType::get_namespace() const {
    return m_namespace;
}

std::string const &DofusType::get_type() const {
    return m_type;
}

std::string DofusType::to_string() const {
    std::string result{m_namespace.to_string()};
    if (!result.empty()) {
        result += ".";
    }
    result += m_type;
    return result;
}

bool DofusType::is_vector() const {
    return starts_with(m_type, "Vector<") && ends_with(m_type, ">");
}
bool DofusType::is_bytearray() const {
    return m_type == "ByteArray";
}

[[nodiscard]] json DofusType::to_json() const {
    json j;
    j["name"]      = this->get_type();
    j["namespace"] = this->get_namespace().to_string();
    return j;
}
