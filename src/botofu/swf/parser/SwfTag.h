#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFTAG_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFTAG_H

#include <cstdint>   // Fixed-size integers
#include <memory>    // std::shared_ptr
#include <ostream>   // std::ostream
#include <string>    // std::string

#include "botofu/swf/parser/SwfConstants.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/parsing/ParseTagNotImplemented.h"
#include "botofu/swf/parser/tags/TagBase.h"

class SwfTag {
public:
    SwfTag()                    = delete;  /**< Deleted default constructor.*/
    SwfTag(SwfTag const &other) = default; /**< Default copy constructor.*/
    SwfTag(SwfTag &&other)      = default; /**< Default constructor.*/
    SwfTag &
            operator=(SwfTag const &other) = default; /**< Default copy operator.*/
    SwfTag &operator=(SwfTag &&other) = default; /**< Default move operator.*/
    ~SwfTag()                         = default; /**< Default destructor.*/

    /**
     * Constructor of the SwfTag class.
     *
     * The constructor will consume characters from the reader to
     * parse the tag. If the tag isn't implemented, the reader will
     * also be consumed by the adequate number of characters but a
     * TagNotImplemented will be put as information (instead of a
     * specific tag depending on the _tag_type).
     *
     * @param reader The reader used to output the data.
     */
    explicit SwfTag(SwfReader &reader);

    /**
     * GETTER for the type of the stored tag.
     *
     * @return The type of the stored tag.
     */
    [[nodiscard]] swf_constants::TagType get_type_id() const;

    /**
     * GETTER for the tag name.
     *
     * @return The tag name associated with the ID if it exists, else
     * "UnknownTag[TAGID]".
     */
    [[nodiscard]] std::string get_type_name() const;

    friend std::ostream &operator<<(std::ostream &os, SwfTag const &tag);

    [[nodiscard]] std::shared_ptr<TagBase> const &get_tag() const;

private:
    swf_constants::TagType   m_tag_type;
    uint32                   m_tag_size;
    std::shared_ptr<TagBase> m_tag_informations;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFTAG_H
