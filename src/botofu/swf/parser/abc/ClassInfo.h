#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CLASSINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CLASSINFO_H

#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/InstanceInfo/TraitInfo.h"

namespace abc {

struct ClassInfo {
    explicit ClassInfo(SwfReader &reader);

    [[nodiscard]] std::string to_string(TagDoAbc const &tag_do_abc,
                                        unsigned        indent_level) const;

    uint32                      m_cinit;
    uint32                      m_trait_count;
    std::vector<abc::TraitInfo> m_traits;
};

}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CLASSINFO_H
