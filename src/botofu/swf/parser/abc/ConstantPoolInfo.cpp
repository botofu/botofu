#include "botofu/swf/parser/abc/ConstantPoolInfo.h"

#include <spdlog/spdlog.h>

abc::ConstantPoolInfo::ConstantPoolInfo(SwfReader &reader) {
    m_int_count = reader.read_var_u30();
    SPDLOG_DEBUG("Number of 'int': {}", m_int_count);
    m_integers.reserve(m_int_count + 1);
    m_integers.push_back(0);
    for (uint32 i{1}; i < m_int_count; ++i) {
        m_integers.push_back(reader.read_var_int32());
    }

    m_uint_count = reader.read_var_u30();
    SPDLOG_DEBUG("Number of 'uint': {}", m_uint_count);
    m_uintegers.reserve(m_uint_count + 1);
    m_uintegers.push_back(0);
    for (uint32 i{1}; i < m_uint_count; ++i) {
        m_uintegers.push_back(reader.read_var_uint32());
    }

    m_double_count = reader.read_var_u30();
    SPDLOG_DEBUG("Number of 'double': {}", m_double_count);
    m_doubles.reserve(m_double_count + 1);
    m_doubles.push_back(std::numeric_limits<double>::quiet_NaN());
    for (uint32 i{1}; i < m_double_count; ++i) {
        m_doubles.push_back(reader.read_double());
    }

    m_string_count = reader.read_var_u30();
    SPDLOG_DEBUG("Number of 'string': {}", m_string_count);
    m_strings.reserve(m_string_count + 1);
    m_strings.emplace_back("*");
    for (uint32 i{1}; i < m_string_count; ++i) {
        m_strings.emplace_back(reader);
    }

    m_namespace_count = reader.read_var_u30();
    SPDLOG_DEBUG("Number of 'namespace': {}", m_namespace_count);
    m_namespaces.reserve(m_namespace_count + 1);
    m_namespaces.emplace_back();
    for (uint32 i{1}; i < m_namespace_count; ++i) {
        m_namespaces.emplace_back(reader);
    }

    m_namespace_set_count = reader.read_var_u30();
    SPDLOG_DEBUG("Number of 'namespace set': {}", m_namespace_set_count);
    m_namespace_sets.reserve(m_namespace_set_count + 1);
    m_namespace_sets.emplace_back();
    for (uint32 i{1}; i < m_namespace_set_count; ++i) {
        m_namespace_sets.emplace_back(reader);
    }

    m_multiname_count = reader.read_var_u30();
    SPDLOG_DEBUG("Number of 'multiname': {}", m_multiname_count);
    m_multinames.reserve(m_multiname_count + 1);
    m_multinames.emplace_back();
    for (uint32 i{1}; i < m_multiname_count; ++i) {
        m_multinames.emplace_back(reader);
    }
}

std::ostream &operator<<(std::ostream &               os,
                         abc::ConstantPoolInfo const &constant_pool) {
    return os << "ConstantPoolInfo("
              << "NumberOfSignedIntegers = " << constant_pool.m_int_count
              << ", "
              << "NumberOfUnsignedIntegers = " << constant_pool.m_uint_count
              << ", "
              << "NumberOfDoubles = " << constant_pool.m_double_count << ", "
              << "NumberOfStrings = " << constant_pool.m_string_count << ", "
              << "NumberOfNamespaces = " << constant_pool.m_namespace_count
              << ", "
              << "NumberOfNamespaceSets = "
              << constant_pool.m_namespace_set_count << ", "
              << "NumberOfMultinames = " << constant_pool.m_multiname_count
              << ")";
}
