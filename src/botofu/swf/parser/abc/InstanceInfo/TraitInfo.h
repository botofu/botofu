#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_INSTANCEINFO_TRAITINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_INSTANCEINFO_TRAITINFO_H

#include <memory>
#include <optional>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/Namespace.h"
#include "botofu/swf/parser/abc/enumerations.h"

struct TagDoAbc;

namespace abc {

namespace trait {

struct TraitBase {
    virtual ~TraitBase() = default;

    [[nodiscard]] virtual std::string
    to_string(TagDoAbc const &tag_do_abc, unsigned indent_level) const = 0;

    [[nodiscard]] virtual std::string
    name(TagDoAbc const &tag_do_abc) const = 0;

    [[nodiscard]] virtual std::string
    data(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] virtual bool has_data() const;

    [[nodiscard]] virtual std::string
    type(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] virtual uint32 index() const;
};

struct TraitSlot : TraitBase {
    explicit TraitSlot(SwfReader &reader);

    [[nodiscard]] std::string to_string(TagDoAbc const &tag_do_abc,
                                        unsigned indent_level) const final;

    [[nodiscard]] std::string name(TagDoAbc const &tag_do_abc) const final;

    [[nodiscard]] std::string
    data(ConstantPoolInfo const &constant_pool_info) const final;

    [[nodiscard]] bool has_data() const final;

    [[nodiscard]] std::string
    type(ConstantPoolInfo const &constant_pool_info) const final;

    [[nodiscard]] uint32 index() const final;

    uint32                           m_slot_id;
    uint32                           m_type_name;
    uint32                           m_vindex;
    std::optional<abc::ConstantKind> m_vkind;
};

struct TraitClass : TraitBase {
    explicit TraitClass(SwfReader &reader);

    [[nodiscard]] std::string to_string(TagDoAbc const &tag_do_abc,
                                        unsigned indent_level) const final;

    [[nodiscard]] std::string name(TagDoAbc const &tag_do_abc) const final;

    [[nodiscard]] uint32 index() const final;

    uint32 m_slot_id;
    uint32 m_classi;
};

struct TraitFunction : TraitBase {
    explicit TraitFunction(SwfReader &reader);

    [[nodiscard]] std::string to_string(TagDoAbc const &tag_do_abc,
                                        unsigned indent_level) const final;

    [[nodiscard]] std::string name(TagDoAbc const &tag_do_abc) const final;

    [[nodiscard]] uint32 index() const final;

    uint32 m_slot_id;
    uint32 m_function;
};

struct TraitMethod : TraitBase {
    explicit TraitMethod(SwfReader &reader);

    [[nodiscard]] std::string to_string(TagDoAbc const &tag_do_abc,
                                        unsigned indent_level) const final;

    [[nodiscard]] std::string name(TagDoAbc const &tag_do_abc) const final;

    [[nodiscard]] uint32 index() const final;

    uint32 m_disp_id;
    uint32 m_method;
};

struct TraitUnknown : TraitBase {
    [[nodiscard]] std::string to_string(TagDoAbc const &tag_do_abc,
                                        unsigned indent_level) const final;

    [[nodiscard]] std::string name(TagDoAbc const &tag_do_abc) const final;
};

}   // namespace trait

struct TraitInfo {
    /**
     * Constructor of the TraitInfo class.
     * @param reader The reader from which we will read the informations needed
     * to construct the class.
     */
    explicit TraitInfo(SwfReader &reader);

    /**
     * Helper method for getting a readable representation of the Trait kind.
     * @return The string (human-readable) representation of the kind of the
     * TraitInfo instance.
     */
    [[nodiscard]] std::string get_kind_representation() const;

    /**
     * GETTER for the TraitInfo kind.
     * @return The kind of the TraitInfo.
     */
    [[nodiscard]] TraitKind get_kind() const;

    /**
     * GETTER for the name of the trait.
     * @param constant_pool The structure that store all the constants of the
     * DOABC tag.
     * @return The name of the trait as a string (human-readable).
     */
    [[nodiscard]] std::string
    get_name(ConstantPoolInfo const &constant_pool) const;

    /**
     * GETTER for the flag attributes of the trait.
     * @return A single byte with the first flag attribute on the less important
     * bit.
     */
    [[nodiscard]] uint8 get_flag_attributes() const;

    [[nodiscard]] std::optional<std::string>
    get_data(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] std::string name(TagDoAbc const &tag_do_abc) const;

    [[nodiscard]] std::string to_string(TagDoAbc const &tag_do_abc,
                                        unsigned        indent_level) const;

    [[nodiscard]] std::string
    get_type(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] uint32 get_index() const;

    [[nodiscard]] bool is_final() const;

    [[nodiscard]] bool is_override() const;

    [[nodiscard]] bool has_metadata() const;

    [[nodiscard]] bool is_getter() const;

    [[nodiscard]] bool is_setter() const;

    [[nodiscard]] bool is_method() const;

    [[nodiscard]] bool is_function() const;

    [[nodiscard]] bool is_class() const;

    [[nodiscard]] bool is_const() const;

    [[nodiscard]] bool is_slot() const;

    [[nodiscard]] bool is_public() const;

    [[nodiscard]] std::optional<abc::Namespace>
    get_namespace(abc::ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] std::optional<std::string> get_name_without_namespace(
          abc::ConstantPoolInfo const &constant_pool_info) const;

    uint32                                 m_name;
    uint8                                  m_kind;
    std::unique_ptr<abc::trait::TraitBase> m_data;
    std::optional<uint32>                  m_metadata_count;
    std::optional<std::vector<uint32>>     m_metadatas;
};

}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_INSTANCEINFO_TRAITINFO_H
