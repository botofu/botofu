#include "botofu/swf/parser/abc/InstanceInfo/TraitInfo.h"

#include <memory>
#include <optional>

#include "botofu/swf/parser/abc/ConstantPoolInfo/Namespace.h"
#include "botofu/swf/parser/abc/constants.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

abc::trait::TraitSlot::TraitSlot(SwfReader &reader) {
    m_slot_id   = reader.read_var_u30();
    m_type_name = reader.read_var_u30();
    m_vindex    = reader.read_var_u30();
    if (m_vindex) {
        m_vkind = static_cast<abc::ConstantKind>(reader.read_uint8());
    }
}

std::string abc::trait::TraitSlot::to_string(TagDoAbc const &tag_do_abc,
                                             unsigned indent_level) const {
    ConstantPoolInfo const &constant_pool_info{tag_do_abc.m_constant_pool_info};
    std::string             result;
    result += std::string(indent_level, '\t') + "TraitSlot(ID='"
              + std::to_string(m_slot_id) + "', type='"
              + this->type(constant_pool_info) + "'";
    if (m_vindex != 0) {
        result += ", data='" + this->data(constant_pool_info) + "'";
    }
    result += ")";
    return result;
}

bool abc::trait::TraitSlot::has_data() const {
    return this->m_vindex != 0;
}

std::string abc::trait::TraitSlot::data(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    if (!this->has_data()) {
        return "";
    }

    ConstantKind const kind{*m_vkind};
    switch (kind) {
    case ConstantKind::INT_:
        return std::to_string(constant_pool_info.get_integer(m_vindex));
    case ConstantKind::UINT_:
        return std::to_string(constant_pool_info.get_uinteger(m_vindex));
    case ConstantKind::DOUBLE_:
        return std::to_string(constant_pool_info.get_double(m_vindex));
    case ConstantKind::UTF8:
        return constant_pool_info.get_string_info(m_vindex).to_string();
    case ConstantKind::NAMESPACE:
    case ConstantKind::PACKAGENAMESPACE:
    case ConstantKind::PACKAGEINTERNALNS:
    case ConstantKind::PROTECTEDNAMESPACE:
    case ConstantKind::EXPLICITNAMESPACE:
    case ConstantKind::STATICPROTECTEDNS:
    case ConstantKind::PRIVATENS:
        return constant_pool_info.get_namespace(m_vindex).to_string(
              constant_pool_info);
    case ConstantKind::TRUE_:
        return "true";
    case ConstantKind::FALSE_:
        return "false";
    case ConstantKind::NULL_:
        return "null";
    case ConstantKind::UNDEFINED:
    default:
        return "UNDEFINED";
    }
}

std::string abc::trait::TraitSlot::type(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return constant_pool_info.get_multiname(m_type_name)
          .to_string(constant_pool_info);
}

std::string
abc::trait::TraitSlot::name([[maybe_unused]] TagDoAbc const &tag_do_abc) const {
    return std::to_string(m_slot_id);
}

uint32 abc::trait::TraitSlot::index() const {
    return m_vindex;
}

abc::trait::TraitClass::TraitClass(SwfReader &reader) {
    m_slot_id = reader.read_var_u30();
    m_classi  = reader.read_var_u30();
}

std::string abc::trait::TraitClass::to_string(TagDoAbc const &tag_do_abc,
                                              unsigned indent_level) const {
    return std::string(indent_level, '\t') + "TraitClass(class='"
           + this->name(tag_do_abc) + "')";
}

std::string abc::trait::TraitClass::name(TagDoAbc const &tag_do_abc) const {
    return tag_do_abc.m_classes[m_classi].to_string(tag_do_abc, 0);
}

uint32 abc::trait::TraitClass::index() const {
    return m_classi;
}

abc::trait::TraitFunction::TraitFunction(SwfReader &reader) {
    m_slot_id  = reader.read_var_u30();
    m_function = reader.read_var_u30();
}

std::string abc::trait::TraitFunction::to_string(TagDoAbc const &tag_do_abc,
                                                 unsigned indent_level) const {
    return std::string(indent_level, '\t') + "TraitFunction(func='"
           + tag_do_abc.m_methods_info[m_function].get_signature(
                 tag_do_abc.m_constant_pool_info, 0)
           + "')";
}

std::string abc::trait::TraitFunction::name(TagDoAbc const &tag_do_abc) const {
    return tag_do_abc.m_methods_info[m_function].get_name(
          tag_do_abc.m_constant_pool_info);
}

uint32 abc::trait::TraitFunction::index() const {
    return m_function;
}

abc::trait::TraitMethod::TraitMethod(SwfReader &reader) {
    m_disp_id = reader.read_var_u30();
    m_method  = reader.read_var_u30();
}

std::string abc::trait::TraitMethod::to_string(TagDoAbc const &tag_do_abc,
                                               unsigned indent_level) const {
    return std::string(indent_level, '\t') + "TraitMethod(meth='"
           + tag_do_abc.m_methods_info[m_method].get_signature(
                 tag_do_abc.m_constant_pool_info, 0)
           + "')";
}

uint32 abc::trait::TraitMethod::index() const {
    return m_method;
}

std::string abc::trait::TraitMethod::name(TagDoAbc const &tag_do_abc) const {
    return tag_do_abc.m_methods_info[m_method].get_name(
          tag_do_abc.m_constant_pool_info);
}

abc::TraitInfo::TraitInfo(SwfReader &reader)
      : m_data(std::make_unique<trait::TraitUnknown>()) {
    constexpr uint8 attr_metadata = 0x04;

    m_name = reader.read_var_u30();
    m_kind = reader.read_uint8();

    TraitKind const type{static_cast<TraitKind>(m_kind & MASK_00001111)};
    if (type == TraitKind::SLOT || type == TraitKind::CONST_) {
        m_data = std::make_unique<trait::TraitSlot>(reader);
    } else if (type == TraitKind::CLASS) {
        m_data = std::make_unique<trait::TraitClass>(reader);
    } else if (type == TraitKind::FUNCTION) {
        m_data = std::make_unique<trait::TraitFunction>(reader);
    } else if (type == TraitKind::METHOD || type == TraitKind::GETTER
               || type == TraitKind::SETTER) {
        m_data = std::make_unique<trait::TraitMethod>(reader);
    }

    uint8 const attributes{static_cast<uint8>(m_kind >> 4u)};
    if (attributes & attr_metadata) {
        m_metadata_count = reader.read_var_u30();
        // We need to create the vector first.
        m_metadatas.emplace();
        for (uint32 i{0}; i < m_metadata_count; ++i) {
            m_metadatas->push_back(reader.read_var_u30());
        }
    }
}

std::string abc::TraitInfo::get_kind_representation() const {
    return abc::TRAIT_KIND_NAME.at(this->get_kind());
}

abc::TraitKind abc::TraitInfo::get_kind() const {
    constexpr uint8 kind_mask{MASK_00001111};
    return static_cast<abc::TraitKind>(m_kind & kind_mask);
}

std::string
abc::TraitInfo::get_name(const abc::ConstantPoolInfo &constant_pool) const {
    return constant_pool.get_multiname(m_name).to_string(constant_pool);
}

bool abc::TraitInfo::is_final() const {
    constexpr uint8 is_final{MASK_00000001};
    return this->get_flag_attributes() & is_final;
}

bool abc::TraitInfo::is_override() const {
    constexpr uint8 is_override{MASK_00000010};
    return this->get_flag_attributes() & is_override;
}

bool abc::TraitInfo::has_metadata() const {
    constexpr uint8 has_metadata{MASK_00000100};
    return this->get_flag_attributes() & has_metadata;
}

uint8 abc::TraitInfo::get_flag_attributes() const {
    return static_cast<uint8>(m_kind >> 4u);
}

std::string abc::TraitInfo::to_string(const TagDoAbc &tag_do_abc,
                                      unsigned        indent_level) const {
    std::string result = std::string(indent_level, '\t')
                         + this->get_name(tag_do_abc.m_constant_pool_info)
                         + "(\n";
    result += m_data->to_string(tag_do_abc, indent_level + 1) + "\n";
    if (m_metadatas && !m_metadatas->empty()) {
        result += std::string(indent_level + 1, '\t') + "'metadatas': {\n";
        for (uint32 const metadata : *m_metadatas) {
            result += tag_do_abc.m_metadatas[metadata].to_string(
                  tag_do_abc.m_constant_pool_info, indent_level + 2);
        }
        result += std::string(indent_level + 1, '\t') + "}\n";
    }
    result += std::string(indent_level, '\t') + ")";
    return result;
}

std::optional<std::string> abc::TraitInfo::get_data(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    if (!this->m_data->has_data()) {
        return std::nullopt;
    }
    return this->m_data->data(constant_pool_info);
}

std::string abc::TraitInfo::get_type(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return m_data->type(constant_pool_info);
}

bool abc::TraitInfo::is_getter() const {
    return this->get_kind() == TraitKind::GETTER;
}

bool abc::TraitInfo::is_setter() const {
    return this->get_kind() == TraitKind::SETTER;
}

bool abc::TraitInfo::is_method() const {
    return this->get_kind() == TraitKind::METHOD;
}

bool abc::TraitInfo::is_function() const {
    return this->get_kind() == TraitKind::FUNCTION;
}

uint32 abc::TraitInfo::get_index() const {
    return m_data->index();
}

bool abc::TraitInfo::is_class() const {
    return this->get_kind() == TraitKind::CLASS;
}

bool abc::TraitInfo::is_const() const {
    return this->get_kind() == TraitKind::CONST_;
}

bool abc::TraitInfo::is_slot() const {
    return this->get_kind() == TraitKind::SLOT;
}

std::optional<abc::Namespace> abc::TraitInfo::get_namespace(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return constant_pool_info.get_multiname(m_name).get_namespace(
          constant_pool_info);
}

std::optional<std::string> abc::TraitInfo::get_name_without_namespace(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return constant_pool_info.get_multiname(m_name).get_name(
          constant_pool_info);
}

std::string abc::TraitInfo::name(TagDoAbc const &tag_do_abc) const {
    return m_data->name(tag_do_abc);
}

std::string
abc::trait::TraitUnknown::to_string([[maybe_unused]] TagDoAbc const &tag_do_abc,
                                    unsigned indent_level) const {
    return std::string(indent_level, '\t') + std::string("UNKNOWN trait");
}

std::string abc::trait::TraitUnknown::name(
      [[maybe_unused]] TagDoAbc const &tag_do_abc) const {
    return std::string("UNKNOWN");
}

std::string abc::trait::TraitBase::data(
      [[maybe_unused]] const abc::ConstantPoolInfo &constant_pool_info) const {
    return std::string();
}

bool abc::trait::TraitBase::has_data() const {
    return false;
}

std::string abc::trait::TraitBase::type(
      [[maybe_unused]] const abc::ConstantPoolInfo &constant_pool_info) const {
    return std::string();
}

uint32 abc::trait::TraitBase::index() const {
    return 0;
}
