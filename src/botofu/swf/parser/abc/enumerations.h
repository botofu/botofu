#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_ENUMERATIONS_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_ENUMERATIONS_H

namespace abc {

enum class ConstantKind {
    INT_               = 0x03,   // integer
    UINT_              = 0x04,   // uinteger
    DOUBLE_            = 0x06,   // double
    UTF8               = 0x01,   // string
    TRUE_              = 0x0B,   // -
    FALSE_             = 0x0A,   // -
    NULL_              = 0x0C,   // -
    UNDEFINED          = 0x00,   // -
    NAMESPACE          = 0x08,   // namespace
    PACKAGENAMESPACE   = 0x16,   // namespace
    PACKAGEINTERNALNS  = 0x17,   // namespace
    PROTECTEDNAMESPACE = 0x18,   // namespace
    EXPLICITNAMESPACE  = 0x19,   // namespace
    STATICPROTECTEDNS  = 0x1A,   // namespace
    PRIVATENS          = 0x05,   // namespace
};

enum class MultinameKind {
    UNKNOWN         = 0x00,
    QNAME           = 0x07,
    QNAMEA          = 0x0D,
    RTQNAME         = 0x0F,
    RTQNAMEA        = 0x10,
    RTQNAMEL        = 0x11,
    RTQNAMELA       = 0x12,
    MULTINAME       = 0x09,
    MULTINAMEA      = 0x0E,
    MULTINAMEL      = 0x1B,
    MULTINAMELA     = 0x1C,
    UNKNOWNTYPENAME = 0x1D
};

enum class NamespaceKind {
    NAMESPACE                = 0x08,
    PACKAGENAMESPACE         = 0x16,
    PACKAGEINTERNALNAMESPACE = 0x17,
    PROTECTEDNAMESPACE       = 0x18,
    EXPLICITNAMESPACE        = 0x19,
    STATICPROTECTEDNAMESPACE = 0x1A,
    PRIVATENAMESPACE         = 0x05
};

enum class TraitKind {
    SLOT     = 0x00,
    METHOD   = 0x01,
    GETTER   = 0x02,
    SETTER   = 0x03,
    CLASS    = 0x04,
    FUNCTION = 0x05,
    CONST_   = 0x06
};

}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_ENUMERATIONS_H
