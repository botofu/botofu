#include "botofu/swf/parser/abc/Metadata.h"

abc::Metadata::Metadata(SwfReader &reader) {
    m_name       = reader.read_var_u30();
    m_item_count = reader.read_var_u30();
    for (uint32 i{0}; i < m_item_count; ++i) {
        m_items.emplace_back(reader);
    }
}

std::string abc::Metadata::to_string(abc::ConstantPoolInfo const &constant_pool,
                                     unsigned indent_level) const {
    std::string result = std::string(indent_level, '\t')
                         + constant_pool.get_string_info(m_name).to_string()
                         + "{\n";
    for (auto const &item : m_items) {
        result += std::string(indent_level + 1, '\t')
                  + item.to_string(constant_pool, indent_level + 2) + ",\n";
    }
    result += std::string(indent_level, '\t') + "}";
    return result;
}
