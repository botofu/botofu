#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODBODY_AVM2INSTRUCTIONS_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODBODY_AVM2INSTRUCTIONS_H

/*
 * Inspired from https://github.com/Kelvyne/as3/blob/master/bytecode/method.go
 * Credits to Lakhdar Slaim (Kelvyne on Github).
 */

#include <iostream>
#include <string>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"

using InstrOperand = uint8;

enum OperandType {
    INSTROPERANDU30,
    INSTROPERANDU8,
    INSTROPERANDS16,
    INSTROPERANDS24,
    INSTROPERANDCASECOUNT,
};

struct InstrModel {
    uint8                     opcode;
    std::string               name;
    std::vector<InstrOperand> operands;
};

struct Instr {
    InstrModel          model;
    std::vector<uint32> operands;

    [[nodiscard]] std::string to_string() const;
};

std::ostream &operator<<(std::ostream &os, Instr const &i);

uint32 disassemble_inst_operand(SwfReader &r, OperandType t);

Instr disassemble_instr(SwfReader &r, uint8 code);

std::vector<Instr> disassemble_code(SwfReader &reader, std::size_t code_length);

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODBODY_AVM2INSTRUCTIONS_H
