#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"

namespace {

inline constexpr uint8 operator"" _u8(unsigned long long arg) noexcept {
    return static_cast<uint8>(arg);
}

std::unordered_map<uint8, InstrModel> const instructions = {
      {0xa0_u8, InstrModel{0xa0_u8, "add", {}}},
      {0xc5_u8, InstrModel{0xc5_u8, "add_i", {}}},
      {0x53_u8, InstrModel{0x53_u8, "applytype", {INSTROPERANDU30}}},
      {0x86_u8, InstrModel{0x86_u8, "astype", {INSTROPERANDU30}}},
      {0x87_u8, InstrModel{0x87_u8, "astypelate", {}}},
      {0xa8_u8, InstrModel{0xa8_u8, "bitand", {}}},
      {0x97_u8, InstrModel{0x97_u8, "bitnot", {}}},
      {0xa9_u8, InstrModel{0xa9_u8, "bitor", {}}},
      {0xaa_u8, InstrModel{0xaa_u8, "bitxor", {}}},
      {0x01_u8, InstrModel{0x01_u8, "bkpt", {}}},
      {0xF2_u8, InstrModel{0xF2_u8, "bkptline", {INSTROPERANDU30}}},
      {0x41_u8, InstrModel{0x41_u8, "call", {INSTROPERANDU30}}},
      {0x43_u8,
       InstrModel{0x43_u8, "callmethod", {INSTROPERANDU30, INSTROPERANDU30}}},
      {0x46_u8,
       InstrModel{0x46_u8, "callproperty", {INSTROPERANDU30, INSTROPERANDU30}}},
      {0x4c_u8,
       InstrModel{0x4c_u8, "callproplex", {INSTROPERANDU30, INSTROPERANDU30}}},
      {0x4f_u8,
       InstrModel{0x4f_u8, "callpropvoid", {INSTROPERANDU30, INSTROPERANDU30}}},
      {0x44_u8,
       InstrModel{0x44_u8, "callstatic", {INSTROPERANDU30, INSTROPERANDU30}}},
      {0x45_u8,
       InstrModel{0x45_u8, "callsuper", {INSTROPERANDU30, INSTROPERANDU30}}},
      {0x4e_u8,
       InstrModel{
             0x4e_u8, "callsupervoid", {INSTROPERANDU30, INSTROPERANDU30}}},
      {0x78_u8, InstrModel{0x78_u8, "checkfilter", {}}},
      {0x80_u8, InstrModel{0x80_u8, "coerce", {INSTROPERANDU30}}},
      {0x82_u8, InstrModel{0x82_u8, "coerce_a", {}}},
      {0x81_u8, InstrModel{0x81_u8, "coerce_b", {}}},
      {0x84_u8, InstrModel{0x84_u8, "coerce_d", {}}},
      {0x83_u8, InstrModel{0x83_u8, "coerce_i", {}}},
      {0x89_u8, InstrModel{0x89_u8, "coerce_o", {}}},
      {0x85_u8, InstrModel{0x85_u8, "coerce_s", {}}},
      {0x88_u8, InstrModel{0x88_u8, "coerce_u", {}}},
      {0x42_u8, InstrModel{0x42_u8, "construct", {INSTROPERANDU30}}},
      {0x4a_u8,
       InstrModel{
             0x4a_u8, "constructprop", {INSTROPERANDU30, INSTROPERANDU30}}},
      {0x49_u8, InstrModel{0x49_u8, "constructsuper", {INSTROPERANDU30}}},
      {0x76_u8, InstrModel{0x76_u8, "convert_b", {}}},
      {0x75_u8, InstrModel{0x75_u8, "convert_d", {}}},
      {0x73_u8, InstrModel{0x73_u8, "convert_i", {}}},
      {0x77_u8, InstrModel{0x77_u8, "convert_o", {}}},
      {0x70_u8, InstrModel{0x70_u8, "convert_s", {}}},
      {0x74_u8, InstrModel{0x74_u8, "convert_u", {}}},
      {0xef_u8,
       InstrModel{0xef_u8,
                  "debug",
                  {INSTROPERANDU8,
                   INSTROPERANDU30,
                   INSTROPERANDU8,
                   INSTROPERANDU30}}},
      {0xf1_u8, InstrModel{0xf1_u8, "debugfile", {INSTROPERANDU30}}},
      {0xf0_u8, InstrModel{0xf0_u8, "debugline", {INSTROPERANDU30}}},
      {0x94_u8, InstrModel{0x94_u8, "declocal", {INSTROPERANDU30}}},
      {0xc3_u8, InstrModel{0xc3_u8, "declocal_i", {INSTROPERANDU30}}},
      {0x93_u8, InstrModel{0x93_u8, "decrement", {}}},
      {0xc1_u8, InstrModel{0xc1_u8, "decrement_i", {}}},
      {0x6a_u8, InstrModel{0x6a_u8, "deleteproperty", {INSTROPERANDU30}}},
      {0xa3_u8, InstrModel{0xa3_u8, "divide", {}}},
      {0x2a_u8, InstrModel{0x2a_u8, "dup", {}}},
      {0x06_u8, InstrModel{0x06_u8, "dxns", {INSTROPERANDU30}}},
      {0x07_u8, InstrModel{0x07_u8, "dxnslate", {}}},
      {0xab_u8, InstrModel{0xab_u8, "equals", {}}},
      {0x72_u8, InstrModel{0x72_u8, "esc_xattr", {}}},
      {0x71_u8, InstrModel{0x71_u8, "esc_xelem", {}}},
      {0x5F_u8, InstrModel{0x5F_u8, "finddef", {INSTROPERANDU30}}},
      {0x5e_u8, InstrModel{0x5e_u8, "findproperty", {INSTROPERANDU30}}},
      {0x5d_u8, InstrModel{0x5d_u8, "findpropstrict", {INSTROPERANDU30}}},
      {0x59_u8, InstrModel{0x59_u8, "getdescendants", {INSTROPERANDU30}}},
      {0x64_u8, InstrModel{0x64_u8, "getglobalscope", {}}},
      {0x6e_u8, InstrModel{0x6e_u8, "getglobalslot", {INSTROPERANDU30}}},
      {0x60_u8, InstrModel{0x60_u8, "getlex", {INSTROPERANDU30}}},
      {0x62_u8, InstrModel{0x62_u8, "getlocal", {INSTROPERANDU30}}},
      {0xd0_u8, InstrModel{0xd0_u8, "getlocal_0", {}}},
      {0xd1_u8, InstrModel{0xd1_u8, "getlocal_1", {}}},
      {0xd2_u8, InstrModel{0xd2_u8, "getlocal_2", {}}},
      {0xd3_u8, InstrModel{0xd3_u8, "getlocal_3", {}}},
      {0x67_u8, InstrModel{0x67_u8, "getouterscope", {INSTROPERANDU30}}},
      {0x66_u8, InstrModel{0x66_u8, "getproperty", {INSTROPERANDU30}}},
      {0x65_u8, InstrModel{0x65_u8, "getscopeobject", {INSTROPERANDU30}}},
      {0x6c_u8, InstrModel{0x6c_u8, "getslot", {INSTROPERANDU30}}},
      {0x04_u8, InstrModel{0x04_u8, "getsuper", {INSTROPERANDU30}}},
      {0xb0_u8, InstrModel{0xb0_u8, "greaterequals", {}}},
      {0xaf_u8, InstrModel{0xaf_u8, "greaterthan", {}}},
      {0x1f_u8, InstrModel{0x1f_u8, "hasnext", {}}},
      {0x32_u8,
       InstrModel{0x32_u8, "hasnext2", {INSTROPERANDU30, INSTROPERANDU30}}},
      {0x13_u8, InstrModel{0x13_u8, "ifeq", {INSTROPERANDS24}}},
      {0x12_u8, InstrModel{0x12_u8, "iffalse", {INSTROPERANDS24}}},
      {0x18_u8, InstrModel{0x18_u8, "ifge", {INSTROPERANDS24}}},
      {0x17_u8, InstrModel{0x17_u8, "ifgt", {INSTROPERANDS24}}},
      {0x16_u8, InstrModel{0x16_u8, "ifle", {INSTROPERANDS24}}},
      {0x15_u8, InstrModel{0x15_u8, "iflt", {INSTROPERANDS24}}},
      {0x14_u8, InstrModel{0x14_u8, "ifne", {INSTROPERANDS24}}},
      {0x0f_u8, InstrModel{0x0f_u8, "ifnge", {INSTROPERANDS24}}},
      {0x0e_u8, InstrModel{0x0e_u8, "ifngt", {INSTROPERANDS24}}},
      {0x0d_u8, InstrModel{0x0d_u8, "ifnle", {INSTROPERANDS24}}},
      {0x0c_u8, InstrModel{0x0c_u8, "ifnlt", {INSTROPERANDS24}}},
      {0x19_u8, InstrModel{0x19_u8, "ifstricteq", {INSTROPERANDS24}}},
      {0x1a_u8, InstrModel{0x1a_u8, "ifstrictne", {INSTROPERANDS24}}},
      {0x11_u8, InstrModel{0x11_u8, "iftrue", {INSTROPERANDS24}}},
      {0xb4_u8, InstrModel{0xb4_u8, "in", {}}},
      {0x92_u8, InstrModel{0x92_u8, "inclocal", {INSTROPERANDU30}}},
      {0xc2_u8, InstrModel{0xc2_u8, "inclocal_i", {INSTROPERANDU30}}},
      {0x91_u8, InstrModel{0x91_u8, "increment", {}}},
      {0xc0_u8, InstrModel{0xc0_u8, "increment_i", {}}},
      {0x68_u8, InstrModel{0x68_u8, "initproperty", {INSTROPERANDU30}}},
      {0xb1_u8, InstrModel{0xb1_u8, "instanceof", {}}},
      {0xb2_u8, InstrModel{0xb2_u8, "istype", {INSTROPERANDU30}}},
      {0xb3_u8, InstrModel{0xb3_u8, "istypelate", {}}},
      {0x10_u8, InstrModel{0x10_u8, "jump", {INSTROPERANDS24}}},
      {0x08_u8, InstrModel{0x08_u8, "kill", {INSTROPERANDU30}}},
      {0x09_u8, InstrModel{0x09_u8, "label", {}}},
      {0xae_u8, InstrModel{0xae_u8, "lessequals", {}}},
      {0xad_u8, InstrModel{0xad_u8, "lessthan", {}}},
      {0x38_u8, InstrModel{0x38_u8, "lf32", {}}},
      {0x39_u8, InstrModel{0x39_u8, "lf64", {}}},
      {0x36_u8, InstrModel{0x36_u8, "li16", {}}},
      {0x37_u8, InstrModel{0x37_u8, "li32", {}}},
      {0x35_u8, InstrModel{0x35_u8, "li8", {}}},
      {0x1b_u8,
       InstrModel{0x1b_u8,
                  "lookupswitch",
                  {INSTROPERANDS24, INSTROPERANDCASECOUNT}}},
      {0xa5_u8, InstrModel{0xa5_u8, "lshift", {}}},
      {0xa4_u8, InstrModel{0xa4_u8, "modulo", {}}},
      {0xa2_u8, InstrModel{0xa2_u8, "multiply", {}}},
      {0xc7_u8, InstrModel{0xc7_u8, "multiply_i", {}}},
      {0x90_u8, InstrModel{0x90_u8, "negate", {}}},
      {0xc4_u8, InstrModel{0xc4_u8, "negate_i", {}}},
      {0x57_u8, InstrModel{0x57_u8, "newactivation", {}}},
      {0x56_u8, InstrModel{0x56_u8, "newarray", {INSTROPERANDU30}}},
      {0x5a_u8, InstrModel{0x5a_u8, "newcatch", {INSTROPERANDU30}}},
      {0x58_u8, InstrModel{0x58_u8, "newclass", {INSTROPERANDU30}}},
      {0x40_u8, InstrModel{0x40_u8, "newfunction", {INSTROPERANDU30}}},
      {0x55_u8, InstrModel{0x55_u8, "newobject", {INSTROPERANDU30}}},
      {0x1e_u8, InstrModel{0x1e_u8, "nextname", {}}},
      {0x23_u8, InstrModel{0x23_u8, "nextvalue", {}}},
      {0x02_u8, InstrModel{0x02_u8, "nop", {}}},
      {0x96_u8, InstrModel{0x96_u8, "not", {}}},
      {0x29_u8, InstrModel{0x29_u8, "pop", {}}},
      {0x1d_u8, InstrModel{0x1d_u8, "popscope", {}}},
      {0x24_u8, InstrModel{0x24_u8, "pushbyte", {INSTROPERANDU8}}},
      {0x22_u8, InstrModel{0x22_u8, "pushconstant", {INSTROPERANDU30}}},
      {0x2f_u8, InstrModel{0x2f_u8, "pushdouble", {INSTROPERANDU30}}},
      {0x27_u8, InstrModel{0x27_u8, "pushfalse", {}}},
      {0x2d_u8, InstrModel{0x2d_u8, "pushint", {INSTROPERANDU30}}},
      {0x31_u8, InstrModel{0x31_u8, "pushnamespace", {INSTROPERANDU30}}},
      {0x28_u8, InstrModel{0x28_u8, "pushnan", {}}},
      {0x20_u8, InstrModel{0x20_u8, "pushnull", {}}},
      {0x30_u8, InstrModel{0x30_u8, "pushscope", {}}},
      {0x25_u8, InstrModel{0x25_u8, "pushshort", {INSTROPERANDU30}}},
      {0x2c_u8, InstrModel{0x2c_u8, "pushstring", {INSTROPERANDU30}}},
      {0x26_u8, InstrModel{0x26_u8, "pushtrue", {}}},
      {0x2e_u8, InstrModel{0x2e_u8, "pushuint", {INSTROPERANDU30}}},
      {0x21_u8, InstrModel{0x21_u8, "pushundefined", {}}},
      {0x1c_u8, InstrModel{0x1c_u8, "pushwith", {}}},
      {0x48_u8, InstrModel{0x48_u8, "returnvalue", {}}},
      {0x47_u8, InstrModel{0x47_u8, "returnvoid", {}}},
      {0xa6_u8, InstrModel{0xa6_u8, "rshift", {}}},
      {0x63_u8, InstrModel{0x63_u8, "setlocal", {INSTROPERANDU30}}},
      {0x6F_u8, InstrModel{0x6F_u8, "setglobalslot", {INSTROPERANDU30}}},
      {0xd4_u8, InstrModel{0xd4_u8, "setlocal_0", {}}},
      {0xd5_u8, InstrModel{0xd5_u8, "setlocal_1", {}}},
      {0xd6_u8, InstrModel{0xd6_u8, "setlocal_2", {}}},
      {0xd7_u8, InstrModel{0xd7_u8, "setlocal_3", {}}},
      {0x61_u8, InstrModel{0x61_u8, "setproperty", {INSTROPERANDU30}}},
      {0x6d_u8, InstrModel{0x6d_u8, "setslot", {INSTROPERANDU30}}},
      {0x05_u8, InstrModel{0x05_u8, "setsuper", {INSTROPERANDU30}}},
      {0x3D_u8, InstrModel{0x3D_u8, "sf32", {}}},
      {0x3E_u8, InstrModel{0x3E_u8, "sf64", {}}},
      {0x3B_u8, InstrModel{0x3B_u8, "si16", {}}},
      {0x3C_u8, InstrModel{0x3C_u8, "si32", {}}},
      {0x3A_u8, InstrModel{0x3A_u8, "si8", {}}},
      {0xac_u8, InstrModel{0xac_u8, "strictequals", {}}},
      {0xa1_u8, InstrModel{0xa1_u8, "subtract", {}}},
      {0xc6_u8, InstrModel{0xc6_u8, "subtract_i", {}}},
      {0x2b_u8, InstrModel{0x2b_u8, "swap", {}}},
      {0x03_u8, InstrModel{0x03_u8, "throw", {}}},
      {0x95_u8, InstrModel{0x95_u8, "typeof", {}}},
      {0xa7_u8, InstrModel{0xa7_u8, "urshift", {}}},
};
}   // namespace

uint32 disassemble_inst_operand(SwfReader &r, OperandType t) {
    switch (t) {
    case INSTROPERANDU30:
        return r.read_var_u30();
    case INSTROPERANDS16:
        return static_cast<uint32>(r.read_int16());
    case INSTROPERANDS24:
        return static_cast<uint32>(r.read_int24());
    case INSTROPERANDU8:
        return static_cast<uint32>(r.read_uint8());
    default:
        throw std::runtime_error("UNKNOWN operand.");
    }
}

Instr disassemble_instr(SwfReader &r, uint8 const code) {
    InstrModel const   &model = instructions.at(code);
    std::vector<uint32> operands{};
    for (InstrOperand t : model.operands) {
        OperandType const operand{static_cast<OperandType>(t)};
        if (t == INSTROPERANDCASECOUNT) {
            uint32 const count{r.read_var_u30()};
            for (unsigned i{0}; i < count + 1; ++i) {
                operands.push_back(
                      disassemble_inst_operand(r, INSTROPERANDS24));
            }
        } else {
            operands.push_back(disassemble_inst_operand(r, operand));
        }
    }
    return Instr{model, operands};
}

std::vector<Instr> disassemble_code(SwfReader  &reader,
                                    std::size_t code_length) {
    std::vector<Instr> res;
    std::size_t const  code_start{reader.tellg()};
    while ((reader.tellg() - code_start) < code_length) {
        uint8 const code{reader.read_uint8()};
        res.push_back(disassemble_instr(reader, code));
    }
    return res;
}

std::ostream &operator<<(std::ostream &os, Instr const &i) {
    return os << i.to_string();
}

std::string Instr::to_string() const {
    std::string result = this->model.name + " ";
    for (uint32 operand : this->operands) {
        result += std::to_string(operand) + " ";
    }
    return result;
}
