#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODBODY_EXCEPTIONINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODBODY_EXCEPTIONINFO_H

#include <istream>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"

namespace abc {

struct ExceptionInfo {
    explicit ExceptionInfo(SwfReader &reader);

    uint32 m_from;
    uint32 m_to;
    uint32 m_target;
    uint32 m_exc_type;
    uint32 m_var_name;
};

}   // namespace abc
#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODBODY_EXCEPTIONINFO_H
