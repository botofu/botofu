#include "botofu/swf/parser/abc/Metadata/ItemInfo.h"

abc::ItemInfo::ItemInfo(SwfReader &reader) {
    m_key   = reader.read_var_u30();
    m_value = reader.read_var_u30();
}

std::string abc::ItemInfo::to_string(const abc::ConstantPoolInfo &constant_pool,
                                     unsigned indent_level) const {
    if (m_key) {
        return std::string(indent_level, '\t')
               + constant_pool.get_string_info(m_key).to_string()
               + std::string(" => ")
               + constant_pool.get_string_info(m_value).to_string();
    }
    return std::string(indent_level, '\t')
           + constant_pool.get_string_info(m_value).to_string();
}
