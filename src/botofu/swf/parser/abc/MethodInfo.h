#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_H

#include <optional>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/MethodInfo/OptionInfo.h"
#include "botofu/swf/parser/abc/MethodInfo/ParamInfo.h"

namespace abc {

struct MethodInfo {
    explicit MethodInfo(SwfReader &reader);

    [[nodiscard]] std::string
    get_signature(ConstantPoolInfo const &constant_pool_info,
                  unsigned                indent_level) const;

    [[nodiscard]] bool has_param_names() const;

    [[nodiscard]] bool has_optional() const;

    [[nodiscard]] std::string
    get_name(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] std::string
    get_return_type(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] std::string
    get_param_type(ConstantPoolInfo const &constant_pool_info,
                   std::size_t             parameter_id) const;

    [[nodiscard]] std::string
    get_param_name(ConstantPoolInfo const &constant_pool_info,
                   std::size_t             parameter_id) const;

    [[nodiscard]] bool
    is_in_network_namespace(ConstantPoolInfo const &constant_pool) const;

    [[nodiscard]] bool
    is_network_enumeration(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] bool
    is_network_type(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] bool
    is_network_message(ConstantPoolInfo const &constant_pool_info) const;

    uint32                         m_param_count;
    uint32                         m_return_type;
    std::vector<uint32>            m_param_type;
    uint32                         m_name;
    uint8                          m_flags;
    std::optional<abc::OptionInfo> m_options;
    std::optional<abc::ParamInfo>  m_param_names;
};

}   // namespace abc
#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_H
