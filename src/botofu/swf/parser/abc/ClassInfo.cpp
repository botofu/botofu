#include "botofu/swf/parser/abc/ClassInfo.h"

abc::ClassInfo::ClassInfo(SwfReader &reader) {
    m_cinit       = reader.read_var_u30();
    m_trait_count = reader.read_var_u30();
    for (uint32 i{0}; i < m_trait_count; ++i) {
        m_traits.emplace_back(reader);
    }
}

std::string abc::ClassInfo::to_string(TagDoAbc const &tag_do_abc,
                                      unsigned        indent_level) const {
    std::string result(indent_level, '\t');
    result += "ClassInfo(cinit=" + std::to_string(m_cinit) + ", traits = [";
    if (!m_traits.empty()) {
        result += "\n";
        for (auto const &trait : m_traits) {
            result += trait.to_string(tag_do_abc, indent_level + 1) + ",\n";
        }
    }
    result += "])";
    return result;
}
