#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METADATA_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METADATA_H

#include <istream>
#include <string>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/Metadata/ItemInfo.h"

namespace abc {

struct Metadata {
    explicit Metadata(SwfReader &reader);

    [[nodiscard]] std::string
    to_string(abc::ConstantPoolInfo const &constant_pool,
              unsigned                     indent_level) const;

    uint32                     m_name;
    uint32                     m_item_count;
    std::vector<abc::ItemInfo> m_items;
};

}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METADATA_H
