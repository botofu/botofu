#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_H

#include <ostream>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/Multiname.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/Namespace.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/NamespaceSet.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/StringInfo.h"

namespace abc {

class ConstantPoolInfo {
public:
    ConstantPoolInfo() = default;

    explicit ConstantPoolInfo(SwfReader &reader);

    uint32 m_int_count{};
    uint32 m_uint_count{};
    uint32 m_double_count{};
    uint32 m_string_count{};
    uint32 m_namespace_count{};
    uint32 m_namespace_set_count{};
    uint32 m_multiname_count{};

    [[nodiscard]] int32 const &get_integer(uint32 index) const {
        return m_integers.at(index);
    }

    [[nodiscard]] uint32 const &get_uinteger(uint32 index) const {
        return m_uintegers.at(index);
    }

    [[nodiscard]] double const &get_double(uint32 index) const {
        return m_doubles.at(index);
    }

    [[nodiscard]] abc::StringInfo const &get_string_info(uint32 index) const {
        return m_strings.at(index);
    }

    [[nodiscard]] abc::Namespace const &get_namespace(uint32 index) const {
        return m_namespaces.at(index);
    }

    [[nodiscard]] abc::NamespaceSet const &
    get_namespace_set(uint32 index) const {
        return m_namespace_sets.at(index);
    }

    [[nodiscard]] abc::Multiname const &get_multiname(uint32 index) const {
        return m_multinames.at(index);
    }

private:
    std::vector<int32>             m_integers;
    std::vector<uint32>            m_uintegers;
    std::vector<double>            m_doubles;
    std::vector<abc::StringInfo>   m_strings;
    std::vector<abc::Namespace>    m_namespaces;
    std::vector<abc::NamespaceSet> m_namespace_sets;
    std::vector<abc::Multiname>    m_multinames;
};

}   // namespace abc

std::ostream &operator<<(std::ostream                &os,
                         abc::ConstantPoolInfo const &constant_pool);

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_H
