#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_SCRIPTINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_SCRIPTINFO_H

#include <istream>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/InstanceInfo/TraitInfo.h"

namespace abc {

struct ScriptInfo {
    explicit ScriptInfo(SwfReader &reader);

    uint32                      m_init;
    uint32                      m_trait_count;
    std::vector<abc::TraitInfo> m_traits;
};

}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_SCRIPTINFO_H
