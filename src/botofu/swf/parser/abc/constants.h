#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTS_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTS_H

#include <map>
#include <string>

#include "botofu/swf/parser/abc/enumerations.h"

namespace abc {
std::string const NAMESPACE_SEPARATOR{"."};
std::string const NAMESPACE_CLASS_SEPARATOR{":"};
std::string const CLASS_ATTRIBUTE_SEPARATOR{":"};
std::string const CLASS_METHOD_SEPARATOR{"/"};

std::string const GETTER_POSTFIX{"/get"};
std::string const SETTER_POSTFIX{"/set"};

std::string const NETWORK_NAMESPACE{"com.ankamagames.dofus.network"};
std::string const DATACENTER_NAMESPACE{"com.ankamagames.dofus.datacenter"};

std::string const NETWORK_MESSAGE_NAMESPACE{NETWORK_NAMESPACE + ".messages"};
std::string const NETWORK_TYPES_NAMESPACE{NETWORK_NAMESPACE + ".types"};
std::string const NETWORK_ENUMS_NAMESPACE{NETWORK_NAMESPACE + ".enums"};

std::map<TraitKind, std::string> const TRAIT_KIND_NAME = {
      {TraitKind::SLOT, "SLOT"},
      {TraitKind::METHOD, "METHOD"},
      {TraitKind::GETTER, "GETTER"},
      {TraitKind::SETTER, "SETTER"},
      {TraitKind::CLASS, "CLASS"},
      {TraitKind::FUNCTION, "FUNCTION"},
      {TraitKind::CONST_, "CONST"}};
}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTS_H
