#include "botofu/swf/parser/abc/InstanceInfo.h"

#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include "botofu/swf/parser/abc/constants.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

using boost::algorithm::starts_with;

abc::InstanceInfo::InstanceInfo(SwfReader &reader) {
    constexpr uint8 constant_class_protected_ns = 0x08;

    m_name       = reader.read_var_u30();
    m_super_name = reader.read_var_u30();
    m_flags      = reader.read_uint8();
    if (m_flags & constant_class_protected_ns) {
        m_protected_namespace = reader.read_var_u30();
    }
    m_interface_count = reader.read_var_u30();
    m_interfaces.reserve(m_interface_count);
    for (uint32 i{0}; i < m_interface_count; ++i) {
        m_interfaces.push_back(reader.read_var_u30());
    }
    m_instance_initializer = reader.read_var_u30();
    m_trait_count          = reader.read_var_u30();
    m_traits.reserve(m_trait_count);
    for (uint32 i{0}; i < m_trait_count; ++i) {
        m_traits.emplace_back(reader);
    }
}

std::string
abc::InstanceInfo::get_name(abc::ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_multiname(m_name).to_string(constant_pool);
}

std::string abc::InstanceInfo::get_super_name(
      abc::ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_multiname(m_super_name).to_string(constant_pool);
}

bool abc::InstanceInfo::is_in_network_namespace(
      abc::ConstantPoolInfo const &constant_pool) const {
    // The current instance is in the network namespace if and only if it's name
    // contains the name of the namespace AND the name of the namespace is at
    // the beginning of the string.
    return starts_with(this->get_name(constant_pool), abc::NETWORK_NAMESPACE);
}

bool abc::InstanceInfo::is_in_datacenter_namespace(
      abc::ConstantPoolInfo const &constant_pool) const {
    // The current instance is in the datacenter namespace if and only if it's
    // name contains the name of the namespace AND the name of the namespace is
    // at the beginning of the string.
    return starts_with(this->get_name(constant_pool),
                       abc::DATACENTER_NAMESPACE);
}

std::vector<std::string> abc::InstanceInfo::get_interfaces_names(
      abc::ConstantPoolInfo const &constant_pool) const {
    std::vector<std::string> names;
    names.reserve(m_interface_count);
    for (std::size_t i{0}; i < m_interface_count; ++i) {
        names.push_back(constant_pool.get_multiname(m_interfaces[i])
                              .to_string(constant_pool));
    }
    return names;
}

std::string abc::InstanceInfo::to_string(TagDoAbc const &tag_do_abc,
                                         unsigned        indent_level) const {
    std::string result =
          std::string(indent_level, '\t') + "class "
          + this->get_name(tag_do_abc.m_constant_pool_info) + " : "
          + this->get_super_name(tag_do_abc.m_constant_pool_info) + ", "
          + boost::join(
                this->get_interfaces_names(tag_do_abc.m_constant_pool_info),
                ", ");
    result += "\n" + std::string(indent_level, '\t') + "{\n";
    for (auto const &trait : m_traits) {
        result += trait.to_string(tag_do_abc, indent_level + 1) + ",\n";
    }
    result += std::string(indent_level, '\t') + "}";
    return result;
}

bool abc::InstanceInfo::is_network_enumeration(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return starts_with(this->get_name(constant_pool_info),
                       abc::NETWORK_ENUMS_NAMESPACE);
}

bool abc::InstanceInfo::is_network_type(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return starts_with(this->get_name(constant_pool_info),
                       abc::NETWORK_TYPES_NAMESPACE);
}

bool abc::InstanceInfo::is_network_message(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return starts_with(this->get_name(constant_pool_info),
                       abc::NETWORK_MESSAGE_NAMESPACE);
}
