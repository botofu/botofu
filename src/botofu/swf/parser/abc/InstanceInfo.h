#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_INSTANCEINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_INSTANCEINFO_H

#include <optional>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/InstanceInfo/TraitInfo.h"

struct TagDoAbc;

namespace abc {

/**
 * Store informations about runtime objects that are instances.
 */
struct InstanceInfo {
    /**
     * Constructor of the InstanceInfo class.
     * @param reader The source from where we read the data needed to construct
     * our InstanceInfo class.
     */
    explicit InstanceInfo(SwfReader &reader);

    /**
     * GETTER for the instance name.
     * @param constant_pool The structure that store all the static constants,
     * as described in the AVM2 specification.
     * @return The name of the instance.
     */
    [[nodiscard]] std::string
    get_name(abc::ConstantPoolInfo const &constant_pool) const;

    /**
     * GETTER for the instance super-class name.
     * @param constant_pool The structure that store all the static constants,
     * as described in the AVM2 specification.
     * @return The name of the super-class of the instance.
     */
    [[nodiscard]] std::string
    get_super_name(abc::ConstantPoolInfo const &constant_pool) const;

    [[nodiscard]] std::vector<std::string>
    get_interfaces_names(abc::ConstantPoolInfo const &constant_pool) const;

    [[nodiscard]] std::string to_string(TagDoAbc const &tag_do_abc,
                                        unsigned        indent_level) const;

    /**
     * Helper method to know if the current instance is in the network namespace
     * or not.
     * @param constant_pool The structure that store all the static constants,
     * as described in the AMV2 specification.
     * @return true if the instance is in the network namespace, false
     * otherwise.
     */
    [[nodiscard]] bool
    is_in_network_namespace(abc::ConstantPoolInfo const &constant_pool) const;

    [[nodiscard]] bool
    is_network_enumeration(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] bool
    is_network_type(ConstantPoolInfo const &constant_pool_info) const;

    [[nodiscard]] bool
    is_network_message(ConstantPoolInfo const &constant_pool_info) const;

    /**
     * Helper method to know if the current instance is in the datacenter
     * namespace or not.
     * @param constant_pool The structure that store all the static constants,
     * as described in the AMV2 specification.
     * @return true if the instance is in the datacenter namespace, false
     * otherwise.
     */
    [[nodiscard]] bool is_in_datacenter_namespace(
          abc::ConstantPoolInfo const &constant_pool) const;

    uint32                      m_name;
    uint32                      m_super_name;
    uint8                       m_flags;
    std::optional<uint32>       m_protected_namespace;
    uint32                      m_interface_count;
    std::vector<uint32>         m_interfaces;
    uint32                      m_instance_initializer;
    uint32                      m_trait_count;
    std::vector<abc::TraitInfo> m_traits;
};

}   // namespace abc
#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_INSTANCEINFO_H
