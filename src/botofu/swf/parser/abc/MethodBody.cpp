#include "botofu/swf/parser/abc/MethodBody.h"

#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

abc::MethodBody::MethodBody(SwfReader &reader) {
    m_method           = reader.read_var_u30();
    m_max_stack        = reader.read_var_u30();
    m_local_count      = reader.read_var_u30();
    m_init_scope_depth = reader.read_var_u30();
    m_max_scope_depth  = reader.read_var_u30();
    m_code_length      = reader.read_var_u30();
    m_code             = reader.read_bytes(m_code_length);
    m_exception_count  = reader.read_var_u30();
    for (uint32 i{0}; i < m_exception_count; ++i) {
        m_exceptions.emplace_back(reader);
    }
    m_trait_count = reader.read_var_u30();
    for (uint32 i{0}; i < m_trait_count; ++i) {
        m_traits.emplace_back(reader);
    }
}

std::vector<Instr> abc::MethodBody::disassemble() const {
    SwfReader code_reader(m_code.data(), m_code.size());
    return disassemble_code(code_reader, m_code.size());
}

abc::MethodInfo const &
abc::MethodBody::get_method_info(TagDoAbc const &tag_do_abc) const {
    return tag_do_abc.m_methods_info[m_method];
}

std::string abc::MethodBody::to_string(TagDoAbc const &tag_do_abc,
                                       unsigned        indent_level) const {
    std::string result(indent_level, '\t');
    result +=
          this->get_method_info(tag_do_abc)
                .get_signature(tag_do_abc.m_constant_pool_info, indent_level);
    result += "{\n";
    //    if(! m_traits.empty()) {
    //        result += "\n";
    //        for (auto const &trait : m_traits) {
    //            result += trait.to_string(tag_do_abc, indent_level + 1) +
    //            ",\n";
    //        }
    //    }
    for (Instr const &instruction : this->disassemble()) {
        result += std::string(indent_level + 1, '\t') + instruction.to_string()
                  + "\n";
    }
    result += std::string(indent_level, '\t') + "}";
    return result;
}
