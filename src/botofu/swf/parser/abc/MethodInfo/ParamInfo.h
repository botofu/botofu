#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_PARAMINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_PARAMINFO_H

#include <istream>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"

namespace abc {

struct ParamInfo {
    ParamInfo(SwfReader &reader, uint32 param_count);

    std::vector<uint32> param_names;
};

}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_PARAMINFO_H
