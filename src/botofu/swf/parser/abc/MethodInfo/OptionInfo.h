#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_OPTIONINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_OPTIONINFO_H

#include <istream>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/MethodInfo/OptionDetail.h"

namespace abc {

struct OptionInfo {
    explicit OptionInfo(SwfReader &reader);

    uint32                    m_option_count;
    std::vector<OptionDetail> m_options;
};

}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_OPTIONINFO_H
