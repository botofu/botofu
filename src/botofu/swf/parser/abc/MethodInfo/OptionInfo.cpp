#include "botofu/swf/parser/abc/MethodInfo/OptionInfo.h"

abc::OptionInfo::OptionInfo(SwfReader &reader) {
    m_option_count = reader.read_var_u30();
    for (uint32 i{0}; i < m_option_count; ++i) {
        m_options.emplace_back(reader);
    }
}