#include "botofu/swf/parser/abc/MethodInfo/OptionDetail.h"

abc::OptionDetail::OptionDetail(SwfReader &reader) {
    m_val  = reader.read_var_u30();
    m_kind = static_cast<ConstantKind>(reader.read_uint8());
}