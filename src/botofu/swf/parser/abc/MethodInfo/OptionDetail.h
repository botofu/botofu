#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_OPTIONDETAIL_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_OPTIONDETAIL_H

#include <istream>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/enumerations.h"

namespace abc {

struct OptionDetail {
    explicit OptionDetail(SwfReader &reader);

    uint32       m_val;
    ConstantKind m_kind;
};

}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODINFO_OPTIONDETAIL_H
