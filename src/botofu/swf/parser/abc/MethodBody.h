#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODBODY_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODBODY_H

#include <istream>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/InstanceInfo/TraitInfo.h"
#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"
#include "botofu/swf/parser/abc/MethodBody/ExceptionInfo.h"
#include "botofu/swf/parser/abc/MethodInfo.h"

namespace abc {

struct MethodBody {
    explicit MethodBody(SwfReader &reader);

    [[nodiscard]] std::vector<Instr> disassemble() const;

    [[nodiscard]] abc::MethodInfo const &
    get_method_info(TagDoAbc const &tag_do_abc) const;

    [[nodiscard]] std::string to_string(TagDoAbc const &tag_do_abc,
                                        unsigned        indent_level) const;

    uint32                          m_method;
    uint32                          m_max_stack;
    uint32                          m_local_count;
    uint32                          m_init_scope_depth;
    uint32                          m_max_scope_depth;
    uint32                          m_code_length;
    ByteArray                       m_code;
    uint32                          m_exception_count;
    std::vector<abc::ExceptionInfo> m_exceptions;
    uint32                          m_trait_count;
    std::vector<abc::TraitInfo>     m_traits;
};

}   // namespace abc
#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_METHODBODY_H
