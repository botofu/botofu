#include "botofu/swf/parser/abc/MethodInfo.h"

#include <boost/algorithm/string/predicate.hpp>

#include "botofu/swf/parser/abc/constants.h"

using boost::algorithm::starts_with;

abc::MethodInfo::MethodInfo(SwfReader &reader) {
    // constexpr UByte NEED_ARGUMENTS  = 0x01;
    // constexpr UByte NEED_ACTIVATION = 0x02;
    // constexpr UByte NEED_REST       = 0x04;
    constexpr uint8 has_optional = 0x08;
    // constexpr UByte SET_DXNS        = 0x40;
    constexpr uint8 has_param_names = 0x80;

    m_param_count = reader.read_var_u30();
    m_return_type = reader.read_var_u30();
    for (uint32 i{0}; i < m_param_count; ++i) {
        m_param_type.push_back(reader.read_var_u30());
    }
    m_name  = reader.read_var_u30();
    m_flags = reader.read_uint8();
    if (m_flags & has_optional) {
        m_options = abc::OptionInfo(reader);
    }
    if (m_flags & has_param_names) {
        m_param_names = abc::ParamInfo(reader, m_param_count);
    }
}

std::string
abc::MethodInfo::get_signature(const abc::ConstantPoolInfo &constant_pool_info,
                               unsigned indent_level) const {
    std::string result(indent_level, '\t');
    result += this->get_return_type(constant_pool_info) + " ";
    result += this->get_name(constant_pool_info) + "(";
    for (std::size_t i{0}; i < m_param_count; ++i) {
        result += this->get_param_type(constant_pool_info, i);
        if (this->has_param_names()) {
            result += " " + this->get_param_name(constant_pool_info, i);
        }
        result += ", ";
    }
    result += ")";
    return result;
}

bool abc::MethodInfo::has_param_names() const {
    constexpr uint8 has_param_names{0x80};
    return (m_flags & has_param_names) && m_param_names;
}

bool abc::MethodInfo::has_optional() const {
    constexpr uint8 has_optional{0x08};
    return (m_flags & has_optional) && m_options;
}

std::string abc::MethodInfo::get_name(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return constant_pool_info.get_string_info(m_name).to_string();
}

std::string abc::MethodInfo::get_return_type(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return constant_pool_info.get_multiname(m_return_type)
          .to_string(constant_pool_info);
}

std::string
abc::MethodInfo::get_param_type(const abc::ConstantPoolInfo &constant_pool_info,
                                std::size_t parameter_id) const {
    return constant_pool_info.get_multiname(m_param_type[parameter_id])
          .to_string(constant_pool_info);
}

std::string
abc::MethodInfo::get_param_name(const abc::ConstantPoolInfo &constant_pool_info,
                                std::size_t parameter_id) const {
    return constant_pool_info
          .get_string_info(m_param_names->param_names[parameter_id])
          .to_string();
}

bool abc::MethodInfo::is_in_network_namespace(
      ConstantPoolInfo const &constant_pool_info) const {
    return starts_with(this->get_name(constant_pool_info),
                       abc::NETWORK_NAMESPACE);
}

bool abc::MethodInfo::is_network_enumeration(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return starts_with(this->get_name(constant_pool_info),
                       abc::NETWORK_ENUMS_NAMESPACE);
}

bool abc::MethodInfo::is_network_type(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return starts_with(this->get_name(constant_pool_info),
                       abc::NETWORK_TYPES_NAMESPACE);
}

bool abc::MethodInfo::is_network_message(
      const abc::ConstantPoolInfo &constant_pool_info) const {
    return starts_with(this->get_name(constant_pool_info),
                       abc::NETWORK_MESSAGE_NAMESPACE);
}
