#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_NAMESPACE_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_NAMESPACE_H

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/enumerations.h"

namespace abc {

// Pre-declaration of the ConstantPoolInfo class.
class ConstantPoolInfo;

/**
 * Store data about a namespace.
 */
struct Namespace {
    /**
     * Default constructor.
     */
    Namespace();

    /**
     * Construct a NAMESPACE from an input data source that is a reader.
     * @param reader The input data source.
     */
    explicit Namespace(SwfReader &reader);

    /**
     * Get the string representation of the NAMESPACE class according to AMV2
     * specification.
     * @param constant_pool The structure that contains the constants defined in
     * the DOABC tag.
     * @return The string representation of the NAMESPACE class according to
     * AMV2 specification.
     */
    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &constant_pool) const;

    NamespaceKind m_kind;
    uint32        m_name;
};
}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_NAMESPACE_H
