#include "botofu/swf/parser/abc/ConstantPoolInfo/NamespaceSet.h"

#include <boost/algorithm/string/join.hpp>

#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/constants.h"

abc::NamespaceSet::NamespaceSet(SwfReader &reader) {
    m_count = reader.read_var_u30();
    m_namespace_set.reserve(m_count);
    for (uint32 i{0}; i < m_count; ++i) {
        m_namespace_set.push_back(reader.read_var_u30());
    }
}

std::string
abc::NamespaceSet::to_string(abc::ConstantPoolInfo const &constant_pool) const {
    std::string       result{};
    std::size_t const limit{m_namespace_set.size() - 1};
    for (uint32 namespace_set_index{0}; namespace_set_index < limit;
         ++namespace_set_index) {
        result.append(
              constant_pool.get_multiname(m_namespace_set[namespace_set_index])
                    .to_string(constant_pool));
        result.push_back(abc::NAMESPACE_SEPARATOR[0]);
    }
    result.append(constant_pool.get_multiname(m_namespace_set[limit])
                        .to_string(constant_pool));
    return result;
}
