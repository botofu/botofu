#include "botofu/swf/parser/abc/ConstantPoolInfo/StringInfo.h"

abc::StringInfo::StringInfo(SwfReader &reader) {
    uint32 const data_size{reader.read_var_u30()};
    m_data = reader.read_utf(data_size);
}

abc::StringInfo::StringInfo(std::string str) : m_data(std::move(str)) {
}

std::string abc::StringInfo::to_string() const {
    return m_data;
}
