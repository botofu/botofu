#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_MULTINAME_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_MULTINAME_H

#include <memory>
#include <optional>
#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/Namespace.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/NamespaceSet.h"
#include "botofu/swf/parser/abc/enumerations.h"

/**
 * CLASS representing an "name" (an identifiant) in the AMV2 specification.
 *
 * This class holds an identifiant and either a namespace or a namespace set.
 */
namespace abc {

class ConstantPoolInfo;

namespace multiname {

/**
 * A base structure to all the MULTINAME's kinds structure to play with
 * polymorphism.
 */
struct BaseMultiname {
    virtual ~BaseMultiname() = default;

    /**
     * Return the name of the structure.
     * @return A string representation of the structure's name.
     */
    [[nodiscard]] virtual std::string get_kind_name() const = 0;

    /**
     * Get the string representation of the structure according to AMV2
     * specifications.
     * @param constant_pool The structure that contains the constants defined in
     * the DOABC tag.
     * @return The string representation of the structure according to AMV2
     * specifications.
     */
    [[nodiscard]] virtual std::string
    to_string(ConstantPoolInfo const &constant_pool) const = 0;

    [[nodiscard]] virtual std::optional<abc::Namespace> get_namespace(
          [[maybe_unused]] ConstantPoolInfo const &constant_pool) const {
        return std::nullopt;
    }
    [[nodiscard]] virtual std::optional<abc::NamespaceSet> get_namespaces(
          [[maybe_unused]] ConstantPoolInfo const &constant_pool) const {
        return std::nullopt;
    }
    [[nodiscard]] virtual std::optional<std::string>
    get_name([[maybe_unused]] ConstantPoolInfo const &constant_pool) const {
        return std::nullopt;
    }
};

/** An empty class saying that the MULTINAME is not fully read at the moment. */
struct NothingForTheMoment : BaseMultiname {
    NothingForTheMoment() = default;

    [[nodiscard]] std::string get_kind_name() const override {
        return "NothingForTheMoment";
    }

    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &) const override {
        return "NothingForTheMoment";
    }
};

/** Qualified Name, a name with exactly one namespace. */
struct QName : BaseMultiname {
    explicit QName(SwfReader &reader);

    [[nodiscard]] std::string get_kind_name() const override {
        return "QNAME";
    }

    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &constant_pool) const override;

    [[nodiscard]] std::optional<abc::Namespace>
    get_namespace(ConstantPoolInfo const &constant_pool) const override;

    [[nodiscard]] std::optional<std::string>
    get_name(ConstantPoolInfo const &constant_pool) const override;

    uint32 m_namespace;
    uint32 m_name;
};

/**
 * RunTime Qualified Name, a name with a runtime resolution of the namespace.
 *
 * The namespace that should be used by the RTQNAME will be on the top of the
 * stack so the RTQNAME will need to pop one namespace from the stack.
 *
 * Example:
 * var ns = getANamespace();
 * x = ns::r;
 * -> the RTQNAME created is "r".
 */
struct RtqName : BaseMultiname {
    explicit RtqName(SwfReader &reader);

    [[nodiscard]] std::string get_kind_name() const override {
        return "RTQNAME";
    }

    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &constant_pool) const override;

    [[nodiscard]] std::optional<std::string>
    get_name(ConstantPoolInfo const &constant_pool) const override;

    uint32 m_name;
};

/**
 * RunTime Qualified Name Late, a name with a runtime resolution of both the
 * namespace and the name.
 *
 * The namespace and the name that should be used by the RTQNAMEL will be on the
 * top of the stack so the RTQNAMEL will need to pop 2 items from the stack.
 *
 * Example:
 * var x = getAName();
 * var ns = getANamespace();
 * w = ns::[x];
 */
struct RtqNameL : BaseMultiname {
    [[nodiscard]] std::string get_kind_name() const override {
        return "RTQNAMEL";
    }

    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &) const override {
        return "RTQNAMEL";
    }
};

/**
 * Multiple NAMESPACE Name, a name and multiple namespaces.
 */
struct Multiname : BaseMultiname {
    explicit Multiname(SwfReader &reader);

    [[nodiscard]] std::string get_kind_name() const override {
        return "MULTINAME";
    }

    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &constant_pool) const override;

    [[nodiscard]] std::optional<abc::NamespaceSet>
    get_namespaces(ConstantPoolInfo const &constant_pool) const override;

    [[nodiscard]] std::optional<std::string>
    get_name(ConstantPoolInfo const &constant_pool) const override;

    uint32 m_name;
    uint32 m_namespace_set;
};

/**
 * Multiple NAMESPACE Name Late, a name resolved at runtime and multiple
 * namespaces.
 */
struct MultinameL : BaseMultiname {
    explicit MultinameL(SwfReader &reader);

    [[nodiscard]] std::string get_kind_name() const override {
        return "MULTINAMEL";
    }

    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &constant_pool) const override;

    [[nodiscard]] std::optional<abc::NamespaceSet>
    get_namespaces(ConstantPoolInfo const &constant_pool) const override;

    uint32 m_namespace_set;
};

/**
 * This type is not described by the AMV2 documentation at the moment of
 * writing.
 *
 * The possible interpretation for this type is that it represent templated
 * types (only types of the form Vector<[type]>).
 */
struct TemplatedTypeMultiname : BaseMultiname {
    explicit TemplatedTypeMultiname(SwfReader &reader);

    [[nodiscard]] std::string get_kind_name() const override {
        return "TemplatedTypeMultiname";
    }

    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &constant_pool) const override;

    [[nodiscard]] std::optional<std::string>
    get_name(ConstantPoolInfo const &constant_pool) const override;

    uint32              m_name;
    uint32              m_param_count;
    std::vector<uint32> m_param_names;
};
}   // namespace multiname

/**
 * CLASS storing an abstract identifier.
 *
 * All the possible identifier types are stored in the structures in the
 * namespace "multiname".
 */
struct Multiname {
    /**
     * Default constructor of the MULTINAME class.
     */
    Multiname();

    explicit Multiname(SwfReader &reader);

    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &constant_pool) const;

    [[nodiscard]] std::string get_kind_representation() const;

    [[nodiscard]] std::optional<abc::Namespace>
    get_namespace(ConstantPoolInfo const &constant_pool) const;
    [[nodiscard]] std::optional<abc::NamespaceSet>
    get_namespaces(ConstantPoolInfo const &constant_pool) const;
    [[nodiscard]] std::optional<std::string>
    get_name(ConstantPoolInfo const &constant_pool) const;

    MultinameKind                             m_kind;
    std::unique_ptr<multiname::BaseMultiname> m_data;
};

}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_MULTINAME_H
