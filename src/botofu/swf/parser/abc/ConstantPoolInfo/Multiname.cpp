#include "botofu/swf/parser/abc/ConstantPoolInfo/Multiname.h"

#include <memory>

#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/constants.h"

abc::multiname::QName::QName(SwfReader &reader) {
    m_namespace = reader.read_var_u30();
    m_name      = reader.read_var_u30();
}

std::string abc::multiname::QName::to_string(
      abc::ConstantPoolInfo const &constant_pool) const {
    std::string result{
          constant_pool.get_namespace(m_namespace).to_string(constant_pool)};
    if (!result.empty()) {
        result += abc::NAMESPACE_CLASS_SEPARATOR;
    }
    result += constant_pool.get_string_info(m_name).to_string();
    return result;
}

std::optional<abc::Namespace> abc::multiname::QName::get_namespace(
      ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_namespace(this->m_namespace);
}

std::optional<std::string>
abc::multiname::QName::get_name(ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_string_info(this->m_name).to_string();
}

abc::multiname::RtqName::RtqName(SwfReader &reader) {
    m_name = reader.read_var_u30();
}

std::string abc::multiname::RtqName::to_string(
      const abc::ConstantPoolInfo &constant_pool) const {
    return constant_pool.get_string_info(m_name).to_string();
}

std::optional<std::string>
abc::multiname::RtqName::get_name(ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_string_info(this->m_name).to_string();
}

abc::multiname::Multiname::Multiname(SwfReader &reader) {
    m_name          = reader.read_var_u30();
    m_namespace_set = reader.read_var_u30();
}

std::string abc::multiname::Multiname::to_string(
      const abc::ConstantPoolInfo &constant_pool) const {
    return constant_pool.get_namespace_set(m_namespace_set)
                 .to_string(constant_pool)
           + abc::NAMESPACE_CLASS_SEPARATOR
           + constant_pool.get_string_info(m_name).to_string();
}

std::optional<abc::NamespaceSet> abc::multiname::Multiname::get_namespaces(
      abc::ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_namespace_set(this->m_namespace_set);
}

std::optional<std::string> abc::multiname::Multiname::get_name(
      ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_string_info(this->m_name).to_string();
}

abc::multiname::MultinameL::MultinameL(SwfReader &reader) {
    m_namespace_set = reader.read_var_u30();
}

std::string abc::multiname::MultinameL::to_string(
      const abc::ConstantPoolInfo &constant_pool) const {
    return constant_pool.get_namespace_set(m_namespace_set)
          .to_string(constant_pool);
}

std::optional<abc::NamespaceSet> abc::multiname::MultinameL::get_namespaces(
      abc::ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_namespace_set(this->m_namespace_set);
}

abc::multiname::TemplatedTypeMultiname::TemplatedTypeMultiname(
      SwfReader &reader) {
    m_name        = reader.read_var_u30();
    m_param_count = reader.read_var_u30();
    for (uint32 i{0}; i < m_param_count; ++i) {
        m_param_names.push_back(reader.read_var_u30());
    }
}

std::string abc::multiname::TemplatedTypeMultiname::to_string(
      const abc::ConstantPoolInfo &constant_pool) const {
    std::string result =
          constant_pool.get_multiname(m_name).to_string(constant_pool);
    if (m_param_count > 0) {
        result += "<";
        for (auto const &param_name : m_param_names) {
            result += constant_pool.get_multiname(param_name)
                            .to_string(constant_pool)
                      + ",";
        }
        result.pop_back();
        result += '>';
    }
    return result;
}

std::optional<std::string> abc::multiname::TemplatedTypeMultiname::get_name(
      ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_string_info(this->m_name).to_string();
}

abc::Multiname::Multiname()
      : m_kind(MultinameKind::UNKNOWN),
        m_data(std::make_unique<multiname::NothingForTheMoment>()) {
}

abc::Multiname::Multiname(SwfReader &reader)
      : m_data(std::make_unique<multiname::NothingForTheMoment>()) {
    m_kind = static_cast<MultinameKind>(reader.read_uint8());

    if (m_kind == MultinameKind::QNAME || m_kind == MultinameKind::QNAMEA) {
        m_data = std::make_unique<multiname::QName>(reader);
    } else if (m_kind == MultinameKind::RTQNAME
               || m_kind == MultinameKind::RTQNAMEA) {
        m_data = std::make_unique<multiname::RtqName>(reader);
    } else if (m_kind == MultinameKind::RTQNAMEL
               || m_kind == MultinameKind::RTQNAMELA) {
        m_data = std::make_unique<multiname::RtqNameL>();
    } else if (m_kind == MultinameKind::MULTINAME
               || m_kind == MultinameKind::MULTINAMEA) {
        m_data = std::make_unique<multiname::Multiname>(reader);
    } else if (m_kind == MultinameKind::MULTINAMEL
               || m_kind == MultinameKind::MULTINAMELA) {
        m_data = std::make_unique<multiname::MultinameL>(reader);
    } else if (m_kind == MultinameKind::UNKNOWNTYPENAME) {
        m_data = std::make_unique<multiname::TemplatedTypeMultiname>(reader);
    }
}

std::string abc::Multiname::get_kind_representation() const {
    return m_data->get_kind_name();
}

std::string
abc::Multiname::to_string(ConstantPoolInfo const &constant_pool) const {
    return m_data->to_string(constant_pool);
}

std::optional<abc::Namespace>
abc::Multiname::get_namespace(ConstantPoolInfo const &constant_pool) const {
    return this->m_data->get_namespace(constant_pool);
}

[[nodiscard]] std::optional<abc::NamespaceSet>
abc::Multiname::get_namespaces(ConstantPoolInfo const &constant_pool) const {
    return this->m_data->get_namespaces(constant_pool);
}

[[nodiscard]] std::optional<std::string>
abc::Multiname::get_name(ConstantPoolInfo const &constant_pool) const {
    return this->m_data->get_name(constant_pool);
}
