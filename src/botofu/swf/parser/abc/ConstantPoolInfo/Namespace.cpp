#include "botofu/swf/parser/abc/ConstantPoolInfo/Namespace.h"

#include "botofu/swf/parser/abc/ConstantPoolInfo.h"

abc::Namespace::Namespace()
      : m_kind(NamespaceKind::NAMESPACE),
        m_name(0)   // "*" namespace by default
{
}

abc::Namespace::Namespace(SwfReader &reader) {
    m_kind = static_cast<NamespaceKind>(reader.read_uint8());
    m_name = reader.read_var_u30();
}

std::string
abc::Namespace::to_string(abc::ConstantPoolInfo const &constant_pool) const {
    return constant_pool.get_string_info(m_name).to_string();
}