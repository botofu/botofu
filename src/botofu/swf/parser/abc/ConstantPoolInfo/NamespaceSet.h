#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_NAMESPACESET_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_NAMESPACESET_H

#include <vector>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"

namespace abc {

// Pre-declaration of the ConstantPoolInfo class.
class ConstantPoolInfo;

/**
 * Store data about a set of namespaces.
 */
struct NamespaceSet {
    /**
     * Default constructor of the NamespaceSet class.
     */
    NamespaceSet() = default;

    /**
     * Constructs a NamespaceSet from a data reader.
     * @param reader The input reader.
     */
    explicit NamespaceSet(SwfReader &reader);

    /**
     * Get the string representation of the NamespaceSet class according to AMV2
     * specification.
     * @param constant_pool The structure that contains the constants defined in
     * the DOABC tag.
     * @return The string representation of the NamespaceSet class according to
     * AMV2 specification.
     */
    [[nodiscard]] std::string
    to_string(ConstantPoolInfo const &constant_pool) const;

    uint32              m_count{};
    std::vector<uint32> m_namespace_set;
};
}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_NAMESPACESET_H
