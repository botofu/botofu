#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_STRINGINFO_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_STRINGINFO_H

#include <istream>
#include <string>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"

namespace abc {

/**
 * Store a string.
 */
struct StringInfo {
    /**
     * Constructs a StringInfo from the given reader.
     * @param reader The data source from where we read the StringInfo.
     */
    explicit StringInfo(SwfReader &reader);

    /**
     * Construct a StringInfo from a given string.
     * @param str The string that the constructed StringInfo will hold.
     */
    explicit StringInfo(std::string str);

    /**
     * Get the string representation of the StringInfo class according to AMV2
     * specification.
     * @return The string representation of the StringInfo class.
     */
    [[nodiscard]] std::string to_string() const;

    /**
     * The stored string.
     */
    std::string m_data;
};
}   // namespace abc

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_ABC_CONSTANTPOOLINFO_STRINGINFO_H
