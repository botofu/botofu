#include "botofu/swf/parser/SwfHeader.h"

#include <string>

SwfHeader::SwfHeader(SwfReader &reader)
      : m_compression{static_cast<Compression>(reader.read_uint8())},
        m_always_ws{reader.read_uint16()}, m_version{reader.read_uint8()},
        m_file_length{reader.read_uint32()}, m_frame_size{reader.read_rect()},
        m_frame_rate{reader.read_fixed_point<uint8>()},
        m_frame_count{reader.read_uint16()} {
}

std::ostream &operator<<(std::ostream &os, SwfHeader const &swf_header) {
    return os << "SwfHeader( "
              << "Compression = " << swf_header.get_compression_string() << ", "
              << "Signature = "
              << static_cast<uint8>(swf_header.m_always_ws >> 8u)
              << static_cast<uint8>(swf_header.m_always_ws) << ", "
              << "Version = " << static_cast<uint16>(swf_header.m_version)
              << ", "
              << "File length = " << swf_header.m_file_length << ", "
              << "Frame size = " << swf_header.m_frame_size << ", "
              << "Frame rate = " << swf_header.m_frame_rate << ", "
              << "Frame count = " << swf_header.m_frame_count << ")";
}

std::string SwfHeader::get_compression_string() const {
    if (m_compression == Compression::UNCOMPRESSED) {
        return "UNCOMPRESSED";
    } else if (m_compression == Compression::ZLIB) {
        return "ZLIB";
    } else if (m_compression == Compression::LZMA) {
        return "LZMA";
    } else {
        return std::string("BAD_COMPRESSION_CODE (")
               + std::to_string(static_cast<uint8>(m_compression))
               + std::string(")");
    }
}
