#include "botofu/swf/parser/SwfStructs.h"

std::ostream &operator<<(std::ostream &os, SwfRect const &rect) {
    return os << "Rect(" << rect.m_x_min.to_ulong() << ", "
              << rect.m_x_max.to_ulong() << ", " << rect.m_y_min.to_ulong()
              << ", " << rect.m_y_max.to_ulong() << ")";
}
