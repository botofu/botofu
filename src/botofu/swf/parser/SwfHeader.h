#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFHEADER_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFHEADER_H

#include <istream>
#include <string>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"

enum class Compression : uint8 { UNCOMPRESSED = 'F', ZLIB = 'C', LZMA = 'Z' };

class SwfHeader {
public:
    constexpr static unsigned int bytes_read_before_compression{8};

    SwfHeader() = delete; /**< Deleted default constructor.*/
    SwfHeader(SwfHeader const &other) = delete; /**< Deleted copy constructor.*/
    SwfHeader(SwfHeader &&other)      = delete; /**< Deleted constructor.*/
    SwfHeader &
    operator=(SwfHeader const &other) = delete; /**< Deleted copy operator.*/
    SwfHeader &
    operator=(SwfHeader &&other) = delete;  /**< Deleted move operator.*/
    ~SwfHeader()                 = default; /**< Default destructor.*/

    /**
     * Constructor of the SwfHeader class.
     *
     * @param reader The reader that contain the desired data.
     */
    explicit SwfHeader(SwfReader &reader);

    /**
     * Classic redefinition of the "<<" operator for output.
     */
    friend std::ostream &operator<<(std::ostream &   os,
                                    SwfHeader const &swf_header);

private:
    /**
     * Helper to represent the compression used by the SwfFile that this header
     * represents.
     */
    [[nodiscard]] std::string get_compression_string() const;

    Compression const          m_compression;
    uint16 const               m_always_ws;
    uint8 const                m_version;
    uint32 const               m_file_length;
    SwfRect const              m_frame_size;
    SwfFixedPoint<uint8> const m_frame_rate;
    uint16 const               m_frame_count;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFHEADER_H
