#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFSTRUCTS_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFSTRUCTS_H

#include <cmath>
#include <ostream>

#include "botofu/ios/core/BinaryReader.h"
#include "botofu/ios/core/types.h"

/**
 * The SWF Rect structure.
 */
struct SwfRect {
    SwfRect() = default;

    BitArray const m_values_bit_length;
    BitArray const m_x_min;
    BitArray const m_x_max;
    BitArray const m_y_min;
    BitArray const m_y_max;
};

std::ostream &operator<<(std::ostream &os, SwfRect const &rect);

/**
 * The SWF fixed-point number structure.
 *
 * @tparam BaseType The type used to store the decimal and the
 * fractional parts.
 */
template <typename BaseType>
struct SwfFixedPoint {
    SwfFixedPoint() = default;

    SwfFixedPoint(BaseType dec, BaseType frac)
          : decimal_part{dec}, fractional_part{frac} {
    }

    BaseType decimal_part;
    BaseType fractional_part;
};

template <typename BaseType>
std::ostream &operator<<(std::ostream &os, SwfFixedPoint<BaseType> const &fp) {
    return os << "FixedPoint<" << typeid(BaseType).name() << ">"
              << "(" << static_cast<int64>(fp.decimal_part) << "."
              << static_cast<int64>(fp.fractional_part) << ")";
}

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFSTRUCTS_H
