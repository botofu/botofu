#include "botofu/swf/parser/SwfFile.h"

SwfFile::SwfFile(fs::path const &filename)
      : m_reader(filename, SwfHeader::bytes_read_before_compression),
        m_header(m_reader) {
    do {
        m_tags.emplace_back(m_reader);
    } while (m_tags.back().get_type_id() != swf_constants::TagType::END);
}

std::ostream &operator<<(std::ostream &os, SwfFile const &swf_file) {
    os << swf_file.m_header << std::endl;
    int i{0};
    for (auto const &tag : swf_file.m_tags) {
        os << "(" << i++ << ") " << tag << std::endl;
    }
    return os;
}

SwfTag const &SwfFile::get_tag(uint32 index) const {
    return m_tags.at(index);
}

SwfTag &SwfFile::find_first_tag_by_type(swf_constants::TagType id) {
    // Searching for the first tag that match the given ID.
    auto it = std::find_if(
          m_tags.begin(), m_tags.end(), [id](SwfTag const &tag) -> bool {
              return tag.get_type_id() == id;
          });
    // If the given tag ID is not present then we can throw an exception.
    if (it == m_tags.end()) {
        throw std::out_of_range(
              "The given ID doesn't exist in the read SWF file.");
    }
    // Else, return the tag
    return *it;
}

SwfTag const &SwfFile::find_first_tag_by_type(swf_constants::TagType id) const {
    // Searching for the first tag that match the given ID.
    auto const it =
          std::find_if(m_tags.cbegin(), m_tags.cend(), [id](SwfTag const &tag) {
              return tag.get_type_id() == id;
          });
    // If the given tag ID is not present then we can throw an exception.
    if (it == m_tags.cend()) {
        throw std::out_of_range(
              "The given ID doesn't exist in the read SWF file.");
    }
    // Else, return the tag
    return *it;
}

std::optional<std::shared_ptr<TagBase> const>
SwfFile::get_first_tag_by_type(swf_constants::TagType const &type) const {
    for (auto const &tag : m_tags) {
        if (tag.get_type_id() == type) {
            return tag.get_tag();
        }
    }
    return std::nullopt;
}
