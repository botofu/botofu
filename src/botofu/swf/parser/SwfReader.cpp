#include "botofu/swf/parser/SwfReader.h"

#include <cstdio>
#include <sstream>
#include <string>

#include <spdlog/spdlog.h>
#include <zlib.h>

#include "botofu/swf/parser/SwfHeader.h"

#define BOTOFU_PRINT_ZLIB_ERROR(ERROR, STR)                                    \
    case ERROR:                                                                \
        SPDLOG_WARN(STR);                                                      \
        break;

static inline void zlib_error_code_print(int error) {
    switch (error) {
        BOTOFU_PRINT_ZLIB_ERROR(Z_OK, "Z_OK")
        BOTOFU_PRINT_ZLIB_ERROR(Z_STREAM_END, "Z_STREAM_END")
        BOTOFU_PRINT_ZLIB_ERROR(Z_NEED_DICT, "Z_NEED_DICT")
        BOTOFU_PRINT_ZLIB_ERROR(Z_ERRNO, "Z_ERRNO")
        BOTOFU_PRINT_ZLIB_ERROR(Z_STREAM_ERROR, "Z_STREAM_ERROR")
        BOTOFU_PRINT_ZLIB_ERROR(Z_DATA_ERROR, "Z_DATA_ERROR")
        BOTOFU_PRINT_ZLIB_ERROR(Z_MEM_ERROR, "Z_MEM_ERROR")
        BOTOFU_PRINT_ZLIB_ERROR(Z_BUF_ERROR, "Z_BUF_ERROR")
        BOTOFU_PRINT_ZLIB_ERROR(Z_VERSION_ERROR, "Z_VERSION_ERROR")
    default:
        SPDLOG_ERROR("UNKNOWN ZLIB error");
    }
}

static inline void zlib_uncompress(char *        data,
                                   std::istream &input_stream,
                                   std::size_t   uncompressed_size) {
    // Algorithm may be sub-optimal. Instead of streaming, let's read all the
    // data and uncompress it in one go.
    std::string compressed_data(std::istreambuf_iterator<char>(input_stream),
                                {});
    // From https://zlib.net/zlib_how.html
    int      ret;
    z_stream strm;
    // allocate inflate state
    strm.zalloc = Z_NULL;
    strm.zfree  = Z_NULL;
    strm.opaque = Z_NULL;
    // One of the few occasion where reinterpret_cast is not bad:
    // https://stackoverflow.com/q/28258136
    strm.next_in = reinterpret_cast<unsigned char *>(compressed_data.data());
    // We do not expect files larger than 2^32 bytes (4GB) so it is safe to do
    // this here.
    strm.avail_in = static_cast<unsigned>(compressed_data.size());
    inflateInit(&strm);
    // One of the few occasion where reinterpret_cast is not bad:
    // https://stackoverflow.com/q/28258136
    strm.next_out = reinterpret_cast<unsigned char *>(data);
    // We do not expect files larger than 2^32 bytes (4GB) so it is safe to do
    // this here.
    strm.avail_out = static_cast<unsigned>(uncompressed_size);
    ret            = inflate(&strm, Z_FINISH);
    if (ret != Z_STREAM_END) {
        SPDLOG_WARN("ZLIB decompression finished a return code different than "
                    "Z_STREAM_END.");
        zlib_error_code_print(ret);
    }
    inflateEnd(&strm);
}

SwfReader::SwfReader(fs::path const &path, std::size_t header_size)
      : LittleEndianReader() {
    std::ifstream swf_file(path.string(), std::ios::binary);

    // Read non-compressed header data
    m_decompressed_file.reserve(header_size);
    swf_file.read(m_decompressed_file.data(),
                  static_cast<std::streamsize>(header_size));

    // We want to make sure that the decompression should use ZLIB, and also
    // know the exact size of the decompressed data. To do so, we duplicate some
    // parts of SwfHeader code. Code duplication may be avoided in the future by
    // using other methods (?).
    this->set_buffer(m_decompressed_file.data(), header_size);
    Compression const compression{static_cast<Compression>(this->read_uint8())};
    if (compression == Compression::LZMA) {
        SPDLOG_ERROR("The SWF file provided uses LZMA compression "
                     "which is not supported at the moment.");
    }
    // Ignore 3 bytes for "always_ws" and "version"
    this->ignore(3);
    uint32_t const file_length{this->read_uint32()};
    // Reserve enough space
    m_decompressed_file.reserve(file_length);
    // Uncompress if needed
    if (compression == Compression::UNCOMPRESSED) {
        swf_file.read(m_decompressed_file.data() + header_size,
                      static_cast<std::streamsize>(file_length - header_size));
    } else if (compression == Compression::ZLIB) {
        zlib_uncompress(m_decompressed_file.data() + header_size,
                        swf_file,
                        file_length - header_size);
    }

    this->set_buffer(m_decompressed_file.data(), m_decompressed_file.size());
}

SwfRect SwfReader::read_rect() {
    // Just pass the bytes for the moment.
    uint8 const read{this->read_uint8()};
    uint8 const size{static_cast<uint8>(read >> 3u)};
    uint8 number_of_bytes{static_cast<uint8>(std::ceil((4 * size - 3) / 8.0))};
    this->ignore(number_of_bytes);
    return SwfRect();
}

std::string SwfReader::read_null_terminated_utf() {
    std::string ret(this->get_current_position());
    this->advance(ret.size() + 1);
    return ret;
}

uint32 SwfReader::read_var_u30() {
    return this->read_var_uint32();
}

int32 SwfReader::read_int24() {
    ByteArray const bytes = this->read_bytes(3);
    uint32          result{static_cast<uint32>(bytes[0])
                  | static_cast<uint32>(static_cast<uint8>(bytes[1]) << 8u)
                  | static_cast<uint32>(static_cast<uint8>(bytes[2]) << 16u)};
    if (result >> 23u) {
        result |= 0xFF000000;
    }
    return static_cast<int32>(result);
}

SwfReader::SwfReader(char const *data, std::size_t size)
      : BinaryReader(data, size) {
}
