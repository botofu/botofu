#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFFILE_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFFILE_H

#include <filesystem>
#include <fstream>   // std::ifstream
#include <optional>
#include <ostream>   // std::ostream
#include <string>    // std::string
#include <vector>    // std::vector

#include "botofu/swf/parser/SwfHeader.h"   // SwfHeader
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/SwfTag.h"   // SwfTag

namespace fs = std::filesystem;

/**
 * Represent the content of a SWF file.
 *
 * An SWF file is composed of an header, followed by an undetermined
 * number of tags. The tags data are defined in the subdirectory "tags"
 * and the functors used to parse theses data (and so construct the tags)
 * are defined in the subdirectory "parsing".
 */
class SwfFile {
public:
    SwfFile()                     = delete; /**< Deleted default constructor.*/
    SwfFile(SwfFile const &other) = delete; /**< Deleted copy constructor.*/
    SwfFile(SwfFile &&other)      = delete; /**< Deleted constructor.*/
    SwfFile &
             operator=(SwfFile const &other) = delete; /**< Deleted copy operator.*/
    SwfFile &operator=(SwfFile &&other) = delete;  /**< Deleted move operator.*/
    ~SwfFile()                          = default; /**< Default destructor.*/

    /**
     * @brief Constructor of the SwfFile class.
     *
     * @param [in] filename The path of the SWF file to read.
     */
    explicit SwfFile(fs::path const &filename);

    /**
     * Definition of "<<" operator to display the parsed informations.
     */
    friend std::ostream &operator<<(std::ostream &os, SwfFile const &swf_file);

    /**
     * @brief Return the tag at the position given by index.
     *
     * @param index The position of the tag in the file.
     * @return The tag number index, starting from the beginning of the SWF
     * file.
     * @throw std::out_of_range if index is superior than the total number of
     * tags in the SWF file.
     */
    [[nodiscard]] SwfTag const &get_tag(uint32 index) const;

    /**
     * @brief try to find the first tag of the given type
     * @param type the type of the tag we are searching for
     * @return the first tag of the given type if found, else std::nullopt
     */
    [[nodiscard]] std::optional<std::shared_ptr<TagBase> const>
    get_first_tag_by_type(swf_constants::TagType const &type) const;

private:
    /**
     * Find the first tag that has the given type.
     * @param id The type we want to search.
     * @return The first tag with the given type.
     * @throw std::out_of_range if there is no tag with the given type in the
     * SWF file.
     */
    SwfTag &find_first_tag_by_type(swf_constants::TagType id);

    /**
     * Find the first tag that has the given type.
     * @param id The type we want to search.
     * @return The first tag with the given type.
     * @throw std::out_of_range if there is no tag with the given type in the
     * SWF file.
     */
    [[nodiscard]] SwfTag const &
    find_first_tag_by_type(swf_constants::TagType id) const;

    /**
     * The reader used to read all the data in the SwfFile.
     */
    SwfReader m_reader;

    /**
     * The header of the SwfFile.
     *
     * The header may (or may not) rebuild SwfFile::_filtered_stream during
     * instanciation. If the SWF file is compressed, then the header will
     * rebuild SwfFile::_filtered_stream.
     */
    SwfHeader const m_header;

    /**
     * The list of all the tags in the SwfFile.
     */
    std::vector<SwfTag> m_tags;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_SWFFILE_H
