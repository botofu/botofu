#include "botofu/swf/parser/SwfTag.h"

SwfTag::SwfTag(SwfReader &reader) {
    constexpr uint8 MASK_00111111{static_cast<uint8>(0x3F)};

    uint16 const tag_code_and_length{reader.read_uint16()};
    m_tag_type = static_cast<swf_constants::TagType>(tag_code_and_length >> 6u);
    m_tag_size = static_cast<uint32>(tag_code_and_length & MASK_00111111);
    if (m_tag_size == MASK_00111111) {
        m_tag_size = reader.read_uint32();
    }

    auto const it = swf_constants::TAG_CONSTRUCTION_FUNCTIONS.find(m_tag_type);
    if (it != swf_constants::TAG_CONSTRUCTION_FUNCTIONS.end()) {
        m_tag_informations = it->second(reader, m_tag_size);
    } else {
        m_tag_informations = ParseTagNotImplemented()(reader, m_tag_size);
    }
}

swf_constants::TagType SwfTag::get_type_id() const {
    return m_tag_type;
}

std::string SwfTag::get_type_name() const {
    std::string name;
    auto const  it = swf_constants::TAG_NAME.find(m_tag_type);
    if (it != swf_constants::TAG_NAME.end()) {
        return it->second;
    }
    return std::string("UnknownTag[")
           + std::to_string(static_cast<uint16>(m_tag_type)) + std::string("]");
}

std::ostream &operator<<(std::ostream &os, SwfTag const &tag) {
    os << "SwfTag("
       << "Type = " << tag.get_type_name() << ", "
       << "Size = " << tag.m_tag_size << ", "
       << "Informations = ";
    tag.m_tag_informations->display(os);
    return os << ")";
}

std::shared_ptr<TagBase> const &SwfTag::get_tag() const {
    return m_tag_informations;
}
