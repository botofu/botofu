#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGBASE_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGBASE_H

#include <ostream>

struct TagBase {
    virtual void display(std::ostream &) const = 0;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGBASE_H
