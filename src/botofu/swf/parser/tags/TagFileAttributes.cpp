#include "botofu/swf/parser/tags/TagFileAttributes.h"

void TagFileAttributes::display(std::ostream &os) const {
    os << "TagFileAttributes(" << std::boolalpha
       << "UseDirectBlit = " << m_use_direct_blit << ", "
       << "UseGPU = " << m_use_gpu << ", "
       << "HasMetadata = " << m_has_metadata << ", "
       << "Actionscript3 = " << m_actionscript_3 << ", "
       << "UseNetwork = " << m_use_network << ")" << std::noboolalpha;
}