#include "botofu/swf/parser/tags/TagDoAbc.h"

#include <utility>

void TagDoAbc::display(std::ostream &os) const {
    os << "TagDoAbc(" << "Flags = 0x" << std::hex << m_flags << std::dec << ", "
       << "Name = " << m_name << ", " << "Minor version = " << m_minor_version
       << ", " << "Major version = " << m_major_version << ", "
       << "Constant pool = " << m_constant_pool_info << ", "
       << "METHOD count = " << m_method_count << ", "
       << "METADATA count = " << m_metadata_count << ", "
       << "CLASS count = " << m_class_count << ", "
       << "Script count = " << m_script_count << ", "
       << "METHOD body count = " << m_method_body_count << ")";
}

void TagDoAbc::emplace_method_body(abc::MethodBody &&method_body) {
    m_method_bodies.emplace_back(std::move(method_body));
    m_body_map[m_method_bodies.back().m_method] = m_method_bodies.size() - 1;
}

abc::MethodBody const &TagDoAbc::get_method_body(uint32 index) const {
    return m_method_bodies[m_body_map.at(index)];
}

bool TagDoAbc::method_has_body(uint32 index) const {
    return m_body_map.find(index) != m_body_map.end();
}
