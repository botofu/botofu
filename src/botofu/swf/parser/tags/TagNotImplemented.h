#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGNOTIMPLEMENTED_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGNOTIMPLEMENTED_H

#include <ostream>

#include "botofu/swf/parser/tags/TagBase.h"

struct TagNotImplemented final : public TagBase {
    virtual ~TagNotImplemented() = default;

    void display(std::ostream &os) const override;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGNOTIMPLEMENTED_H
