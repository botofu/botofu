#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGFILEATTRIBUTES_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGFILEATTRIBUTES_H

#include <iomanip>
#include <ostream>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/tags/TagBase.h"

struct TagFileAttributes final : public TagBase {
    virtual ~TagFileAttributes() = default;

    void display(std::ostream &os) const override;

    bool   m_first_reserved_bit{};
    bool   m_use_direct_blit{};
    bool   m_use_gpu{};
    bool   m_has_metadata{};
    bool   m_actionscript_3{};
    uint8  m_second_reserved_bits{};
    bool   m_use_network{};
    uint32 m_third_reserved_bits{};
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGFILEATTRIBUTES_H
