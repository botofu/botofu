#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGDOABC_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGDOABC_H

#include <ostream>
#include <string>
#include <unordered_map>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/abc/ClassInfo.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/InstanceInfo.h"
#include "botofu/swf/parser/abc/Metadata.h"
#include "botofu/swf/parser/abc/MethodBody.h"
#include "botofu/swf/parser/abc/MethodInfo.h"
#include "botofu/swf/parser/abc/ScriptInfo.h"
#include "botofu/swf/parser/tags/TagBase.h"

struct TagDoAbc final : public TagBase {
    virtual ~TagDoAbc() = default;

    void display(std::ostream &os) const override;

    abc::MethodBody const &get_method_body(uint32 index) const;
    bool                   method_has_body(uint32 index) const;

    void emplace_method_body(abc::MethodBody &&method_body);

    uint32                         m_flags;
    std::string                    m_name;
    uint16                         m_minor_version;
    uint16                         m_major_version;
    abc::ConstantPoolInfo          m_constant_pool_info;
    uint32                         m_method_count;
    std::vector<abc::MethodInfo>   m_methods_info;
    uint32                         m_metadata_count;
    std::vector<abc::Metadata>     m_metadatas;
    uint32                         m_class_count;
    std::vector<abc::InstanceInfo> m_instances;
    std::vector<abc::ClassInfo>    m_classes;
    uint32                         m_script_count;
    std::vector<abc::ScriptInfo>   m_scripts;
    uint32                         m_method_body_count;
    std::vector<abc::MethodBody>   m_method_bodies;

private:
    std::unordered_map<uint32 /*real index*/, std::size_t /*internal index*/>
          m_body_map;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_TAGS_TAGDOABC_H
