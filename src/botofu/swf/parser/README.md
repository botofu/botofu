# `botofu/swf/parser`

A stupid and partial translation of the 
[SWF File Format Specification (version 19)](https://www.adobe.com/content/dam/acom/en/devnet/pdf/swf-file-format-spec.pdf) 
and 
[ActionScript Virtual Machine 2 Overview](https://wwwimages2.adobe.com/content/dam/acom/en/devnet/pdf/avm2overview.pdf).

## What's implemented? 

As written in the introduction, this library only implements partially the
[SWF File Format Specification (version 19)](https://www.adobe.com/content/dam/acom/en/devnet/pdf/swf-file-format-spec.pdf).

The library is currently capable of:

- Reading the SWF file header.
- Uncompressing SWF files compressed with ZLIB or uncompressed SWF files. LZMA
  compression is not supported for the moment.
- Reading the implemented SWF tags and ignoring the non-implemented ones. 
  Currently implemented tags are:
  1. `DoAbc`
  2. `FileAttribute`
- The `DoAbc` tag is fully implemented and can be explored with the provided 
  methods. See the 
  [ActionScript Virtual Machine 2 Overview](https://wwwimages2.adobe.com/content/dam/acom/en/devnet/pdf/avm2overview.pdf)
  for more details on the available data.
  
## Additions to the [ActionScript Virtual Machine 2 Overview](https://wwwimages2.adobe.com/content/dam/acom/en/devnet/pdf/avm2overview.pdf) specification

During the implementation I found out that several `Multiname`s were not
parsed correctly. The type `TemplatedTypeMultiname` in 
[Multiname.h](src/botofu/swf/parser/abc/ConstantPoolInfo/Multiname.h) 
is never explained in the official documentation and I had to guess its 
semantic. Thanks to @Lakh92 for helping me out with this. 

  