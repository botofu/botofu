#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGFILEATTRIBUTES_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGFILEATTRIBUTES_H

#include <memory>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/parsing/ParseTagBase.h"
#include "botofu/swf/parser/tags/TagBase.h"

struct ParseTagFileAttributes final : public ParseTagBase {
    virtual ~ParseTagFileAttributes() = default;

    std::shared_ptr<TagBase> operator()(SwfReader &reader,
                                        uint32     size) override;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGFILEATTRIBUTES_H
