#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGNOTIMPLEMENTED_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGNOTIMPLEMENTED_H

#include <memory>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/parsing/ParseTagBase.h"
#include "botofu/swf/parser/tags/TagBase.h"

struct ParseTagNotImplemented final : public ParseTagBase {
    virtual ~ParseTagNotImplemented() = default;

    std::shared_ptr<TagBase> operator()(SwfReader &reader,
                                        uint32     size) override;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGNOTIMPLEMENTED_H
