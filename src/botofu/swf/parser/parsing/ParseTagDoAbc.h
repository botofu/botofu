#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGDOABC_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGDOABC_H

#include <istream>   // std::istream
#include <memory>    // std::shared_ptr

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/parsing/ParseTagBase.h"
#include "botofu/swf/parser/tags/TagBase.h"

struct ParseTagDoAbc final : public ParseTagBase {
    virtual ~ParseTagDoAbc() = default;

    std::shared_ptr<TagBase> operator()(SwfReader &reader,
                                        uint32 /*size*/) override;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGDOABC_H
