#include "botofu/swf/parser/parsing/ParseTagNotImplemented.h"

#include <spdlog/spdlog.h>

#include "botofu/swf/parser/tags/TagNotImplemented.h"

std::shared_ptr<TagBase> ParseTagNotImplemented::operator()(SwfReader &reader,
                                                            uint32     size) {
    SPDLOG_DEBUG("Parsing a TagNotImplemented of size '{}'...", size);

    reader.ignore(size);
    return std::make_shared<TagNotImplemented>();
}