#include "botofu/swf/parser/parsing/ParseTagDoAbc.h"

#include <spdlog/spdlog.h>

#include "botofu/swf/parser/tags/TagDoAbc.h"

std::shared_ptr<TagBase>
ParseTagDoAbc::operator()(SwfReader &reader, [[maybe_unused]] uint32 size) {
    SPDLOG_DEBUG("Parsing a TagDoAbc of size '{}'...", size);
    std::shared_ptr<TagDoAbc> tag = std::make_shared<TagDoAbc>();
    tag->m_flags                  = reader.read_uint32();
    tag->m_name                   = reader.read_null_terminated_utf();
    tag->m_minor_version          = reader.read_uint16();
    tag->m_major_version          = reader.read_uint16();
    tag->m_constant_pool_info     = abc::ConstantPoolInfo(reader);
    tag->m_method_count           = reader.read_var_u30();
    for (uint32 i{0}; i < tag->m_method_count; ++i) {
        tag->m_methods_info.emplace_back(reader);
    }
    tag->m_metadata_count = reader.read_var_u30();
    for (uint32 i{0}; i < tag->m_metadata_count; ++i) {
        tag->m_metadatas.emplace_back(reader);
    }
    tag->m_class_count = reader.read_var_u30();
    for (uint32 i{0}; i < tag->m_class_count; ++i) {
        tag->m_instances.emplace_back(reader);
    }
    for (uint32 i{0}; i < tag->m_class_count; ++i) {
        tag->m_classes.emplace_back(reader);
    }
    tag->m_script_count = reader.read_var_u30();
    for (uint32 i{0}; i < tag->m_script_count; ++i) {
        tag->m_scripts.emplace_back(reader);
    }
    tag->m_method_body_count = reader.read_var_u30();
    for (uint32 i{0}; i < tag->m_method_body_count; ++i) {
        tag->emplace_method_body(abc::MethodBody(reader));
    }

    SPDLOG_DEBUG("Name: {}", tag->m_name);
    SPDLOG_DEBUG("Major version: {}", tag->m_major_version);
    SPDLOG_DEBUG("Minor version: {}", tag->m_minor_version);
    SPDLOG_DEBUG("Number of methods: {}", tag->m_method_count);
    SPDLOG_DEBUG("Number of metadata: {}", tag->m_metadata_count);
    SPDLOG_DEBUG("Number of classes: {}", tag->m_class_count);
    SPDLOG_DEBUG("Number of scripts: {}", tag->m_script_count);
    SPDLOG_DEBUG("Number of method bodies: {}", tag->m_method_body_count);

    return tag;
}
