#include "botofu/swf/parser/parsing/ParseTagFileAttributes.h"

#include <spdlog/spdlog.h>

#include "botofu/swf/parser/tags/TagFileAttributes.h"

std::shared_ptr<TagBase>
ParseTagFileAttributes::operator()(SwfReader &             reader,
                                   [[maybe_unused]] uint32 size) {
    std::shared_ptr<TagFileAttributes> tag =
          std::make_shared<TagFileAttributes>();

    SPDLOG_DEBUG(
          "Parsing a TagFileAttributes of size '{}' (size should be 4)...",
          size);

    assert(size == 4);

    uint8 tmp{reader.read_uint8()};
    tag->m_first_reserved_bit = (tmp >> 7u);
    tag->m_use_direct_blit    = static_cast<uint8>(tmp >> 6u) & MASK_00000001;
    tag->m_use_gpu            = static_cast<uint8>(tmp >> 5u) & MASK_00000001;
    tag->m_has_metadata       = static_cast<uint8>(tmp >> 4u) & MASK_00000001;
    tag->m_actionscript_3     = static_cast<uint8>(tmp >> 3u) & MASK_00000001;
    tag->m_second_reserved_bits =
          static_cast<uint8>(tmp >> 1u)
          & static_cast<uint8>(MASK_00000001 | MASK_00000010);
    tag->m_use_network = tmp & MASK_00000001;
    tag->m_third_reserved_bits =
          static_cast<uint32>(reader.read_uint8() << 16u);
    tag->m_third_reserved_bits |=
          static_cast<uint32>(reader.read_uint8() << 8u);
    tag->m_third_reserved_bits |= static_cast<uint32>(reader.read_uint8());

    return tag;
}
