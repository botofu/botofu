#ifndef BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGBASE_H
#define BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGBASE_H

#include <istream>
#include <memory>

#include "botofu/ios/core/types.h"
#include "botofu/swf/parser/SwfReader.h"
#include "botofu/swf/parser/tags/TagBase.h"

class ParseTagBase {
public:
    virtual std::shared_ptr<TagBase> operator()(SwfReader &reader, uint32) = 0;
};

#endif   // BOTOFU_SRC_BOTOFU_SWF_PARSER_PARSING_PARSETAGBASE_H
