# `botofu/protocol/parser`

Parse the `DofusInvoker.swf`, outputs a JSON. Simple.

## Executables

Executables are available for Windows and Linux. If you have any issue using 
these executables please open an issue on this repository or contact me via 
Discord @Nelimee#2764.

1. [Windows x64](https://mega.nz/file/zp5hUQwI#u5p6ggs-2PDeAKhX8mLEGO42dTv7Gd7px39KtrNfqC8)
2. [Linux x64](https://mega.nz/file/awwDEA6R#Dnz7KyzSf-_VZHXd-DxM-uBsIpINdNsJTpL1xXZcazc)

### Note about the Linux executable

You might have an error looking like 
```text
./botofu_protocol_parser: /usr/lib/x86_64-linux-gnu/libstdc++.so.6: version `GLIBCXX_3.4.26' not found (required by ./botofu_protocol_parser)
```
or
```text
./botofu_protocol_parser: error while loading shared libraries: libstdc++.so.6: cannot open shared object file: No such file or directory
```
This means that your version of `libstdc++.so` is either outdated or cannot be 
found.
The solution is to install/upgrade `libstdc++.so` (or any other shared library
that appears in the error).

## How to install

### Using `docker` (recommended)

```shell script
docker pull registry.gitlab.com/botofu/protocol_parser
# Assuming that the Dofus directory is /local/dir/ (/local/dir/DofusInvoker.swf should exist)
docker run --rm -v /local/dir/:/data/ -it registry.gitlab.com/botofu/protocol_parser -i 2 /data/DofusInvoker.swf /data/protocol.json
# protocol.json is now in /local/dir/protocol.json 
```

### From source

Make sure that you have `git`, `cmake >= 3.13` and a C++17-compatible compiler 
installed.

#### Compiler support

Tested successfully with:

- `clang` 9 and 11
- `g++` 8.3 and 9.3
- `MSVC` 19.25.28614.0

The `g++` 8.3 was tested on the Debian distribution of the Windows Subsystem for Linux 
with success.

#### Installation procedure

```shell script
git clone git@gitlab.com:botofu/botofu.git
cd botofu
mkdir build && cd build
# The next command will download dependencies and may take a long time
cmake -H. -DCMAKE_BUILD_TYPE=Release .. 
# You may add options for parallel compilation here.
# On linux, add "-- -j 8" for parallel compilation on 8 processes.
cmake --build ./ --target botofu_protocol_parser
```

## Usage

### Documentation 

```text
>>> ./botofu_protocol_parser --help
Protocol parser for the Botofu project. Version 0.5.
Usage: ./botofu_protocol_parser [OPTIONS] DofusInvokerPath [ExportPath]

Positionals:
  DofusInvokerPath TEXT:FILE REQUIRED
                              Path to the DofusInvoker.swf
  ExportPath TEXT             Path to the file that will contain the output of the protocol parsing. If not provided, dump to the standard output.

Options:
  -h,--help                   Print this help message and exit
  -o,--out TEXT               Path to the file that will contain the output of the protocol parsing. If not provided, dump to the standard output.
  -i,--indent INT             Number of spaces used to indent resulting JSON. Default to minified (0).
  -a,--all,--no-default       No "default" entry in the resulting JSON. All the keys will be explicitly written, with a default value for the non-set keys.
  --ipa,--include-private-attributes
                              Include also the private attributes prefixed by a "_" in the generated JSON.
```

## Output JSON format

The created JSON looks like (needs to be updated):
```text
{
  "default": {
    "field": {
      "boolean_byte_wrapper_position": -1,
      "bounds": null,
      "constant_length": 0,
      "default_value": "",
      "name": "",
      "namespace": "",
      "null_checked": false,
      "prefixed_by_type_id": false,
      "self_serialize_method": "",
      "type": "",
      "type_namespace": "",
      "use_boolean_byte_wrapper": false,
      "write_false_if_null_method": "",
      "write_length_method": "",
      "write_method": "",
      "write_type_id_method": ""
    }
  },
  "enumerations": [
    {
      "entries_type": "uint",
      "members": {
        "HOOK_POINT_CATEGORY_BASE_BACKGROUND": "4",
        "HOOK_POINT_CATEGORY_BASE_FOREGROUND": "6",
        "HOOK_POINT_CATEGORY_LIFTED_ENTITY": "3",
        "HOOK_POINT_CATEGORY_MERCHANT_BAG": "5",
        "HOOK_POINT_CATEGORY_MOUNT_DRIVER": "2",
        "HOOK_POINT_CATEGORY_PET": "1",
        "HOOK_POINT_CATEGORY_PET_FOLLOWER": "7",
        "HOOK_POINT_CATEGORY_UNDERWATER_BUBBLES": "8",
        "HOOK_POINT_CATEGORY_UNUSED": "0"
      },
      "name": "SubEntityBindingPointCategoryEnum"
    },
    // ...
  ],
  "messages": [
    {
      "fields": [
        {
          "default_value": "false",
          "name": "quiet",
          "type": "Boolean",
          "write_method": "writeBoolean"
        },
        {
          "default_value": "false",
          "name": "_isInitialized",
          "namespace": "com.ankamagames.dofus.network.messages.common.basic",
          "type": "Boolean"
        }
      ],
      "name": "BasicPingMessage",
      "namespace": "com.ankamagames.dofus.network.messages.common.basic",
      "protocolID": 182,
      "super": "NetworkMessage",
      "supernamespace": "com.ankamagames.jerakine.network",
      "use_hash_function": false
    },
    // ...
  ],
  "types": [
    {
      "fields": [
        {
          "bounds": {
            "low": "-9007199254740992.000000",
            "up": "9007199254740992.000000"
          },
          "default_value": "0",
          "name": "contextualId",
          "type": "Number",
          "write_method": "writeDouble"
        },
        {
          "name": "_dispositiontree",
          "namespace": "com.ankamagames.dofus.network.types.game.context",
          "type": "FuncTree",
          "type_namespace": "com.ankamagames.jerakine.network.utils"
        },
        {
          "name": "disposition",
          "prefixed_by_type_id": true,
          "self_serialize_method": "serialize",
          "type": "EntityDispositionInformations",
          "type_namespace": "com.ankamagames.dofus.network.types.game.context",
          "write_type_id_method": "writeShort"
        }
      ],
      "name": "GameContextActorPositionInformations",
      "namespace": "com.ankamagames.dofus.network.types.game.context",
      "protocolID": 566,
      "super": "",
      "supernamespace": "",
      "use_hash_function": false
    },
    // ...
  ],
  "version": {
    "client": {
      "build": {
        "date": {
          "day": 1,
          "full": "2020-4-1 13:45",
          "hour": 13,
          "minute": 45,
          "month": 4,
          "year": 2020
        }
      },
      "major": 2,
      "minor": 55,
      "patch": 1
    },
    "parser": {
      "major": 0,
      "minor": 2
    },
    "protocol": {
      "date": {
        "day": 13,
        "full": "2020-3-13 14:43",
        "hour": 14,
        "minute": 43,
        "month": 3,
        "year": 2020
      },
      "version": {
        "current": 1966,
        "minimum": 1966
      }
    }
  }
}
```

