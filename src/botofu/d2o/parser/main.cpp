#include <filesystem>
#include <iostream>
#include <memory>
#include <optional>

#include <CLI/CLI.hpp>
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>

#include "botofu/common/matchers/core/I18nCallPropertyCoreMatcher.h"
#include "botofu/d2o/parser/data_structures/DatacenterClassInformation.h"
#include "botofu/d2o/parser/utils/CLIOptions.h"
#include "botofu/d2o/parser/version.h"
#include "botofu/swf/parser/SwfFile.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

namespace fs = std::filesystem;

static std::optional<std::shared_ptr<TagDoAbc>>
get_do_abc_tag(fs::path const &dofus_invoker_path) {
    SPDLOG_INFO("Parsing SWF file provided: '{}'...",
                dofus_invoker_path.string());
    SwfFile file(dofus_invoker_path);
    auto    do_abc_tag_optional =
          file.get_first_tag_by_type(swf_constants::TagType::DOABC);
    if (!do_abc_tag_optional) {
        SPDLOG_ERROR("Could not find a DoABC tag in the SWF file provided.");
        return std::nullopt;
    }
    return std::dynamic_pointer_cast<TagDoAbc>(*do_abc_tag_optional);
}

int main(int argc, char *argv[]) {
    // 1. Command line interface.
    CLIOptions cli_options(argc, argv);
    CLI::App   app("Protocol parser for the Botofu project. Version "
                 + std::to_string(D2O_PARSER_MAJOR_VERSION) + "."
                 + std::to_string(D2O_PARSER_MINOR_VERSION) + ".");
    app.add_option("DofusInvokerPath",
                   cli_options.dofus_invoker_path,
                   "Path to the DofusInvoker.swf")
          ->required()
          ->check(CLI::ExistingFile);
    app.add_option("ExportPath,-o,--out",
                   cli_options.target_json_path,
                   "Path to the file that will contain the output of the "
                   "protocol parsing. "
                   "If not provided, dump to the standard output.");
    app.add_option("-i,--indent",
                   cli_options.indent,
                   "Number of spaces used to indent resulting JSON. Default to "
                   "minified (0).");
    app.add_option("--name",
                   cli_options.parsing_name,
                   "Custom name for the parsing that will be included in the "
                   "JSON metadata.");
    app.add_option(
          "-t,--tag",
          cli_options.tags,
          "A list of tags that will be included in the generated JSON.");
    app.add_flag("-a,--all,--no-default",
                 cli_options.no_default_optimisation,
                 "No \"default\" entry in the resulting JSON. All the "
                 "keys will be explicitly written, with a default value for "
                 "the non-set keys.");

    CLI11_PARSE(app, argc, argv)

    // 2. Load the SWF file provided and recover the DoABC tag.
    auto optional_tag_do_abc = get_do_abc_tag(cli_options.dofus_invoker_path);
    if (!optional_tag_do_abc) {
        return 1;
    }
    std::shared_ptr<TagDoAbc> const tag_do_abc = *optional_tag_do_abc;

    abc::ConstantPoolInfo const &constant_pool_info{
          tag_do_abc->m_constant_pool_info};
    I18nCallPropertyCoreMatcher const matcher(constant_pool_info);

    SPDLOG_INFO("Found {} instances", tag_do_abc->m_classes.size());
    nlohmann::json j;
    j["metadata"]["default"] = DatacenterClassInformation::default_values();
    std::optional<nlohmann::json> const default_class_information_values{
          j["metadata"]["default"]};
    //   std::nullopt};
    j["classes"] = json::array();
    for (uint32_t i{0}; i < tag_do_abc->m_classes.size(); ++i) {
        abc::InstanceInfo const &instance_info{tag_do_abc->m_instances[i]};
        // Do not consider the instances outside of the datacenter namespace
        if (!instance_info.is_in_datacenter_namespace(constant_pool_info)) {
            continue;
        }

        DatacenterClassInformation class_info(*tag_do_abc, i);
        j["classes"].push_back(
              class_info.to_json(default_class_information_values));
    }

    std::cerr << j.dump(2) << std::endl;

    return 0;
}
