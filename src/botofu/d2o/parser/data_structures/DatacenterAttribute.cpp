#include "DatacenterAttribute.h"

#include <optional>
#include <string>

#include <boost/algorithm/string/predicate.hpp>

#include "botofu/protocol/parser/utils/json_helpers.h"
#include "botofu/swf/parser/abc/enumerations.h"

DatacenterAttributeVisibility
visibility_from_namespace(abc::Namespace const &namespace_) {
    switch (namespace_.m_kind) {
    case abc::NamespaceKind::NAMESPACE:
    case abc::NamespaceKind::PACKAGENAMESPACE:
    case abc::NamespaceKind::PACKAGEINTERNALNAMESPACE:
    case abc::NamespaceKind::EXPLICITNAMESPACE:
        return DatacenterAttributeVisibility::PUBLIC;
    case abc::NamespaceKind::PROTECTEDNAMESPACE:
    case abc::NamespaceKind::STATICPROTECTEDNAMESPACE:
        return DatacenterAttributeVisibility::PROTECTED;
    case abc::NamespaceKind::PRIVATENAMESPACE:
        return DatacenterAttributeVisibility::PRIVATE;
    }
}

std::string visibility_to_string(DatacenterAttributeVisibility visibility) {
    switch (visibility) {
    case DatacenterAttributeVisibility::PRIVATE:
        return std::string("private");
    case DatacenterAttributeVisibility::PROTECTED:
        return std::string("protected");
    case DatacenterAttributeVisibility::PUBLIC:
        return std::string("public");
    }
}

json DatacenterAttribute::default_values() {
    json j;
    j["visibility"] =
          visibility_to_string(DatacenterAttributeVisibility::PUBLIC);
    j["is_getter"] = false;
    return j;
}

json DatacenterAttribute::to_json(
      std::optional<json> const &field_default_values) const {
    json j;
    j["name"] = this->name;
    j["type"] = this->type.to_string();
    insert_value(field_default_values,
                 j,
                 "visibility",
                 visibility_to_string(this->visibility));
    insert_value(field_default_values, j, "is_getter", this->is_getter);
    if (this->default_value.has_value()) {
        insert_value(field_default_values,
                     j,
                     "default_value",
                     this->default_value.value());
    }
    return j;
}
