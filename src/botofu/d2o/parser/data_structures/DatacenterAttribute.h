#ifndef BOTOFU_SRC_BOTOFU_D2O_PARSER_DATA_STRUCTURES_DATACENTERATTRIBUTE_H
#define BOTOFU_SRC_BOTOFU_D2O_PARSER_DATA_STRUCTURES_DATACENTERATTRIBUTE_H

#include <optional>
#include <string>
#include <utility>

#include <nlohmann/json.hpp>

#include "botofu/common/dofus_interface/DofusAttribute.h"
#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/dofus_interface/DofusType.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/Namespace.h"

using json = nlohmann::json;

enum struct DatacenterAttributeVisibility { PUBLIC, PROTECTED, PRIVATE };

[[nodiscard]] DatacenterAttributeVisibility
visibility_from_namespace(abc::Namespace const &namespace_);

[[nodiscard]] std::string
visibility_to_string(DatacenterAttributeVisibility visibility);

struct DatacenterAttribute {
    DatacenterAttribute(std::string                   name,
                        DofusType                     type,
                        DatacenterAttributeVisibility visibility,

                        bool                       is_getter,
                        std::optional<std::string> default_value = std::nullopt)
          : name(std::move(name)), type(std::move(type)),
            visibility(visibility), is_getter(is_getter),
            default_value(std::move(default_value)) {
    }

    [[nodiscard]] static json default_values();

    [[nodiscard]] json to_json(
          std::optional<json> const &field_default_values = std::nullopt) const;

    std::string                   name;
    DofusType                     type;
    DatacenterAttributeVisibility visibility;
    bool                          is_getter;
    std::optional<std::string>    default_value;
};

#endif   // BOTOFU_SRC_BOTOFU_D2O_PARSER_DATA_STRUCTURES_DATACENTERATTRIBUTE_H
