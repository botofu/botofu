#include "DatacenterGetter.h"

#include <nlohmann/json.hpp>

#include "botofu/d2o/parser/data_structures/DatacenterAttribute.h"
#include "botofu/protocol/parser/utils/json_helpers.h"

using json = nlohmann::json;

json DatacenterAttributeSerialisation::to_json(
      [[maybe_unused]] std::optional<json> const &field_default_values) const {
    json j;
    j["serialisation_method"] = this->serialisation_method.to_json();
    j["set_attribute_name"]   = this->set_attribute_name.to_json();
    j["attribute_used_as_id"] = this->attribute_used_as_id.to_json();
    return j;
}

json DatacenterGetter::default_values() {
    json j             = DatacenterAttribute::default_values();
    j["serialisation"] = DatacenterAttributeSerialisation().to_json();
    return j;
}

json DatacenterGetter::to_json(
      std::optional<json> const &field_default_values) const {
    json j = DatacenterAttribute::to_json(field_default_values);

    std::optional<json> serialisation_default_values{std::nullopt};
    if (field_default_values.has_value()
        && field_default_values->contains("serialisation")) {
        serialisation_default_values =
              field_default_values->at("serialisation");
    }
    insert_value(field_default_values,
                 j,
                 "serialisation",
                 this->serialisation.to_json(serialisation_default_values));
    return j;
}
