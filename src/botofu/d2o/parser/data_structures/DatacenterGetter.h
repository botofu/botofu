#ifndef BOTOFU_SRC_BOTOFU_D2O_PARSER_DATA_STRUCTURES_DATACENTERGETTER_H
#define BOTOFU_SRC_BOTOFU_D2O_PARSER_DATA_STRUCTURES_DATACENTERGETTER_H

#include <string>
#include <utility>

#include "botofu/d2o/parser/data_structures/DatacenterAttribute.h"

struct DatacenterAttributeSerialisation {
    [[nodiscard]] json to_json(
          std::optional<json> const &field_default_values = std::nullopt) const;

    DofusMethod    serialisation_method;
    DofusAttribute set_attribute_name;
    DofusAttribute attribute_used_as_id;
};

struct DatacenterGetter : public DatacenterAttribute {
    DatacenterGetter(std::string                      name,
                     DofusType                        type,
                     DatacenterAttributeVisibility    visibility,
                     DatacenterAttributeSerialisation serialisation,
                     bool                             is_getter,
                     std::optional<std::string> default_value = std::nullopt)
          : DatacenterAttribute(std::move(name),
                                std::move(type),
                                visibility,
                                is_getter,
                                std::move(default_value)),
            serialisation(serialisation) {
    }

    [[nodiscard]] static json default_values();

    [[nodiscard]] json to_json(
          std::optional<json> const &field_default_values = std::nullopt) const;

    DatacenterAttributeSerialisation serialisation;
};

#endif   // BOTOFU_SRC_BOTOFU_D2O_PARSER_DATA_STRUCTURES_DATACENTERGETTER_H
