#ifndef BOTOFU_SRC_BOTOFU_D2O_PARSER_DATA_STRUCTURES_CLASSINFORMATION_H
#define BOTOFU_SRC_BOTOFU_D2O_PARSER_DATA_STRUCTURES_CLASSINFORMATION_H

#include <optional>
#include <string>
#include <vector>

#include <nlohmann/json.hpp>

#include "botofu/common/dofus_interface/DofusType.h"
#include "botofu/d2o/parser/data_structures/DatacenterAttribute.h"
#include "botofu/d2o/parser/data_structures/DatacenterGetter.h"
#include "botofu/swf/parser/abc/ClassInfo.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/InstanceInfo.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

using json = nlohmann::json;

/**
 * Extracts and store all the data about one class from the network namespace.
 */
struct DatacenterClassInformation {
    DatacenterClassInformation(TagDoAbc const &tag, std::size_t index);

    bool has_attribute(std::string const &name) const;

    [[nodiscard]] json to_json(
          std::optional<json> const &field_default_values = std::nullopt) const;

    [[nodiscard]] static json default_values();

    DofusType                        class_;
    DofusType                        super_class;
    std::optional<std::string>       module_attribute;
    std::vector<DatacenterAttribute> attributes;
    std::vector<DatacenterGetter>    getters;

private:
    /**
     * Extract the data with the given class by calling
     * `extract_attribute_trait` on all the traits associated with the given
     * class.
     */
    void extract_attributes(TagDoAbc const              &tag,
                            abc::ConstantPoolInfo const &constant_pool_info,
                            abc::ClassInfo const        &class_info,
                            abc::InstanceInfo const     &instance_info);

    /**
     * Create an entry in `m_attribute` for the given attribute.
     */
    void
    extract_attribute_trait(abc::TraitInfo const        &trait,
                            abc::ConstantPoolInfo const &constant_pool_info);

    /**
     * Create an entry in `m_attribute` for the given attribute.
     */
    void extract_getter_trait(abc::TraitInfo const        &trait,
                              abc::ConstantPoolInfo const &constant_pool_info,
                              TagDoAbc const              &tag);
};

#endif   // BOTOFU_SRC_BOTOFU_D2O_PARSER_DATA_STRUCTURES_CLASSINFORMATION_H
