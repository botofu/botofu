#include "DatacenterClassInformation.h"

#include <cstddef>
#include <optional>
#include <vector>

#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <fmt/core.h>
#include <spdlog/spdlog.h>

#include "botofu/common/dofus_interface/DofusMethod.h"
#include "botofu/common/dofus_interface/DofusType.h"
#include "botofu/common/matchers/core/I18nCallPropertyCoreMatcher.h"
#include "botofu/d2o/parser/data_structures/DatacenterGetter.h"
#include "botofu/ios/core/types.h"
#include "botofu/protocol/parser/utils/json_helpers.h"
#include "botofu/swf/parser/abc/ClassInfo.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo/Namespace.h"
#include "botofu/swf/parser/abc/InstanceInfo.h"
#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"
#include "botofu/swf/parser/abc/MethodInfo.h"
#include "botofu/swf/parser/abc/constants.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"

DatacenterClassInformation::DatacenterClassInformation(TagDoAbc const &tag,
                                                       std::size_t     index) {
    abc::ClassInfo const        &class_info{tag.m_classes[index]};
    abc::InstanceInfo const     &instance_info{tag.m_instances[index]};
    abc::ConstantPoolInfo const &constant_pool_info{tag.m_constant_pool_info};

    this->class_ = DofusType(instance_info.get_name(constant_pool_info));
    this->super_class =
          DofusType(instance_info.get_super_name(constant_pool_info));
    this->extract_attributes(
          tag, constant_pool_info, class_info, instance_info);
}

void DatacenterClassInformation::extract_attributes(
      TagDoAbc const              &tag,
      abc::ConstantPoolInfo const &constant_pool_info,
      abc::ClassInfo const        &class_info,
      abc::InstanceInfo const     &instance_info) {
    for (auto const &trait : class_info.m_traits) {
        this->extract_attribute_trait(trait, constant_pool_info);
        this->extract_getter_trait(trait, constant_pool_info, tag);
    }
    for (auto const &trait : instance_info.m_traits) {
        this->extract_attribute_trait(trait, constant_pool_info);
        this->extract_getter_trait(trait, constant_pool_info, tag);
    }
}

void DatacenterClassInformation::extract_attribute_trait(
      abc::TraitInfo const        &trait,
      abc::ConstantPoolInfo const &constant_pool_info) {
    abc::TraitKind const kind{trait.get_kind()};
    if (kind == abc::TraitKind::SLOT || kind == abc::TraitKind::CONST_) {
        std::optional<std::string> const optional_name{
              trait.get_name_without_namespace(constant_pool_info)};
        if (!optional_name.has_value()) {
            SPDLOG_ERROR("Tried to get the name of a trait without a name.");
        }
        std::string const name{*optional_name};

        if (name == "MODULE") {
            this->module_attribute = trait.get_data(constant_pool_info);
        } else {
            DofusType const type{DofusType(trait.get_type(constant_pool_info))};
            std::optional<abc::Namespace> namespace_ =
                  trait.get_namespace(constant_pool_info);
            if (!namespace_.has_value()) {
                SPDLOG_ERROR(
                      "Expected a namespace for trait {} but found none.",
                      trait.get_name(constant_pool_info));
            }
            DatacenterAttributeVisibility const visibility{
                  visibility_from_namespace(*namespace_)};
            std::optional<std::string> default_value{
                  trait.get_data(constant_pool_info)};

            this->attributes.emplace_back(
                  name, type, visibility, false, default_value);
        }
    }
}

void DatacenterClassInformation::extract_getter_trait(
      abc::TraitInfo const        &trait,
      abc::ConstantPoolInfo const &constant_pool_info,
      TagDoAbc const              &tag) {
    abc::TraitKind const kind{trait.get_kind()};
    if (kind == abc::TraitKind::GETTER) {
        uint32 const method_index = trait.get_index();
        if (!tag.method_has_body(method_index)) {
            return;
        }
        auto const                 &method = tag.get_method_body(method_index);
        std::vector<Instr> const    instructions{method.disassemble()};
        I18nCallPropertyCoreMatcher matcher(constant_pool_info);
        std::size_t const match{matcher.find_pattern_and_update(instructions)};

        abc::MethodInfo const getter_info{method.get_method_info(tag)};
        std::string const     getter_full_name{
              getter_info.get_name(constant_pool_info)};
        DofusMethod const getter_method{getter_full_name.substr(
              0, getter_full_name.size() - abc::GETTER_POSTFIX.size())};
        DofusType const   getter_return_type{
              getter_info.get_return_type(constant_pool_info)};
        DatacenterAttributeVisibility const visibility{
              visibility_from_namespace(
                    trait.get_namespace(constant_pool_info).value())};
        DatacenterAttributeSerialisation serialisation{};
        if (match < instructions.size()) {
            // Found a match, update serialisation information.
            serialisation.serialisation_method = matcher.serialisation_method;
            serialisation.attribute_used_as_id = matcher.attribute_used_as_id;
            serialisation.set_attribute_name   = matcher.set_attribute_name;
        }
        this->getters.emplace_back(getter_method.get_method_name(),
                                   getter_return_type,
                                   visibility,
                                   serialisation,
                                   true);
    }
}

bool DatacenterClassInformation::has_attribute(std::string const &name) const {
    for (auto const &attribute : this->attributes) {
        if (attribute.name == name) {
            return true;
        }
    }
    return false;
}

[[nodiscard]] json DatacenterClassInformation::default_values() {
    json j;
    j["module"]     = "";
    j["superclass"] = {{"name", "Object"}, {"namespace", ""}};
    j["attributes"] = json::array({DatacenterAttribute::default_values()});
    j["getters"]    = json::array({DatacenterGetter::default_values()});
    return j;
}

[[nodiscard]] json DatacenterClassInformation::to_json(
      std::optional<json> const &field_default_values) const {
    json j;
    j["class"] = this->class_.to_json();
    insert_value(
          field_default_values, j, "superclass", this->super_class.to_json());
    insert_value(field_default_values,
                 j,
                 "module",
                 this->module_attribute.value_or(""));
    j["attributes"] = json::array();
    std::optional<json> attribute_default_values{std::nullopt};
    if (field_default_values.has_value()
        && field_default_values->contains("attributes")) {
        attribute_default_values = field_default_values->at("attributes")[0];
    }
    for (auto const &attribute : this->attributes) {
        j["attributes"].push_back(attribute.to_json(attribute_default_values));
    }

    j["getters"] = json::array();
    std::optional<json> getter_default_values{std::nullopt};
    if (field_default_values.has_value()
        && field_default_values->contains("getters")) {
        getter_default_values = field_default_values->at("getters")[0];
    }
    for (auto const &getter : this->getters) {
        j["getters"].push_back(getter.to_json(getter_default_values));
    }
    return j;
}
